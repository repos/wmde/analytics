"""
Generates HQL testing scripts based on production script found in a given directory.

Usage:
    python3 gen_hql_test_scripts.py -d PATH_TO_HQL_AIRFLOW_JOBS_DIR
"""

import argparse
import os
import re

HEART_CHAR = "\u2665"
WD_GREEN = "\033[38;2;51;153;102m"
COLOR_RESET = "\033[0m"

# MARK: Test Headers

SCRIPT_NAME = "SCRIPT_NAME"

test_create_table_header = """
-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f SCRIPT_NAME

-- Note: This script is for testing and should be ran in your local Hive DB schema.
"""

test_task_header = """
-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f SCRIPT_NAME

-- Note: Please replace TEST_HIVE_USER_DB, TEST_TIME_PERIODS for testing.
"""

test_output_header = """
-- Parameters:
--     None
--
-- Usage:
--     hive -f SCRIPT_NAME

-- Note: Please replace TEST_HIVE_USER_DB for testing.
"""

test_output_footer = """
-- MARK: Drop Table

-- Note: Save the output for your MR!
-- Note: If possible report the output in the Phabricator task (values <25 as '<25').
-- Note: Check HDFS to make sure that test data and metadata have been deleted.

DROP TABLE
    SOURCE_TABLE
;
"""


def main() -> None:
    # MARK: CLI Setup

    parser = argparse.ArgumentParser(
        description="This script converts production HQL scripts into their testing equivalents.",
        epilog="Thank you for testing!",
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=60),
    )

    parser._actions[0].help = "Show this help message and exit."

    parser.add_argument(
        "-d",
        "--directory",
        type=str,
        required=True,
        help="The directory for which testing HQL scripts should be generated.",
    )

    args = parser.parse_args()

    directory = args.directory

    # MARK: Argument Checks

    if directory[0] == "/":
        directory = directory[1:]

    assert (
        len(directory.split("/")) == 3
    ), f"The length of the passed directory path {directory} is invalid."

    assert (
        directory.split("/")[0] == "hql" and directory.split("/")[1] == "airflow_jobs"
    ), f"{directory} is not a valid directory path as it should lead to a directory in hql/airflow_jobs/..."

    dir_hql_files = [f for f in os.listdir(directory) if f[-4:] == ".hql"]

    create_table_hql_files = [
        f for f in dir_hql_files if f[: len("create_table_")] == "create_table_"
    ]
    gen_csv_hql_files = [f for f in dir_hql_files if f[: len("gen_csv_")] == "gen_csv_"]

    dag_task_query_files = [
        f
        for f in dir_hql_files
        if f not in create_table_hql_files and f not in gen_csv_hql_files
    ]

    assert create_table_hql_files, "The create table HQL script(s) are missing."
    assert dag_task_query_files, "The DAG task HQL script(s) are missing."

    dag_frequency = dag_task_query_files[0].split("_")[-1].replace(".hql", "")

    assert all(
        dag_frequency in f for f in dir_hql_files
    ), "Not all files in the directory have the same frequency in their name."

    print()

    # MARK: Testing Directory

    testing_directory = f"{directory}/_test_{dag_frequency}"

    if os.path.isdir(testing_directory):
        print(
            f"Testing directory {testing_directory} already exists. Files will be overwritten.\n"
        )

    else:
        print(
            f"Testing directory {testing_directory} does not exists. Creating it now.\n"
        )
        os.makedirs(testing_directory)

    # MARK: Create Table

    for f in create_table_hql_files:
        path_to_file = f"{directory}/{f}"

        test_file_name = f"t1_{f}"
        path_to_test_file = f"{testing_directory}/{test_file_name}"

        with open(path_to_file, "r") as file:
            script = file.read()

        script_without_header = script.split("\n\n", 1)[1]  # split first line break

        # Don't put a space between the Better Comment keywords.
        space_after_header = (
            ""
            if script_without_header[: len("-- Attn:")] == "-- Attn:"
            or script_without_header[: len("-- Note:")] == "-- Note:"
            or script_without_header[: len("-- See:")] == "-- See:"
            else "\n"
        )

        test_data_dir = f.replace("create_table_", "").replace(".hql", "")

        test_script = (
            test_create_table_header.replace(SCRIPT_NAME, test_file_name)
            + space_after_header
            + script_without_header.replace(
                "${location}",
                f"hdfs://analytics-hadoop/tmp/wmde/analytics/test_data/{test_data_dir}/",
            )
        )[1:]

        with open(path_to_test_file, "w") as file:
            file.write(test_script)
            print(f"Wrote {test_file_name}")

    # MARK: DAG Tasks

    for f in dag_task_query_files:
        path_to_file = f"{directory}/{f}"

        test_file_name = f"t2_{f}"
        path_to_test_file = f"{testing_directory}/{test_file_name}"

        with open(path_to_file, "r") as file:
            script = file.read()

        script_without_header = script.split("\n\n", 1)[1]  # split first line break

        if dag_frequency == "yearly":
            time_periods_to_replace = "TEST_YEAR"

        elif dag_frequency == "monthly":
            time_periods_to_replace = "TEST_YEAR and TEST_MONTH"

        else:
            time_periods_to_replace = "TEST_YEAR, TEST_MONTH and TEST_DAY"

        # Don't put a space between the Better Comment keywords.
        space_after_header = (
            ""
            if script_without_header[: len("-- Attn:")] == "-- Attn:"
            or script_without_header[: len("-- Note:")] == "-- Note:"
            or script_without_header[: len("-- See:")] == "-- See:"
            else "\n"
        )

        # Create a dictionary of arguments and the values that should replace them.
        script_usage_str = script.split("-- Usage:")[1].split("\n\n", 1)[0]

        arg_to_value_strings = re.findall("-d .*=.*", script_usage_str)
        arg_to_value_strings = [
            s.replace("-d ", "").replace(" ", "").replace("\\", "")
            for s in arg_to_value_strings
        ]

        # Date arguments are handled by specific replacement below.
        arg_to_value_dict = {
            s.split("=")[0]: s.split("=")[1]
            for s in arg_to_value_strings
            if s.split("=")[0] not in ["year", "month", "day"]
        }

        script_with_testing_args = (
            script_without_header.replace("${year}", "TEST_YEAR")
            .replace("${month}", "TEST_MONTH")
            .replace("${day}", "TEST_DAY")
        )

        # Enforce that testing should be done on a private schema instead of within wmde.
        for k, v in arg_to_value_dict.items():
            script_with_testing_args = script_with_testing_args.replace(
                "${" + k + "}", v.replace("wmde.", "TEST_HIVE_USER_DB.")
            )

        test_script = (
            test_task_header.replace(SCRIPT_NAME, test_file_name).replace(
                "TEST_TIME_PERIODS", time_periods_to_replace
            )
            + space_after_header
            + script_with_testing_args
        )[1:]

        with open(path_to_test_file, "w") as file:
            file.write(test_script)
            print(f"Wrote {test_file_name}")

    # MARK: Gen CSV

    for f in gen_csv_hql_files:
        path_to_file = f"{directory}/{f}"

        test_file_name = f.replace("gen_csv", "t3_output_drop_table")
        path_to_test_file = f"{testing_directory}/{test_file_name}"

        with open(path_to_file, "r") as file:
            script = file.read()

        script_without_header = script.split("\n\n", 1)[1]  # split first line break

        # Don't put a space between the Better Comment keywords.
        space_after_header = (
            ""
            if script_without_header[: len("-- Attn:")] == "-- Attn:"
            or script_without_header[: len("-- Note:")] == "-- Note:"
            or script_without_header[: len("-- See:")] == "-- See:"
            else "\n"
        )

        # Create a dictionary of arguments and the values that should replace them.
        script_usage_str = script.split("-- Usage:")[1].split("\n\n", 1)[0]

        arg_to_value_strings = re.findall("-d .*=.*", script_usage_str)
        arg_to_value_strings = [
            s.replace("-d ", "").replace(" ", "").replace("\\", "")
            for s in arg_to_value_strings
        ]

        # No need to account for date arguments as above.
        arg_to_value_dict = {
            s.split("=")[0]: s.split("=")[1] for s in arg_to_value_strings
        }

        # Remove insert statement.
        script_with_testing_args = "-- MARK: Select" + re.sub(
            r"-- MARK\: Insert[\s\S]+?-- MARK\: Select", "", script_without_header
        )

        for k, v in arg_to_value_dict.items():
            script_with_testing_args = script_with_testing_args.replace(
                "${" + k + "}", v.replace("wmde.", "TEST_HIVE_USER_DB.")
            )

        script_with_testing_args = (
            script_with_testing_args.replace(
                "-- Spark hint to tell it to write only one file as an output for the job.\n",
                "",
            )
            .replace("SELECT /*+ COALESCE(1) */", "SELECT")
            .replace(
                ";",
                """
LIMIT
    5
;""",
            )
        )

        test_script = (
            test_output_header.replace(SCRIPT_NAME, test_file_name)
            + space_after_header
            + script_with_testing_args
            + test_output_footer.replace(
                "SOURCE_TABLE",
                arg_to_value_dict["source_table"].replace(
                    "wmde.", "TEST_HIVE_USER_DB."
                ),
            )
        )[1:]

        with open(path_to_test_file, "w") as file:
            file.write(test_script)
            print(f"Wrote {test_file_name}")

    # MARK: Print Success

    print(
        f"\n{WD_GREEN}Testing scripts generated successfully. Happy testing! {HEART_CHAR}{COLOR_RESET}"
    )


if __name__ == "__main__":
    main()
