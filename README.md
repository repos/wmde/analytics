# [Analytics](https://gitlab.wikimedia.org/repos/wmde/analytics)

> Research, tasks and automated jobs by Software Analytics at Wikimedia Deutschland
>
> Our Airflow DAGs: [data-engineering/airflow-dags/wmde](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde)

<!-- > Our team on Phabricator: [WMDE SWE Analytics](https://phabricator.wikimedia.org/project/profile/5408/) -->

### Contents

- [Docs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs)
  - Docs for regular processes and onboarding
- [HQL](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql)
  - Hive based automated jobs
- [Spark](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/spark)
  - Spark based automated jobs
- [Tasks](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks)
  - Research and tasks

## Setup

Please run the following commands in the project root to set up your [conda](https://conda.io/projects/conda/en/latest/user-guide/getting-started.html) development environment and [pre-commit](https://pre-commit.com/) hooks. See the [WMDE SWE Analytics contributing guide](https://gitlab.wikimedia.org/repos/wmde/analytics/-/blob/main/docs/CONTRIBUTING.md) for an in depth introduction to team processes.

### [conda env](https://conda.io/projects/conda/en/latest/user-guide/getting-started.html)

```bash
conda env create -f conda-environment.yaml
conda activate wmde-analytics
```

### [pre-commit](https://pre-commit.com/)

```bash
pip install pre-commit
pre-commit install
pre-commit run --all-files
```
