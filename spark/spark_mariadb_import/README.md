# [Spark MariaDB Import](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/spark/spark_mariadb_import)

See: https://gitlab.wikimedia.org/kcvelaga/pyspark-conda-mariadb

Import tables from Maria DB into Hive tables in the WMF data lake.
