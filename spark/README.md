# [Spark](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/spark)

> Spark based automated jobs
>
> Our Airflow DAGs: [data-engineering/airflow-dags/wmde](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde)

### Contents

- [Airflow Jobs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/spark/airflow_jobs)
  - All Spark jobs associated with [WMDE Airflow processes](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde)
- [Spark MariaDB Import](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/spark/spark_mariadb_import)
  - Import tables from Maria DB into the WMF data lake
