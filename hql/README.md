# [HQL](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql)

> Hive based automated jobs
>
> Our Airflow DAGs: [data-engineering/airflow-dags/wmde](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde)

### Contents

- [Airflow jobs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs)
  - All HQL jobs associated with [WMDE Airflow processes](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde)
- [WIP Airflow DAGs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/wip_airflow_dags)
  - WIP DAGs to be added to the [WMDE Airflow processes](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde)
