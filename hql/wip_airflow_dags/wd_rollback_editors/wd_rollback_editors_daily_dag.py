"""
Derives daily aggregates of Wikidata edits based on the device type of the user.

# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wd_rollback_editors
"""

from datetime import datetime, timedelta

from wmde.config.dag_config import (
    COMPUTE,
    DAILY,
    GEN_CSV,
    GITLAB_WMDE_HQL_JOBS_DIR,
    HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR,
    HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
    dataset,
    spark_sql_operator_default_args,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# MARK: Identifiers

WD_ROLLBACK_EDITORS = "wd_rollback_editors"  # task id
WD_ROLLBACK_EDITORS_DAILY = f"{WD_ROLLBACK_EDITORS}_{DAILY}"  # DAG id
GITLAB_HQL_TASK_DIR = f"{GITLAB_WMDE_HQL_JOBS_DIR}/{WD_ROLLBACK_EDITORS}"

# MARK: Properties

props = DagProperties(
    # HDFS source tables:
    hive_event_mediawiki_page_change_v1="event.mediawiki_page_change_v1",
    # HDFS destination table:
    hive_wmde_wd_rollback_editors_daily=f"wmde.{WD_ROLLBACK_EDITORS_DAILY}",
    # Task Hive query:
    hql_wd_rollback_editors_daily=f"{GITLAB_HQL_TASK_DIR}/{WD_ROLLBACK_EDITORS_DAILY}.hql",
    # TMP export directory:
    tmp_dir_wd_rollback_editors_daily=f"{HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR}/{WD_ROLLBACK_EDITORS_DAILY}",
    # TMP export query:
    hql_gen_csv_wd_rollback_editors_daily=f"{GITLAB_HQL_TASK_DIR}/{GEN_CSV}_{WD_ROLLBACK_EDITORS_DAILY}.hql",
    # Archive export directory:
    pub_data_dir_wd_rollback_editors_daily=(
        f"{HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR}/{WD_ROLLBACK_EDITORS_DAILY}"
    ),
    # Archive export file:
    archive_csv_wd_rollback_editors_daily=f"{WD_ROLLBACK_EDITORS_DAILY}.csv",
    # Metadata:
    start_date=datetime(2024, 7, 1),
    sla=timedelta(hours=6),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "daily",
        "from_hive",
        "requires_hive_event_mediawiki_page_change_v1",
        "requires_wmf_raw_mediawiki_user",
        "requires_wmf_raw_mediawiki_user_groups",
        "to_hive",
        "to_published_datasets",
        "uses_hql",
    ],
)

with create_easy_dag(
    dag_id=WD_ROLLBACK_EDITORS_DAILY,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: Sensors

    sensor_page_change = dataset("hive_event_mediawiki_page_change_v1").get_sensor_for(
        dag
    )

    # MARK: Compute Metrics

    compute = SparkSqlOperator(
        task_id=f"{COMPUTE}_{WD_ROLLBACK_EDITORS_DAILY}",
        sql=props.hql_wd_rollback_editors_daily,
        query_parameters={
            "source_hive_event_mw_page_change_v1": props.hive_event_mediawiki_page_change_v1,
            "destination_table": props.hive_wmde_wd_rollback_editors_daily,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
        **spark_sql_operator_default_args,
    )

    # MARK: Generate CSVs

    # gen_csv = SparkSqlOperator(
    #     task_id=f"{GEN_CSV}_{WD_ROLLBACK_EDITORS_DAILY}",
    #     sql=props.hql_gen_csv_wd_rollback_editors_daily,
    #     query_parameters={
    #         "source_table": props.hive_wmde_wd_rollback_editors_daily,
    #         "destination_directory": props.tmp_dir_wd_rollback_editors_daily,
    #     },
    # )

    # MARK: Archive Datasets

    # archive_csv = HDFSArchiveOperator(
    #     task_id=f"{ARCHIVE_CSV}_{WD_ROLLBACK_EDITORS_DAILY}",
    #     source_directory=props.tmp_dir_wd_rollback_editors_daily,
    #     archive_file=(
    #         props.pub_data_dir_wd_rollback_editors_daily + "/" + props.archive_csv_wd_rollback_editors_daily
    #     ),
    #     expected_filename_ending=".csv",
    #     check_done=True,
    # )

    # MARK: Execute DAG

    sensor_page_change >> compute  # >> gen_csv >> archive_csv
