"""
Tests for wd_rollback_editors_daily_dag.
"""

import pytest


# This fixture defines the dag_path for the shared dagbag one.
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return [
        "wmde",
        "dags",
        "wd_rollback_editors",
        "wd_rollback_editors_daily_dag.py",
    ]


def test_wd_rollback_editors_daily_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="wd_rollback_editors_daily")
    assert dag is not None
    assert len(dag.tasks) == 2  # without CSV export
    # assert len(dag.tasks) == 4  # with CSV export
