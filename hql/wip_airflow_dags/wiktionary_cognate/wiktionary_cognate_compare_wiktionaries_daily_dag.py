"""
Collect daily data on comparisons between the entries in all Wiktionaries.

# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wiktionary_cognate/compare_wiktionaries # noqa: E501, W505

Note: This DAG depends on wiktionary_cognate_hdfs_import_daily.

Note: The data for the corresponding jobs comes from the Wiktionary Cognate extension.
See: https://www.mediawiki.org/wiki/Extension:Cognate
"""

from datetime import datetime, timedelta

from airflow.sensors import ExternalTaskSensor

from wmde.config.dag_config import (
    ARCHIVE_CSV,
    COMPUTE,
    DAILY,
    GEN_CSV,
    GITLAB_WMDE_HQL_JOBS_DIR,
    HDFS_WMDE_ANALYTICS_TMP_DIR,
    PUBLISHED_DATASETS_DIR_WMDE_ANALYTICS,
    SENSOR,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
    spark_sql_operator_default_args,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

# MARK: Identifiers

WIKTIONARY_COGNATE = "wiktionary_cognate"  # global task id
COMPARE_WIKTIONARIES = "compare_wiktionaries"  # sub task id
WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES = (
    f"{WIKTIONARY_COGNATE}_{COMPARE_WIKTIONARIES}"  # task id
)
WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY = (
    f"{WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES}_{DAILY}"  # DAG id
)
GITLAB_HQL_TASK_DIR = (
    f"{GITLAB_WMDE_HQL_JOBS_DIR}/{WIKTIONARY_COGNATE}/{COMPARE_WIKTIONARIES}"
)

# Note: Upstream DAG
# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wiktionary_cognate/cognate_wiktionary_hdfs_import # noqa: E501, W505
WIKTIONARY_COGNATE_HDFS_IMPORT = f"{WIKTIONARY_COGNATE}_hdfs_import"
WIKTIONARY_COGNATE_HDFS_IMPORT_DAILY = f"{WIKTIONARY_COGNATE_HDFS_IMPORT}_daily"

# MARK: Properties

props = DagProperties(
    # HDFS source tables:
    hive_wiktionary_cognate_pages_daily="wmde.wiktionary_cognate_pages_daily",
    hive_wiktionary_cognate_sites_daily="wmde.wiktionary_cognate_sites_daily",
    hive_wiktionary_cognate_titles_daily="wmde.wiktionary_cognate_titles_daily",
    # HDFS destination table:
    hive_compare_wiktionaries_daily=f"wmde.{WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY}",
    # Task Hive query:
    hql_compare_wiktionaries_daily=(
        f"{GITLAB_HQL_TASK_DIR}/{WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY}.hql"
    ),
    # TMP export directory:
    tmp_dir_compare_wiktionaries_daily=f"{HDFS_WMDE_ANALYTICS_TMP_DIR}/{WIKTIONARY_COGNATE}/{COMPARE_WIKTIONARIES}",
    # TMP export query:
    hql_gen_csv_compare_wiktionaries_daily=(
        f"{GITLAB_HQL_TASK_DIR}/{GEN_CSV}_{WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY}.hql"
    ),
    # Archive export directory:
    pub_data_dir_compare_wiktionaries_daily=(
        f"{PUBLISHED_DATASETS_DIR_WMDE_ANALYTICS}/{WIKTIONARY_COGNATE}/{COMPARE_WIKTIONARIES}"
    ),
    # Archive export file:
    archive_csv_compare_wiktionaries_daily=f"{WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY}.csv",
    # Metadata:
    start_date=datetime(2024, 6, 10),
    sla=timedelta(hours=6),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "daily",
        "from_hive",
        "requires_wiktionary_cognate",
        "to_hive",
        "to_published_datasets",
        "uses_archiver",
        "uses_hql",
        "uses_spark",
    ],
)

with create_easy_dag(
    dag_id=WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: Sensor

    sensor_wiktionary_cognate_hdfs_import_daily_dag = ExternalTaskSensor(
        task_id=f"{SENSOR}_{WIKTIONARY_COGNATE_HDFS_IMPORT_DAILY}",
        external_dag_id=WIKTIONARY_COGNATE_HDFS_IMPORT_DAILY,
        external_task_id=WIKTIONARY_COGNATE_HDFS_IMPORT,
        timeout=600,
        allowed_states=["success"],
        failed_states=["failed", "skipped"],
        mode="reschedule",
    )

    # MARK: Compute Values

    compute_compare_wiktionaries = SparkSqlOperator(
        task_id=f"{COMPUTE}_{WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY}",
        sql=props.hql_compare_wiktionaries_uri,
        query_parameters={
            "src_hive_wiktionary_cognate_pages_daily": props.hive_wiktionary_cognate_pages_daily,
            "src_hive_wiktionary_cognate_sites_daily": props.hive_wiktionary_cognate_sites_daily,
            "src_hive_wiktionary_cognate_titles_daily": props.hive_wiktionary_cognate_titles_daily,
            "destination_table": props.hive_compare_wiktionaries_daily,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
        **spark_sql_operator_default_args,
    )

    # MARK: Generate CSV

    gen_csv_compare_wiktionaries = SparkSqlOperator(
        task_id=f"{GEN_CSV}_{WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY}",
        sql=props.hql_gen_csv_compare_wiktionaries,
        query_parameters={
            "source_table": props.hive_compare_wiktionaries_daily,
            "destination_directory": props.tmp_dir_compare_wiktionaries_daily,
        },
    )

    # MARK: Archive Dataset

    archive_csv_compare_wiktionaries = HDFSArchiveOperator(
        task_id=f"{ARCHIVE_CSV}_{WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY}",
        source_directory=props.hive_compare_wiktionaries_daily,
        archive_file=props.pub_data_dir_compare_wiktionaries_daily
        + "/"
        + props.archive_csv_compare_wiktionaries_daily,
        expected_filename_ending=".csv",
        check_done=True,
    )

    # MARK: Execute DAG

    (
        sensor_wiktionary_cognate_hdfs_import_daily_dag
        >> compute_compare_wiktionaries
        >> gen_csv_compare_wiktionaries
        >> archive_csv_compare_wiktionaries
    )
