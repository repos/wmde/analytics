"""
Daily DAG to clear data for the HDFS Wiktionary Cognate data that's older than seven days.

# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wiktionary_cognate/clear_tables

Note: This DAG depends on the following DAGs:
    - wiktionary_cognate_compare_wiktionaries_daily_dag
    - wiktionary_cognate_missing_entries_daily_dag
    - wiktionary_cognate_most_popular_daily_dag

Note: The data for the corresponding jobs comes from the Wiktionary Cognate extension.
See: https://www.mediawiki.org/wiki/Extension:Cognate
"""

from datetime import datetime, timedelta

from airflow.sensors import ExternalTaskSensor

from wmde.config.dag_config import (
    COMPUTE,
    DAILY,
    GITLAB_WMDE_HQL_JOBS_DIR,
    SENSOR,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
    spark_sql_operator_default_args,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# MARK: Identifiers

WIKTIONARY_COGNATE = "wiktionary_cognate"  # global task id
CLEAR_TABLES = "clear_tables"  # sub task id
WIKTIONARY_COGNATE_CLEAR_TABLES = f"{WIKTIONARY_COGNATE}_{CLEAR_TABLES}"  # task id
WIKTIONARY_COGNATE_CLEAR_TABLES_DAILY = f"{CLEAR_TABLES}_{DAILY}"
GITLAB_HQL_TASK_DIR = f"{GITLAB_WMDE_HQL_JOBS_DIR}/{WIKTIONARY_COGNATE}/{CLEAR_TABLES}"

# Note: Upstream DAGs
# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wiktionary_cognate

WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES = f"{WIKTIONARY_COGNATE}_compare_wiktionaries"
WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY = (
    f"{WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES}_{DAILY}"  # DAG id
)

WIKTIONARY_COGNATE_MISSING_ENTRIES = f"{WIKTIONARY_COGNATE}_missing_entries"
WIKTIONARY_COGNATE_MISSING_ENTRIES_DAILY = (
    f"{WIKTIONARY_COGNATE_MISSING_ENTRIES}_{DAILY}"  # DAG id
)
WIKTIONARY_COGNATE_MOST_POPULAR_ENTRIES = f"{WIKTIONARY_COGNATE}_most_popular_entries"
WIKTIONARY_COGNATE_MOST_POPULAR_ENTRIES_DAILY = (
    f"{WIKTIONARY_COGNATE_MOST_POPULAR_ENTRIES}_{DAILY}"  # DAG id
)

# MARK: Properties

props = DagProperties(
    # HDFS source tables:
    hive_wiktionary_cognate_pages_daily="wmde.wiktionary_cognate_pages_daily",
    hive_wiktionary_cognate_sites_daily="wmde.wiktionary_cognate_sites_daily",
    hive_wiktionary_cognate_titles_daily="wmde.wiktionary_cognate_titles_daily",
    # Process cleanup:
    hql_clear_tables_daily=f"{GITLAB_HQL_TASK_DIR}/{CLEAR_TABLES}/{WIKTIONARY_COGNATE_CLEAR_TABLES_DAILY}.hql",
    # Metadata:
    start_date=datetime(2024, 6, 10),
    sla=timedelta(hours=6),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "daily",
        "requires_wiktionary_cognate",
        "uses_hql",
        "uses_spark",
    ],
)

with create_easy_dag(
    dag_id=WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: Sensors

    sensor_wiktionary_cognate_compare_wiktionaries_daily_dag = ExternalTaskSensor(
        task_id=f"{SENSOR}_{WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY}",
        external_dag_id=WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES_DAILY,
        external_task_id=WIKTIONARY_COGNATE_COMPARE_WIKTIONARIES,
        timeout=600,
        allowed_states=["success"],
        failed_states=["failed", "skipped"],
        mode="reschedule",
    )

    sensor_wiktionary_cognate_missing_entries_daily_dag = ExternalTaskSensor(
        task_id=f"{SENSOR}_{WIKTIONARY_COGNATE_MISSING_ENTRIES_DAILY}",
        external_dag_id=WIKTIONARY_COGNATE_MISSING_ENTRIES_DAILY,
        external_task_id=WIKTIONARY_COGNATE_MISSING_ENTRIES,
        timeout=600,
        allowed_states=["success"],
        failed_states=["failed", "skipped"],
        mode="reschedule",
    )

    sensor_wiktionary_cognate_most_popular_daily_dag = ExternalTaskSensor(
        task_id=f"{SENSOR}_{WIKTIONARY_COGNATE_MOST_POPULAR_ENTRIES_DAILY}",
        external_dag_id=WIKTIONARY_COGNATE_MOST_POPULAR_ENTRIES_DAILY,
        external_task_id=WIKTIONARY_COGNATE_MOST_POPULAR_ENTRIES,
        timeout=600,
        allowed_states=["success"],
        failed_states=["failed", "skipped"],
        mode="reschedule",
    )

    # MARK: Clean Inputs

    # Note: We're cleaning data only past one week old to assure that backups are available.
    clear_old_wiktionary_cognate_data = SparkSqlOperator(
        task_id=f"{COMPUTE}_{WIKTIONARY_COGNATE_CLEAR_TABLES_DAILY}",
        sql=props.hql_clear_tables_uri,
        query_parameters={
            "hive_wiktionary_cognate_pages_daily": props.hive_wiktionary_cognate_pages_daily,
            "hive_wiktionary_cognate_sites_daily": props.hive_wiktionary_cognate_sites_daily,
            "hive_wiktionary_cognate_titles_daily": props.hive_wiktionary_cognate_titles_daily,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
        **spark_sql_operator_default_args,
    )

    # MARK: Execute DAG

    (
        [
            sensor_wiktionary_cognate_compare_wiktionaries_daily_dag,
            sensor_wiktionary_cognate_missing_entries_daily_dag,
            sensor_wiktionary_cognate_most_popular_daily_dag,
        ]
        >> clear_old_wiktionary_cognate_data
    )
