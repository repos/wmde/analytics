"""
Daily DAG to migrate Wiktionary Cognate extension data from MariaDB to HDFS.

Note: This DAG is in preparation for the following DAGs:
    - wiktionary_cognate_compare_wiktionaries_daily_dag
    - wiktionary_cognate_missing_entries_daily_dag
    - wiktionary_cognate_most_popular_daily_dag

Note: The data for the corresponding jobs comes from the Wiktionary Cognate extension.
See: https://www.mediawiki.org/wiki/Extension:Cognate
"""

from datetime import datetime, timedelta

from airflow.operators.python import PythonOperator

from wmde.config.dag_config import (
    GITLAB_WMDE_SPARK_JOBS_DIR,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
)
from wmf_airflow_common.config.dag_properties import DagProperties

# MARK: Identifiers

WIKTIONARY_COGNATE = "wiktionary_cognate"
TASK_ID = f"{WIKTIONARY_COGNATE}_hdfs_import"
DAG_ID = f"{TASK_ID}_daily"

# MARK: Properties

props = DagProperties(
    # MariaDB sources tables:
    mariadb_wiktionary_cognate_pages="cognate_pages",
    mariadb_wiktionary_cognate_sites="cognate_sites",
    mariadb_wiktionary_cognate_titles="cognate_titles",
    # HDFS destination tables:
    hive_wiktionary_cognate_pages_daily="wmde.wiktionary_cognate_pages_daily",
    hive_wiktionary_cognate_sites_daily="wmde.wiktionary_cognate_sites_daily",
    hive_wiktionary_cognate_titles_daily="wmde.wiktionary_cognate_titles_daily",
    # Task Spark jobs:
    split_datasets_job_path=f"{GITLAB_WMDE_SPARK_JOBS_DIR}/{WIKTIONARY_COGNATE}/{DAG_ID}.py",
    # Metadata:
    start_date=datetime(2024, 6, 10),
    sla=timedelta(hours=6),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "daily",
        "from_mariadb",
        "requires_wiktionary_cognate",
        "to_hive",
        "uses_spark",
    ],
)

with create_easy_dag(
    dag_id=DAG_ID,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: MariaDB Import

    wiktionary_cognate_hdfs_import = PythonOperator(
        task_id=TASK_ID,
    )

    # MARK: Execute DAG

    wiktionary_cognate_hdfs_import
