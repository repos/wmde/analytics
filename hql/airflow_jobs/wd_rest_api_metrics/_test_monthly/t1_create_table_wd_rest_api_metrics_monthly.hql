-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t1_create_table_wd_rest_api_metrics_monthly.hql

-- Note: This script is for testing and should be ran in your local Hive DB schema.

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wd_rest_api_metrics_monthly`(
        `month`                       date    COMMENT 'The month for which the metrics are computed over',
        `total_user_agents`           bigint  COMMENT 'Total distinct user agents making REST API requests in the given month',
        `total_filtered_user_agents`  bigint  COMMENT 'Total distinct user agents making REST API requests in the given month filtered for user agents found to have outlier activity',
        `total_ips`                   bigint  COMMENT 'Total distinct IPs making REST API requests in the given month'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    'hdfs://analytics-hadoop/tmp/wmde/analytics/test_data/wd_rest_api_metrics_monthly/'
;
