-- Parameters:
--     None
--
-- Usage:
--     hive -f t3_output_drop_table_wd_rest_api_metrics_monthly.hql

-- Note: Please replace TEST_HIVE_USER_DB for testing.

-- MARK: Select

SELECT
    month AS month,

    CASE
        WHEN
            total_user_agents >= 25
        THEN
            cast(total_user_agents AS STRING)
        ELSE
            '<25'
    END AS total_user_agents,

    CASE
        WHEN
            total_filtered_user_agents >= 25
        THEN
            cast(total_filtered_user_agents AS STRING)
        ELSE
            '<25'
    END AS total_filtered_user_agents,

    CASE
        WHEN
            total_ips >= 25
        THEN
            cast(total_ips AS STRING)
        ELSE
            '<25'
    END AS total_ips

FROM
    TEST_HIVE_USER_DB.wd_rest_api_metrics_monthly

ORDER BY
    month DESC

LIMIT
    5
;

-- MARK: Drop Table

-- Note: Save the output for your MR!
-- Note: If possible report the output in the Phabricator task (values <25 as '<25').
-- Note: Check HDFS to make sure that test data and metadata have been deleted.

DROP TABLE
    TEST_HIVE_USER_DB.wd_rest_api_metrics_monthly
;
