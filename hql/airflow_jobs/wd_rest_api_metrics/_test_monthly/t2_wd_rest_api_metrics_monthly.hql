-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t2_wd_rest_api_metrics_monthly.hql

-- Note: Please replace TEST_HIVE_USER_DB, TEST_YEAR and TEST_MONTH for testing.

-- MARK: Set Variables

SET hivevar:month_date = cast(
    concat(
        lpad(TEST_YEAR, 4, '0'),
        '-',
        lpad(TEST_MONTH, 2, '0'),
        '-',
        '01'
    ) AS DATE
)
;

-- MARK: Delete Data

-- Delete existing data for the period to prevent duplication of data in case of recomputation.
DELETE FROM
    TEST_HIVE_USER_DB.wd_rest_api_metrics_monthly

WHERE
    month = ${month_date}
;

-- MARK: Insert Data

INSERT INTO
    TEST_HIVE_USER_DB.wd_rest_api_metrics_monthly

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    ${month_date} AS month,

    count(DISTINCT user_agent) AS total_user_agents,

    count(
        DISTINCT CASE
            WHEN user_agent
            NOT LIKE 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/% (KHTML, like Gecko) Chrome/% Safari/%'
            THEN user_agent
        END
    ) AS total_filtered_user_agents,

    count(DISTINCT ip) AS total_ips

FROM
    wmf.webrequest

WHERE
    year = TEST_YEAR
    AND month = TEST_MONTH
    AND webrequest_source = 'text'
    AND normalized_host.project_family = 'wikidata'
    AND uri_path LIKE '/w/rest.php/wikibase/%'
;
