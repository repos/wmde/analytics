-- Parameters:
--     source_table             -- Fully qualified table name to compute the
--                                 aggregations for.
--     destination_directory    -- Fully qualified directory name that the file
--                                 should be exported to.
--
-- Usage:
--     spark3-sql -f gen_csv_wd_rest_api_metrics_monthly.hql                                       \
--         -d source_table=wmde.wd_rest_api_metrics_monthly                                        \
--         -d destination_directory=/wmf/tmp/wmde/analytics/airflow/wd_rest_api_metrics_monthly

-- MARK: Insert

INSERT OVERWRITE DIRECTORY
    '${destination_directory}'

USING CSV OPTIONS
    ('sep'=',', 'header'='true', 'compression'='none')

-- MARK: Select

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    month AS month,

    CASE
        WHEN
            total_user_agents >= 25
        THEN
            cast(total_user_agents AS STRING)
        ELSE
            '<25'
    END AS total_user_agents,

    CASE
        WHEN
            total_filtered_user_agents >= 25
        THEN
            cast(total_filtered_user_agents AS STRING)
        ELSE
            '<25'
    END AS total_filtered_user_agents,

    CASE
        WHEN
            total_ips >= 25
        THEN
            cast(total_ips AS STRING)
        ELSE
            '<25'
    END AS total_ips

FROM
    ${source_table}

ORDER BY
    month DESC
;
