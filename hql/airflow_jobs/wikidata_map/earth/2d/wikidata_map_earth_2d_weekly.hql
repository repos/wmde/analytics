-- Parameters:
--     source_table         -- Fully qualified table name to compute the
--                             aggregations for.
--     destination_table    -- Fully qualified table name to fill in
--                             aggregated values.
--     year                 -- Year of partition to compute aggregations
--                             for.
--     month                -- Month of partition to compute aggregations
--                             for.
--     day                  -- Day of partition to compute aggregations
--                             for.
--
-- Usage:
--     spark3-sql -f wikidata_map_earth_2d_weekly.hql                \
--         -d source_table=wmf.wikidata_entity                       \
--         -d destination_table=wmde.wikidata_map_earth_2d_weekly    \
--         -d year=2024                                              \
--         -d month=4                                                \
--         -d day=1

-- MARK: Set Variables

SET hivevar:week_date = cast(
    concat(
        lpad(${year}, 4, '0'),
        '-',
        lpad(${month}, 2, '0'),
        '-',
        lpad(${day}, 2, '0')
    ) AS DATE
)
;

-- MARK: Delete Data

-- Delete existing data for the period to prevent duplication of data in case of recomputation.
DELETE FROM
    ${destination_table}

WHERE
    week = ${week_date}
;

-- MARK: Insert Data

INSERT INTO
    ${destination_table}

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    ${week_date} AS week

FROM
    ${source_table} AS e

WHERE
    e.snapshot = ${week_date}
;
