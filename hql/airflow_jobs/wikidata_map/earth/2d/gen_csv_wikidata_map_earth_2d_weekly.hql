-- Parameters:
--     source_table             -- Fully qualified table name to compute the
--                                 aggregations for.
--     destination_directory    -- Fully qualified directory name that the file
--                                 should be exported to.
--
-- Usage:
--     spark3-sql -f gen_csv_wikidata_map_earth_2d_weekly.hql                                       \
--         -d source_table=wmde.wikidata_map_earth_2d_weekly                                        \
--         -d destination_directory=/wmf/tmp/wmde/analytics/airflow/wikidata_map_earth_2d_weekly

-- MARK: Insert

INSERT OVERWRITE DIRECTORY
    '${destination_directory}'

USING CSV OPTIONS
    ('sep'=',', 'header'='true', 'compression'='none')

-- MARK: Select

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    *

FROM
    ${source_table}

ORDER BY
    week DESC
;
