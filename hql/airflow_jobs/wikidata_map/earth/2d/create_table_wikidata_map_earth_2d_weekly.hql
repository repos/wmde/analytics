-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f create_table_wikidata_map_earth_2d_weekly.hql    \
--         --database wmde                                            \
--         -d location=/wmf/data/wmde/wikidata_map_earth_2d_weekly

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wikidata_map_earth_2d_weekly`(
        `week`  date    COMMENT 'The date at the start of the week for which the segments are computed'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    '${location}'
;
