-- Parameters:
--     source_mw_page_change_v1          -- Fully qualified table name to compute the
--                                          aggregations for.
--     source_mw_revision_tags_change    -- Fully qualified table name to compute the
--                                          aggregations for.
--     source_mw_private_cu_changes      -- Fully qualified table name to compute the
--                                          aggregations for.
--     destination_table                 -- Fully qualified table name to fill in
--                                          aggregated values.
--     year                              -- Year of partition to compute aggregations
--                                          for.
--     month                             -- Month of partition to compute aggregations
--                                          for.
--
-- Usage:
--     spark3-sql -f wd_device_type_edits_monthly.hql                                \
--         -d source_mw_page_change_v1=event.mediawiki_page_change_v1                \
--         -d source_mw_revision_tags_change=event.mediawiki_revision_tags_change    \
--         -d source_mw_private_cu_changes=wmf_raw.mediawiki_private_cu_changes      \
--         -d destination_table=wmde.wd_device_type_edits_monthly                    \
--         -d year=2024                                                              \
--         -d month=8

-- MARK: User Agent UDF

ADD JAR hdfs:///wmf/refinery/current/artifacts/refinery-hive-shaded.jar;
CREATE TEMPORARY FUNCTION ua_parser AS 'org.wikimedia.analytics.refinery.hive.GetUAPropertiesUDF';

-- MARK: Set Variables

SET hivevar:month_date = cast(
    concat(
        lpad(${year}, 4, '0'),
        '-',
        lpad(${month}, 2, '0'),
        '-',
        '01'
    ) AS DATE
)
;

SET hivevar:month_snapshot = concat(
    lpad(${year}, 4, '0'),
    '-',
    lpad(${month}, 2, '0')
)
;

-- MARK: Delete Data

-- Delete existing data for the period to prevent duplication of data in case of recomputation.
DELETE FROM
    ${destination_table}

WHERE
    month = ${month_date}
;

-- MARK: Revs and Tags

WITH revisions_with_tags AS (
    SELECT
        c.revision.rev_id AS rev_id,
        ua_parser(coalesce(cu.cuc_agent, '')).os_family AS os_family,
        concat(t.tags) AS tags

    FROM
        ${source_mw_page_change_v1} AS c

    -- Note: There is minor data loss from date segmentation in the joins.
    LEFT JOIN
        ${source_mw_private_cu_changes} AS cu

    ON
        c.revision.rev_id = cu.cuc_this_oldid
        AND cu.month = ${month_snapshot}
        AND cu.wiki_db = 'wikidatawiki'
        AND cu.cuc_namespace = 0

    LEFT JOIN
        ${source_mw_revision_tags_change} AS t

    ON
        c.revision.rev_id = t.rev_id
        AND t.year = ${year}
        AND t.month = ${month}
        AND t.database = 'wikidatawiki'
        AND t.page_namespace = 0
        AND t.page_is_redirect = false

    WHERE
        c.year = ${year}
        AND c.month = ${month}
        AND c.wiki_id = 'wikidatawiki'
        AND c.page.namespace_id = 0
        AND c.page.is_redirect = false

    GROUP BY
        c.revision.rev_id,
        ua_parser(coalesce(cu.cuc_agent, '')).os_family,
        concat(t.tags)
),

-- MARK: Revs and Flags

revisions_with_tag_flags AS (
    SELECT
        rev_id AS rev_id,

        coalesce(
            (
                os_family IN ('Android', 'iOS')
                AND (
                    (
                        array_contains(tags, 'wikidata-ui')
                        AND array_contains(tags, 'termbox')
                    )
                    OR (
                        array_contains(tags, 'mobile edit')
                        AND NOT array_contains(tags, 'mobile app edit')
                    )
                )
            ), false
        ) AS is_mobile_edit_mobile_ui,

        coalesce(
            (
                os_family IN ('Android', 'iOS')
                AND array_contains(tags, 'wikidata-ui')
                AND NOT array_contains(tags, 'termbox')
                AND NOT array_contains(tags, 'mobile edit')
            ), false
        ) AS is_mobile_edit_desktop_ui,

        coalesce(
            (
                NOT os_family IN ('Android', 'iOS')
                AND array_contains(tags, 'wikidata-ui')
                AND NOT array_contains(tags, 'termbox')
                AND NOT array_contains(tags, 'mobile edit')
            ), false
        ) AS is_desktop_edit_desktop_ui

    FROM
        revisions_with_tags
)

-- MARK: Insert Data

INSERT INTO
    ${destination_table}

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    ${month_date} AS month,

    count(rev_id) AS total_wd_edits,

    count(
        CASE
            WHEN
                is_mobile_edit_mobile_ui
            THEN
                rev_id
        END
    ) AS total_mobile_edits_mobile_ui,

    count(
        CASE
            WHEN
                is_mobile_edit_desktop_ui
            THEN
                rev_id
        END
    ) AS total_mobile_edits_desktop_ui,

    count(
        CASE
            WHEN
                is_desktop_edit_desktop_ui
            THEN
                rev_id
        END
    ) AS total_desktop_edits_desktop_ui,

    count(
        CASE
            WHEN
                NOT is_mobile_edit_mobile_ui
                AND NOT is_mobile_edit_desktop_ui
                AND NOT is_desktop_edit_desktop_ui
            THEN
                rev_id
        END
    ) AS total_edits_other

FROM
    revisions_with_tag_flags
;
