-- Parameters:
--     source_table             -- Fully qualified table name to compute the
--                                 aggregations for.
--     destination_directory    -- Fully qualified directory name that the file
--                                 should be exported to.
--
-- Usage:
--     spark3-sql -f gen_csv_wd_device_type_edits_monthly.hql                                       \
--         -d source_table=wmde.wd_device_type_edits_monthly                                        \
--         -d destination_directory=/wmf/tmp/wmde/analytics/airflow/wd_device_type_edits_monthly

-- MARK: Insert

INSERT OVERWRITE DIRECTORY
    '${destination_directory}'

USING CSV OPTIONS
    ('sep'=',', 'header'='true', 'compression'='none')

-- MARK: Select

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    day AS day,

    CASE
        WHEN
            total_wd_edits >= 25
        THEN
            cast(total_wd_edits AS STRING)
        ELSE
            '<25'
    END AS total_wd_edits,

    CASE
        WHEN
            total_mobile_edits_mobile_ui >= 25
        THEN
            cast(total_mobile_edits_mobile_ui AS STRING)
        ELSE
            '<25'
    END AS total_mobile_edits_mobile_ui,

    CASE
        WHEN
            total_mobile_edits_desktop_ui >= 25
        THEN
            cast(total_mobile_edits_desktop_ui AS STRING)
        ELSE
            '<25'
    END AS total_mobile_edits_desktop_ui,

    CASE
        WHEN
            total_desktop_edits_desktop_ui >= 25
        THEN
            cast(total_desktop_edits_desktop_ui AS STRING)
        ELSE
            '<25'
    END AS total_desktop_edits_desktop_ui,

    CASE
        WHEN
            total_edits_other >= 25
        THEN
            cast(total_edits_other AS STRING)
        ELSE
            '<25'
    END AS total_edits_other

FROM
    ${source_table}

ORDER BY
    day DESC
;
