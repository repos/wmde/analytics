-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t1_create_table_wd_device_type_edits_monthly.hql

-- Note: This script is for testing and should be ran in your local Hive DB schema.

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wd_device_type_edits_monthly`(
        `month`                             date    COMMENT 'The month for which the metrics are computed over',
        `total_wd_edits`                    bigint  COMMENT 'Total edits to Wikidata during the period',
        `total_mobile_edits_mobile_ui`      bigint  COMMENT 'Total edits to Wikidata from mobile devices using the mobile UI',
        `total_mobile_edits_desktop_ui`     bigint  COMMENT 'Total edits to Wikidata from mobile devices using the desktop UI',
        `total_desktop_edits_desktop_ui`    bigint  COMMENT 'Total edits to Wikidata from desktop devices using the desktop UI',
        `total_edits_other`                 bigint  COMMENT 'Total edits to Wikidata from unknown devices'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    'hdfs://analytics-hadoop/tmp/wmde/analytics/test_data/wd_device_type_edits_monthly/'
;
