-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t2_wd_device_type_edits_monthly.hql

-- Note: Please replace TEST_HIVE_USER_DB, TEST_YEAR and TEST_MONTH for testing.

-- MARK: User Agent UDF

ADD JAR hdfs:///wmf/refinery/current/artifacts/refinery-hive-shaded.jar;
CREATE TEMPORARY FUNCTION ua_parser AS 'org.wikimedia.analytics.refinery.hive.GetUAPropertiesUDF';

-- MARK: Set Variables

SET hivevar:month_date = cast(
    concat(
        lpad(TEST_YEAR, 4, '0'),
        '-',
        lpad(TEST_MONTH, 2, '0'),
        '-',
        '01'
    ) AS DATE
)
;

SET hivevar:month_snapshot = concat(
    lpad(TEST_YEAR, 4, '0'),
    '-',
    lpad(TEST_MONTH, 2, '0')
)
;

-- MARK: Delete Data

-- Delete existing data for the period to prevent duplication of data in case of recomputation.
DELETE FROM
    TEST_HIVE_USER_DB.wd_device_type_edits_monthly

WHERE
    month = ${month_date}
;

-- MARK: Revs and Tags

WITH revisions_with_tags AS (
    SELECT
        c.revision.rev_id AS rev_id,
        ua_parser(coalesce(cu.cuc_agent, '')).os_family AS os_family,
        concat(t.tags) AS tags

    FROM
        event.mediawiki_page_change_v1 AS c

    -- Note: There is minor data loss from date segmentation in the joins.
    LEFT JOIN
        wmf_raw.mediawiki_private_cu_changes AS cu

    ON
        c.revision.rev_id = cu.cuc_this_oldid
        AND cu.month = ${month_snapshot}
        AND cu.wiki_db = 'wikidatawiki'
        AND cu.cuc_namespace = 0

    LEFT JOIN
        event.mediawiki_revision_tags_change AS t

    ON
        c.revision.rev_id = t.rev_id
        AND t.year = TEST_YEAR
        AND t.month = TEST_MONTH
        AND t.database = 'wikidatawiki'
        AND t.page_namespace = 0
        AND t.page_is_redirect = false

    WHERE
        c.year = TEST_YEAR
        AND c.month = TEST_MONTH
        AND c.wiki_id = 'wikidatawiki'
        AND c.page.namespace_id = 0
        AND c.page.is_redirect = false

    GROUP BY
        c.revision.rev_id,
        ua_parser(coalesce(cu.cuc_agent, '')).os_family,
        concat(t.tags)
),

-- MARK: Revs and Flags

revisions_with_tag_flags AS (
    SELECT
        rev_id AS rev_id,

        coalesce(
            (
                os_family IN ('Android', 'iOS')
                AND (
                    (
                        array_contains(tags, 'wikidata-ui')
                        AND array_contains(tags, 'termbox')
                    )
                    OR (
                        array_contains(tags, 'mobile edit')
                        AND NOT array_contains(tags, 'mobile app edit')
                    )
                )
            ), false
        ) AS is_mobile_edit_mobile_ui,

        coalesce(
            (
                os_family IN ('Android', 'iOS')
                AND array_contains(tags, 'wikidata-ui')
                AND NOT array_contains(tags, 'termbox')
                AND NOT array_contains(tags, 'mobile edit')
            ), false
        ) AS is_mobile_edit_desktop_ui,

        coalesce(
            (
                NOT os_family IN ('Android', 'iOS')
                AND array_contains(tags, 'wikidata-ui')
                AND NOT array_contains(tags, 'termbox')
                AND NOT array_contains(tags, 'mobile edit')
            ), false
        ) AS is_desktop_edit_desktop_ui

    FROM
        revisions_with_tags
)

-- MARK: Insert Data

INSERT INTO
    TEST_HIVE_USER_DB.wd_device_type_edits_monthly

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    ${month_date} AS month,

    count(rev_id) AS total_wd_edits,

    count(
        CASE
            WHEN
                is_mobile_edit_mobile_ui
            THEN
                rev_id
        END
    ) AS total_mobile_edits_mobile_ui,

    count(
        CASE
            WHEN
                is_mobile_edit_desktop_ui
            THEN
                rev_id
        END
    ) AS total_mobile_edits_desktop_ui,

    count(
        CASE
            WHEN
                is_desktop_edit_desktop_ui
            THEN
                rev_id
        END
    ) AS total_desktop_edits_desktop_ui,

    count(
        CASE
            WHEN
                NOT is_mobile_edit_mobile_ui
                AND NOT is_mobile_edit_desktop_ui
                AND NOT is_desktop_edit_desktop_ui
            THEN
                rev_id
        END
    ) AS total_edits_other

FROM
    revisions_with_tag_flags
;
