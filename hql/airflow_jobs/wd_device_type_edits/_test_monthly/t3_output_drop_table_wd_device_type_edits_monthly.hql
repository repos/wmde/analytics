-- Parameters:
--     None
--
-- Usage:
--     hive -f t3_output_drop_table_wd_device_type_edits_monthly.hql

-- Note: Please replace TEST_HIVE_USER_DB for testing.

-- MARK: Select

SELECT
    day AS day,

    CASE
        WHEN
            total_wd_edits >= 25
        THEN
            cast(total_wd_edits AS STRING)
        ELSE
            '<25'
    END AS total_wd_edits,

    CASE
        WHEN
            total_mobile_edits_mobile_ui >= 25
        THEN
            cast(total_mobile_edits_mobile_ui AS STRING)
        ELSE
            '<25'
    END AS total_mobile_edits_mobile_ui,

    CASE
        WHEN
            total_mobile_edits_desktop_ui >= 25
        THEN
            cast(total_mobile_edits_desktop_ui AS STRING)
        ELSE
            '<25'
    END AS total_mobile_edits_desktop_ui,

    CASE
        WHEN
            total_desktop_edits_desktop_ui >= 25
        THEN
            cast(total_desktop_edits_desktop_ui AS STRING)
        ELSE
            '<25'
    END AS total_desktop_edits_desktop_ui,

    CASE
        WHEN
            total_edits_other >= 25
        THEN
            cast(total_edits_other AS STRING)
        ELSE
            '<25'
    END AS total_edits_other

FROM
    TEST_HIVE_USER_DB.wd_device_type_edits_monthly

ORDER BY
    day DESC

LIMIT
    5
;

-- MARK: Drop Table

-- Note: Save the output for your MR!
-- Note: If possible report the output in the Phabricator task (values <25 as '<25').
-- Note: Check HDFS to make sure that test data and metadata have been deleted.

DROP TABLE
    TEST_HIVE_USER_DB.wd_device_type_edits_monthly
;
