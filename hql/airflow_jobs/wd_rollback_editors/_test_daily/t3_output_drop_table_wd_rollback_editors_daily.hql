-- Parameters:
--     None
--
-- Usage:
--     hive -f t3_output_drop_table_wd_rollback_editors_daily.hql

-- Note: Please replace TEST_HIVE_USER_DB for testing.

SELECT
    *

FROM
    TEST_HIVE_USER_DB.wd_rollback_editors_daily

ORDER BY
    day DESC
;

-- Note: Save the output for your MR!
-- Note: If possible report the output in the Phabricator task (values <25 as '<25').
-- Note: Check HDFS to make sure that test data and metadata have been deleted.

DROP TABLE
    TEST_HIVE_USER_DB.wd_rollback_editors_daily
;
