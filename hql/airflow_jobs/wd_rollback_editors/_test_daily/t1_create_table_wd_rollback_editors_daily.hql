-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t1_create_table_wd_rollback_editors_daily.hql

-- Note: This script is for testing and should be ran in your local Hive DB schema.

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wd_rollback_editors_daily`(
        `day`                              date    COMMENT 'The day for which the metrics are computed over',
        `total_rollbackers`                bigint  COMMENT 'Total Wikidata users with rollback privaleges',
        `total_active_editor_rollbackers`  bigint  COMMENT 'Total Wikidata users with rollback privaleges who are also active editors'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    'hdfs://analytics-hadoop/tmp/wmde/analytics/test_data/wd_rollback_editors_daily/'
;
