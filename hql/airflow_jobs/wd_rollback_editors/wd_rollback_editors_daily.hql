-- Parameters:
--     source_table         -- Fully qualified table name to compute the
--                             aggregations for.
--     destination_table    -- Fully qualified table name to fill in
--                             aggregated values.
--     year                 -- Year of partition to compute aggregations
--                             for.
--     month                -- Month of partition to compute aggregations
--                             for.
--     day                  -- Day of partition to compute aggregations
--                             for.
--
-- Usage:
--     spark3-sql -f wd_rollback_editors_daily.hql                \
--         -d source_table=event.mediawiki_revision_create        \
--         -d destination_table=wmde.wd_rollback_editors_daily    \
--         -d year=2024                                           \
--         -d month=8                                             \
--         -d day=1

-- MARK: Set Variables

SET hivevar:day_date = cast(
    concat(
        lpad(${year}, 4, '0'),
        '-',
        lpad(${month}, 2, '0'),
        '-',
        lpad(${day}, 2, '0')
    ) AS DATE
)
;

-- MARK: Delete Data

-- Delete existing data for the period to prevent duplication of data in case of recomputation.
DELETE FROM
    ${destination_table}

WHERE
    day = ${day_date}
;

-- MARK: Insert Data

INSERT INTO
    ${destination_table}

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    ${day_date} AS day

FROM
    ${source_table} AS c
;
