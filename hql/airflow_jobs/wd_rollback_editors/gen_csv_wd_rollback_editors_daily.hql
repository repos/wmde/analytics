-- Parameters:
--     source_table             -- Fully qualified table name to compute the
--                                 aggregations for.
--     destination_directory    -- Fully qualified directory name that the file
--                                 should be exported to.
--
-- Usage:
--     spark3-sql -f gen_csv_wd_rollback_editors_daily.hql                                      \
--         -d source_table=wmde.wd_rollback_editors_daily                                       \
--         -d destination_directory=/wmf/tmp/wmde/analytics/airflow/wd_rollback_editors_daily

-- MARK: Insert

INSERT OVERWRITE DIRECTORY
    '${destination_directory}'

USING CSV OPTIONS
    ('sep'=',', 'header'='true', 'compression'='none')

-- MARK: Select

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    day AS day,

    CASE
        WHEN
            total_rollbackers >= 25
        THEN
            cast(total_rollbackers AS STRING)
        ELSE
            '<25'
    END AS total_rollbackers,

    CASE
        WHEN
            total_active_editor_rollbackers >= 25
        THEN
            cast(total_active_editor_rollbackers AS STRING)
        ELSE
            '<25'
    END AS total_active_editor_rollbackers

FROM
    ${source_table}

ORDER BY
    day DESC
;
