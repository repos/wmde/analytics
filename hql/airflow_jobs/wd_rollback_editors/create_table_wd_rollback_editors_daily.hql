-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f create_table_wd_rollback_editors_daily.hql    \
--         --database wmde                                         \
--         -d location=/wmf/data/wmde/wd_rollback_editors_daily

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wd_rollback_editors_daily`(
        `day`                              date    COMMENT 'The day for which the metrics are computed over',
        `total_rollbackers`                bigint  COMMENT 'Total Wikidata users with rollback privaleges',
        `total_active_editor_rollbackers`  bigint  COMMENT 'Total Wikidata users with rollback privaleges who are also active editors'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    '${location}'
;
