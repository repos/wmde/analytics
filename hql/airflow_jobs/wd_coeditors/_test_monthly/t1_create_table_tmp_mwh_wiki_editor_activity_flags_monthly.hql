-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t1_create_table_tmp_mwh_wiki_editor_activity_flags_monthly.hql

-- Note: This script is for testing and should be ran in your local Hive DB schema.

CREATE EXTERNAL TABLE IF NOT EXISTS
    `tmp_mwh_wiki_editor_activity_flags_monthly`(
        `wiki`                               string   COMMENT 'The wiki for which the user activity flags have been calculated for',
        `event_user_text`                    string   COMMENT 'The text to identify the user of the given wiki',
        `is_casual_editor`                   boolean  COMMENT 'Whether the user has between 1 and 4 edits',
        `is_casual_content_editor`           boolean  COMMENT 'Whether the user has between 1 and 4 content edits',
        `is_casual_non_content_editor`       boolean  COMMENT 'Whether the user has between 1 and 4 non-content edits',
        `is_active_editor`                   boolean  COMMENT 'Whether the user has between 5 and 99 edits',
        `is_active_content_editor`           boolean  COMMENT 'Whether the user has between 5 and 99 content edits',
        `is_active_non_content_editor`       boolean  COMMENT 'Whether the user has between 5 and 99 non-content edits',
        `is_very_active_editor`              boolean  COMMENT 'Whether the user has 100 or more edits',
        `is_very_active_content_editor`      boolean  COMMENT 'Whether the user has 100 or more content edits',
        `is_very_active_non_content_editor`  boolean  COMMENT 'Whether the user has 100 or more non-content edits'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    'hdfs://analytics-hadoop/tmp/wmde/analytics/test_data/tmp_mwh_wiki_editor_activity_flags_monthly/'
;
