-- Parameters:
--     None
--
-- Usage:
--     hive -f t3_output_drop_table_wd_coeditors_by_wiki_monthly.hql

-- Note: Please replace TEST_HIVE_USER_DB for testing.

-- MARK: Select

SELECT
    month AS month,

    wiki AS wiki,

    CASE
        WHEN
            total_casual_wd_coeditors >= 25
        THEN
            cast(total_casual_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_casual_wd_coeditors,

    CASE
        WHEN
            total_casual_content_wd_coeditors >= 25
        THEN
            cast(total_casual_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_casual_content_wd_coeditors,

    CASE
        WHEN
            total_casual_non_content_wd_coeditors >= 25
        THEN
            cast(total_casual_non_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_casual_non_content_wd_coeditors,

    CASE
        WHEN
            total_active_wd_coeditors >= 25
        THEN
            cast(total_active_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_active_wd_coeditors,

    CASE
        WHEN
            total_active_content_wd_coeditors >= 25
        THEN
            cast(total_active_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_active_content_wd_coeditors,

    CASE
        WHEN
            total_active_non_content_wd_coeditors >= 25
        THEN
            cast(total_active_non_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_active_non_content_wd_coeditors,

    CASE
        WHEN
            total_very_active_wd_coeditors >= 25
        THEN
            cast(total_very_active_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_very_active_wd_coeditors,

    CASE
        WHEN
            total_very_active_content_wd_coeditors >= 25
        THEN
            cast(total_very_active_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_very_active_content_wd_coeditors,

    CASE
        WHEN
            total_very_active_non_content_wd_coeditors >= 25
        THEN
            cast(total_very_active_non_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_very_active_non_content_wd_coeditors

FROM
    TEST_HIVE_USER_DB.wd_coeditors_by_wiki_monthly

ORDER BY
    month DESC,
    wiki ASC

LIMIT
    5
;

-- MARK: Drop Table

-- Note: Save the output for your MR!
-- Note: If possible report the output in the Phabricator task (values <25 as '<25').
-- Note: Check HDFS to make sure that test data and metadata have been deleted.

DROP TABLE
    TEST_HIVE_USER_DB.wd_coeditors_by_wiki_monthly
;
