-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f create_table_wd_coeditors_distinct_monthly.hql    \
--         --database wmde                                             \
--         -d location=/wmf/data/wmde/wd_coeditors_distinct_monthly

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wd_coeditors_distinct_monthly`(
        `month`                                       date    COMMENT 'The month for which the metrics are computed over',
        `total_casual_wd_coeditors`                   bigint  COMMENT 'The total number of users editing on Wikidata and another wiki with between 1 and 4 edits in either wiki',
        `total_casual_content_wd_coeditors`           bigint  COMMENT 'The total number of users editing content pages on Wikidata and another wiki with between 1 and 4 edits in either wiki',
        `total_casual_non_content_wd_coeditors`       bigint  COMMENT 'The total number of users editing non-content pages on Wikidata and another wiki with between 1 and 4 edits in either wiki',
        `total_active_wd_coeditors`                   bigint  COMMENT 'The total number of users editing on Wikidata and another wiki with between 5 and 99 edits in both wikis',
        `total_active_content_wd_coeditors`           bigint  COMMENT 'The total number of users editing content pages on Wikidata and another wiki with between 5 and 99 edits in both wikis',
        `total_active_non_content_wd_coeditors`       bigint  COMMENT 'The total number of users editing non-content pages on Wikidata and another wiki with between 5 and 99 edits in both wikis',
        `total_very_active_wd_coeditors`              bigint  COMMENT 'The total number of users editing on Wikidata and another wiki with >=100 edits in both wikis',
        `total_very_active_content_wd_coeditors`      bigint  COMMENT 'The total number of users editing content pages on Wikidata and another wiki with >=100 edits in both wikis',
        `total_very_active_non_content_wd_coeditors`  bigint  COMMENT 'The total number of users editing non-content pages on Wikidata and another wiki with >=100 edits in both wikis'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    '${location}'
;
