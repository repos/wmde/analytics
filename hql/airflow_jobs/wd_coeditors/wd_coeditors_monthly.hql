-- Parameters:
--     source_table                                    -- Fully qualified table name to compute the
--                                                        aggregations for.
--     tmp_mwh_wiki_editor_activity_flags_monthly      -- Fully qualified table name to store the
--                                                        partial stage results to.
--     dest_hive_wmde_wd_coeditors_by_wiki_monthly     -- Fully qualified table name to fill in
--                                                        distinct aggregated values.
--     dest_hive_wmde_wd_coeditors_distinct_monthly    -- Fully qualified table name to fill in
--                                                        by wiki aggregated values.
--     year                                            -- Year of partition to compute aggregations
--                                                        for.
--     month                                           -- Month of partition to compute aggregations
--                                                        for.
--
-- Usage:
--     spark3-sql -f wd_coeditors_monthly.hql                                                               \
--         -d source_table=wmf.mediawiki_history                                                            \
--         -d tmp_mwh_wiki_editor_activity_flags_monthly=wmde.tmp_mwh_wiki_editor_activity_flags_monthly    \
--         -d dest_hive_wmde_wd_coeditors_by_wiki_monthly=wmde.wd_coeditors_by_wiki_monthly                 \
--         -d dest_hive_wmde_wd_coeditors_distinct_monthly=wmde.wd_coeditors_distinct_monthly               \
--         -d year=2024                                                                                     \
--         -d month=6

-- Attn: This DAG relies on a temporary table. Separate runs cannot run in parallel as data will leak or be deleted.

-- MARK: Set Variables

SET hivevar:month_date = cast(
    concat(
        lpad(${year}, 4, '0'),
        '-',
        lpad(${month}, 2, '0'),
        '-',
        '01'
    ) AS DATE
)
;

SET hivevar:month_snapshot = concat(
    lpad(${year}, 4, '0'),
    '-',
    lpad(${month}, 2, '0')
)
;

-- MARK: Delete Data

-- Delete existing data for the period to prevent duplication of data in case of recomputation.
DELETE FROM
    ${tmp_mwh_wiki_editor_activity_flags_monthly}

WHERE
    1=1
;

DELETE FROM
    ${dest_hive_wmde_wd_coeditors_by_wiki_monthly}

WHERE
    month = ${month_date}
;

DELETE FROM
    ${dest_hive_wmde_wd_coeditors_distinct_monthly}

WHERE
    month = ${month_date}
;

-- MARK: Edits by Wiki

WITH mw_history_content_type_edits_by_wiki AS (
    SELECT
        wiki_db AS wiki,

        event_user_text AS event_user_text,

        count(event_user_text) AS total_edits,

        count(
            CASE
                WHEN
                    page_namespace_is_content
                THEN
                    event_user_text
            END
        ) AS total_content_edits,

        count(
            CASE
                WHEN
                    NOT page_namespace_is_content
                THEN
                    event_user_text
            END
        ) AS total_non_content_edits

    FROM
        ${source_table}

    WHERE
        snapshot = ${month_snapshot}
        AND event_timestamp LIKE concat(${month_snapshot}, '%')
        AND event_entity = 'revision'
        AND event_type = 'create'
        AND NOT event_user_is_anonymous
        -- Note: Needed after temporary accounts are integrated.
        -- AND NOT user_is_temp
        AND NOT array_contains(event_user_groups, 'bot')
        AND NOT array_contains(event_user_groups, 'botadmin')
        AND NOT array_contains(event_user_groups, 'copyviobot')
        AND NOT array_contains(event_user_groups, 'flow-bot')
        AND NOT revision_is_deleted_by_page_deletion

    GROUP BY
        wiki,
        event_user_text
)

-- MARK: Activity Flags

INSERT INTO
    ${tmp_mwh_wiki_editor_activity_flags_monthly}

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    wiki AS wiki,
    event_user_text AS event_user_text,
    total_edits BETWEEN 1 AND 4 AS is_casual_editor,
    total_content_edits BETWEEN 1 AND 4 AS is_casual_content_editor,
    total_non_content_edits BETWEEN 1 AND 4 AS is_casual_non_content_editor,
    total_edits BETWEEN 5 AND 99 AS is_active_editor,
    total_content_edits BETWEEN 5 AND 99 AS is_active_content_editor,
    total_non_content_edits BETWEEN 5 AND 99 AS is_active_non_content_editor,
    total_edits >= 100 AS is_very_active_editor,
    total_content_edits >= 100 AS is_very_active_content_editor,
    total_non_content_edits >= 100 AS is_very_active_non_content_editor

FROM
    mw_history_content_type_edits_by_wiki
;

-- MARK: Insert By Wiki

INSERT INTO
    ${dest_hive_wmde_wd_coeditors_by_wiki_monthly}

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    ${month_date} AS month,

    w.wiki AS wiki,

    -- MARK: Casual

    count(
        DISTINCT
        CASE
            WHEN
                w.is_casual_editor OR wd.is_casual_editor
            THEN
                w.event_user_text
        END
    ) AS total_casual_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                w.is_casual_content_editor OR wd.is_casual_content_editor
            THEN
                w.event_user_text
        END
    ) AS total_casual_content_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                w.is_casual_non_content_editor OR wd.is_casual_non_content_editor
            THEN
                w.event_user_text
        END
    ) AS total_casual_non_content_wd_coeditors,

    -- MARK: Active

    count(
        DISTINCT
        CASE
            WHEN
                (
                    w.is_active_editor
                    AND wd.is_active_editor
                )
                OR (
                    w.is_active_editor
                    AND wd.is_very_active_editor
                )
                OR (
                    w.is_very_active_editor
                    AND wd.is_active_editor
                )
            THEN
                w.event_user_text
        END
    ) AS total_active_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                (
                    w.is_active_content_editor
                    AND wd.is_active_content_editor
                )
                OR (
                    w.is_active_content_editor
                    AND wd.is_very_active_content_editor
                )
                OR (
                    w.is_very_active_content_editor
                    AND wd.is_active_content_editor
                )
            THEN
                w.event_user_text
        END
    ) AS total_active_content_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                (
                    w.is_active_non_content_editor
                    AND wd.is_active_non_content_editor
                )
                OR (
                    w.is_active_non_content_editor
                    AND wd.is_very_active_non_content_editor
                )
                OR (
                    w.is_very_active_non_content_editor
                    AND wd.is_active_non_content_editor
                )
            THEN
                w.event_user_text
        END
    ) AS total_active_non_content_wd_coeditors,

    -- MARK: Very Active

    count(
        DISTINCT
        CASE
            WHEN
                w.is_very_active_editor AND wd.is_very_active_editor
            THEN
                w.event_user_text
        END
    ) AS total_very_active_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                w.is_very_active_content_editor AND wd.is_very_active_content_editor
            THEN
                w.event_user_text
        END
    ) AS total_very_active_content_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                w.is_very_active_non_content_editor AND wd.is_very_active_non_content_editor
            THEN
                w.event_user_text
        END
    ) AS total_very_active_non_content_wd_coeditors

FROM
    ${tmp_mwh_wiki_editor_activity_flags_monthly} AS w

JOIN
    ${tmp_mwh_wiki_editor_activity_flags_monthly} AS wd

ON
    w.event_user_text = wd.event_user_text
    AND wd.wiki = 'wikidatawiki'

WHERE
    w.wiki != 'wikidatawiki'

GROUP BY
    w.wiki

ORDER BY
    w.wiki ASC
;

-- MARK: Insert Distinct

INSERT INTO
    ${dest_hive_wmde_wd_coeditors_distinct_monthly}

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    ${month_date} AS month,

    -- MARK: Casual

    count(
        DISTINCT
        CASE
            WHEN
                w.is_casual_editor OR wd.is_casual_editor
            THEN
                w.event_user_text
        END
    ) AS total_casual_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                w.is_casual_content_editor OR wd.is_casual_content_editor
            THEN
                w.event_user_text
        END
    ) AS total_casual_content_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                w.is_casual_non_content_editor OR wd.is_casual_non_content_editor
            THEN
                w.event_user_text
        END
    ) AS total_casual_non_content_wd_coeditors,

    -- MARK: Active

    count(
        DISTINCT
        CASE
            WHEN
                (
                    w.is_active_editor
                    AND wd.is_active_editor
                )
                OR (
                    w.is_active_editor
                    AND wd.is_very_active_editor
                )
                OR (
                    w.is_very_active_editor
                    AND wd.is_active_editor
                )
            THEN
                w.event_user_text
        END
    ) AS total_active_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                (
                    w.is_active_content_editor
                    AND wd.is_active_content_editor
                )
                OR (
                    w.is_active_content_editor
                    AND wd.is_very_active_content_editor
                )
                OR (
                    w.is_very_active_content_editor
                    AND wd.is_active_content_editor
                )
            THEN
                w.event_user_text
        END
    ) AS total_active_content_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                (
                    w.is_active_non_content_editor
                    AND wd.is_active_non_content_editor
                )
                OR (
                    w.is_active_non_content_editor
                    AND wd.is_very_active_non_content_editor
                )
                OR (
                    w.is_very_active_non_content_editor
                    AND wd.is_active_non_content_editor
                )
            THEN
                w.event_user_text
        END
    ) AS total_active_non_content_wd_coeditors,

    -- MARK: Very Active

    count(
        DISTINCT
        CASE
            WHEN
                w.is_very_active_editor AND wd.is_very_active_editor
            THEN
                w.event_user_text
        END
    ) AS total_very_active_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                w.is_very_active_content_editor AND wd.is_very_active_content_editor
            THEN
                w.event_user_text
        END
    ) AS total_very_active_content_wd_coeditors,

    count(
        DISTINCT
        CASE
            WHEN
                w.is_very_active_non_content_editor AND wd.is_very_active_non_content_editor
            THEN
                w.event_user_text
        END
    ) AS total_very_active_non_content_wd_coeditors

FROM
    ${tmp_mwh_wiki_editor_activity_flags_monthly} AS w

JOIN
    ${tmp_mwh_wiki_editor_activity_flags_monthly} AS wd

ON
    w.event_user_text = wd.event_user_text
    AND wd.wiki = 'wikidatawiki'

WHERE
    w.wiki != 'wikidatawiki'
;

-- MARK: Delete TMP Data

DELETE FROM
    ${tmp_mwh_wiki_editor_activity_flags_monthly}

WHERE
    1=1
;
