# [Wikidata Coeditors](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wd_coeditors/WIKIDATA_COEDITORS.md)

The following defines the breakdown of coeditors based on their activity level on Wikidata and another wiki.

## Table Breakdown

| editor activity level  |    wd casual    |    wd active    |    wd very active    |
| :--------------------: | :-------------: | :-------------: | :------------------: |
|   other wiki casual    | casual coeditor | casual coeditor |   casual coeditor    |
|   other wiki active    | casual coeditor | active coeditor |   active coeditor    |
| other wiki very active | casual coeditor | active coeditor | very active coeditor |

## Logical Breakdown

### Casual Coeditor

Five of nine combinations based on Wikidata casual `OR` other wiki casual.

### Active Coeditor

Three of nine combinations:

- Wikidata active `AND` other wiki active
- Wikidata very active `AND` other wiki active
- Wikidata active `AND` other wiki very active

### Very Active Coeditor

One of nine combinations based on Wikidata very active `AND` other wiki very active.
