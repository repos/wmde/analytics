-- Parameters:
--     source_table             -- Fully qualified table name to compute the
--                                 aggregations for.
--     destination_directory    -- Fully qualified directory name that the file
--                                 should be exported to.
--
-- Usage:
--     spark3-sql -f gen_csv_wd_coeditors_distinct_monthly.hql                                       \
--         -d source_table=wmde.wd_coeditors_distinct_monthly                                        \
--         -d destination_directory=/wmf/tmp/wmde/analytics/airflow/wd_coeditors_distinct_monthly

-- MARK: Insert

INSERT OVERWRITE DIRECTORY
    '${destination_directory}'

USING CSV OPTIONS
    ('sep'=',', 'header'='true', 'compression'='none')

-- MARK: Select

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    month AS month,

    CASE
        WHEN
            total_casual_wd_coeditors >= 25
        THEN
            cast(total_casual_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_casual_wd_coeditors,

    CASE
        WHEN
            total_casual_content_wd_coeditors >= 25
        THEN
            cast(total_casual_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_casual_content_wd_coeditors,

    CASE
        WHEN
            total_casual_non_content_wd_coeditors >= 25
        THEN
            cast(total_casual_non_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_casual_non_content_wd_coeditors,

    CASE
        WHEN
            total_active_wd_coeditors >= 25
        THEN
            cast(total_active_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_active_wd_coeditors,

    CASE
        WHEN
            total_active_content_wd_coeditors >= 25
        THEN
            cast(total_active_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_active_content_wd_coeditors,

    CASE
        WHEN
            total_active_non_content_wd_coeditors >= 25
        THEN
            cast(total_active_non_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_active_non_content_wd_coeditors,

    CASE
        WHEN
            total_very_active_wd_coeditors >= 25
        THEN
            cast(total_very_active_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_very_active_wd_coeditors,

    CASE
        WHEN
            total_very_active_content_wd_coeditors >= 25
        THEN
            cast(total_very_active_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_very_active_content_wd_coeditors,

    CASE
        WHEN
            total_very_active_non_content_wd_coeditors >= 25
        THEN
            cast(total_very_active_non_content_wd_coeditors AS STRING)
        ELSE
            '<25'
    END AS total_very_active_non_content_wd_coeditors

FROM
    ${source_table}

ORDER BY
    month DESC
;
