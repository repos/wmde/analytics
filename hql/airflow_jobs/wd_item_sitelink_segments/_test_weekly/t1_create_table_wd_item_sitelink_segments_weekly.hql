-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t1_create_table_wd_item_sitelink_segments_weekly.hql

-- Note: This script is for testing and should be ran in your local Hive DB schema.

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wd_item_sitelink_segments_weekly`(
        `week`                   date    COMMENT 'The date at the start of the week for which the segments are computed',
        `sitelink_items`         bigint  COMMENT 'Items that have a sitelink to one or more Wikimedia projects',
        `sitelink_item_targets`  bigint  COMMENT 'Items that those in sitelink_items are connected to',
        `all_other_items`        bigint  COMMENT 'Items not included in sitelink_items or sitelink_item_targets'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    'hdfs://analytics-hadoop/tmp/wmde/analytics/test_data/wd_item_sitelink_segments_weekly/'
;
