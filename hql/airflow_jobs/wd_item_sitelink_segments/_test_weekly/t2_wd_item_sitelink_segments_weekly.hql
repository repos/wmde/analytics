-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t2_wd_item_sitelink_segments_weekly.hql

-- Note: Please replace TEST_HIVE_USER_DB, TEST_YEAR, TEST_MONTH and TEST_DAY for testing.

-- MARK: Set Variables

SET hivevar:week_date = cast(
    concat(
        lpad(TEST_YEAR, 4, '0'),
        '-',
        lpad(TEST_MONTH, 2, '0'),
        '-',
        lpad(TEST_DAY, 2, '0')
    ) AS DATE
)
;

SET hivevar:week_snapshot = concat(
    lpad(TEST_YEAR, 4, '0'),
    '-',
    lpad(TEST_MONTH, 2, '0'),
    '-',
    lpad(TEST_DAY, 2, '0')
)
;

-- MARK: Delete Data

-- Delete existing data for the period to prevent duplication of data in case of recomputation.
DELETE FROM
    TEST_HIVE_USER_DB.wd_item_sitelink_segments_weekly

WHERE
    week = ${week_date}
;

-- MARK: Item Links

WITH item_links AS (
    SELECT DISTINCT
        id AS item_id,
        siteLinks AS sitelinks,
        get_json_object(c.exploded_claims['mainSnak'].dataValue.value, '$.id') AS linked_item_id

    FROM
        wmf.wikidata_entity

    LATERAL VIEW
        explode(claims) c AS exploded_claims

    WHERE
        snapshot = ${week_snapshot}
        AND c.exploded_claims['mainSnak'].dataType = 'wikibase-item'
),

-- MARK: Item Sitelinks

items_and_sitelinks AS (
    SELECT DISTINCT
        id AS item_id,
        siteLinks AS sitelinks

    FROM
        wmf.wikidata_entity

    WHERE
        snapshot = ${week_snapshot}
),

-- MARK: Sitelink Items

sitelink_items AS (
    SELECT DISTINCT
        item_id AS item_id

    FROM
        items_and_sitelinks

    WHERE
        sitelinks IS NOT NULL
),

-- MARK: Sitelink Targets

sitelink_item_targets AS (
    SELECT DISTINCT
        l.linked_item_id AS item_id

    FROM
        item_links AS l

    LEFT JOIN
        items_and_sitelinks AS s

    ON
        l.linked_item_id = s.item_id

    WHERE
        l.sitelinks IS NOT NULL
        AND s.sitelinks IS NULL
)

-- MARK: Insert Data

INSERT INTO
    TEST_HIVE_USER_DB.wd_item_sitelink_segments_weekly

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    ${week_date} AS week,
    count(DISTINCT s.item_id) AS sitelink_items,
    count(DISTINCT t.item_id) AS sitelink_item_targets,
    count(DISTINCT e.id) - count(DISTINCT s.item_id) - count(DISTINCT t.item_id) AS all_other_items

FROM
    wmf.wikidata_entity AS e

LEFT JOIN
    sitelink_items AS s

ON
    e.id = s.item_id

LEFT JOIN
    sitelink_item_targets AS t

ON
    e.id = t.item_id

WHERE
    e.snapshot = ${week_snapshot}
;
