-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t1_clear_tables_wiktionary_cognate_daily.hql

-- Note: Please replace TEST_HIVE_USER_DB, TEST_YEAR, TEST_MONTH and TEST_ DAY for testing.
-- Note: We're cleaning data only past one week old to assure that backups are available.

DELETE FROM
    ${TEST_HIVE_USER_DB.wiktionary_cognate_sites_daily}

WHERE
    year = YEAR(DATE_SUB(concat(TEST_YEAR, '-', TEST_MONTH, '-', TEST_DAY), INTERVAL 7 DAY))
    AND month = MONTH(DATE_SUB(concat(TEST_YEAR, '-', TEST_MONTH, '-', TEST_DAY), INTERVAL 7 DAY))
    AND day = DAY(DATE_SUB(concat(TEST_YEAR, '-', TEST_MONTH, '-', TEST_DAY), INTERVAL 7 DAY));
;

DELETE FROM
    ${TEST_HIVE_USER_DB.wiktionary_cognate_pages_daily}

WHERE
    year = YEAR(DATE_SUB(concat(TEST_YEAR, '-', TEST_MONTH, '-', TEST_DAY), INTERVAL 7 DAY))
    AND month = MONTH(DATE_SUB(concat(TEST_YEAR, '-', TEST_MONTH, '-', TEST_DAY), INTERVAL 7 DAY))
    AND day = DAY(DATE_SUB(concat(TEST_YEAR, '-', TEST_MONTH, '-', TEST_DAY), INTERVAL 7 DAY));
;

DELETE FROM
    ${TEST_HIVE_USER_DB.wiktionary_cognate_titles_daily}

WHERE
    year = YEAR(DATE_SUB(concat(TEST_YEAR, '-', TEST_MONTH, '-', TEST_DAY), INTERVAL 7 DAY))
    AND month = MONTH(DATE_SUB(concat(TEST_YEAR, '-', TEST_MONTH, '-', TEST_DAY), INTERVAL 7 DAY))
    AND day = DAY(DATE_SUB(concat(TEST_YEAR, '-', TEST_MONTH, '-', TEST_DAY), INTERVAL 7 DAY));
;
