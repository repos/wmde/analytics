-- Parameters:
--     hive_wiktionary_cognate_sites_daily     -- Fully qualified table remove the project
--                                                entry relationships from.
--     hive_wiktionary_cognate_pages_daily     -- Fully qualified table remove the project
--                                                entry relationships from.
--     hive_wiktionary_cognate_titles_daily    -- Fully qualified table remove the project
--                                                entry relationships from.
--
-- Usage:
--     hive -f clear_tables_wiktionary_cognate_daily.hql                                   \
--         -d hive_wiktionary_cognate_sites_daily=wmde.wiktionary_cognate_sites_daily      \
--         -d hive_wiktionary_cognate_pages_daily=wmde.wiktionary_cognate_pages_daily      \
--         -d hive_wiktionary_cognate_titles_daily=wmde.wiktionary_cognate_titles_daily    \
--         -d year=2024                                                                    \
--         -d month=6                                                                      \
--         -d day=1

-- Note: We're cleaning data only past one week old to assure that backups are available.

DELETE FROM
    ${hive_wiktionary_cognate_sites_daily}

WHERE
    year = YEAR(DATE_SUB(concat(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND month = MONTH(DATE_SUB(concat(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND day = DAY(DATE_SUB(concat(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY));
;

DELETE FROM
    ${hive_wiktionary_cognate_pages_daily}

WHERE
    year = YEAR(DATE_SUB(concat(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND month = MONTH(DATE_SUB(concat(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND day = DAY(DATE_SUB(concat(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY));
;

DELETE FROM
    ${hive_wiktionary_cognate_titles_daily}

WHERE
    year = YEAR(DATE_SUB(concat(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND month = MONTH(DATE_SUB(concat(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND day = DAY(DATE_SUB(concat(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY));
;
