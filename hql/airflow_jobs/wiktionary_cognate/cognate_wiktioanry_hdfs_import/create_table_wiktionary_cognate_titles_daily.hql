-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f create_table_wiktionary_cognate_titles_daily.hql    \
--         --database wmde                                               \
--         -d location=/wmf/data/wmde/wiktionary_cognate_titles_daily

-- See: https://www.mediawiki.org/wiki/Extension:Cognate

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wiktionary_cognate_titles_daily`(
        `cgti_raw`             string  COMMENT 'The raw string representation of the title',
        `cgti_raw_key`         string  COMMENT 'SHA-256 hashed equivalent of the value in cgti_raw (cognate_pages.cgpa_title link)',
        `cgti_normalized_key`  string  COMMENT 'SHA-256 hashed equivalent of the normalized value in cgti_raw (Foo... = Foo… ≠ Foo)',
        `year`                 bigint  COMMENT 'Unpadded year of request (partition field)',
        `month`                bigint  COMMENT 'Unpadded month of request (partition field)',
        `day`                  bigint  COMMENT 'Unpadded day of request (partition field)'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    '${location}'
;
