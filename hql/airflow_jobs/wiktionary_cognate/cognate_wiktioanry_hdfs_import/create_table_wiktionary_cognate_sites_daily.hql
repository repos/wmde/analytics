-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f create_table_wiktionary_cognate_sites_daily.hql    \
--         --database wmde                                              \
--         -d location=/wmf/data/wmde/wiktionary_cognate_sites_daily

-- See: https://www.mediawiki.org/wiki/Extension:Cognate

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wiktionary_cognate_sites_daily`(
        `cgsi_key`        string  COMMENT 'SHA-256 hashed equivalent of the value in cgsi_dbname (cognate_pages.cgpa_site link)',
        `cgsi_dbname`     string  COMMENT 'Wiktionary project identifier for the site (iso2wiktionary)',
        `cgsi_interwiki`  string  COMMENT 'The ISO alpha-2 code for the project in cgsi_dbname',
        `year`            bigint  COMMENT 'Unpadded year of request (partition field)',
        `month`           bigint  COMMENT 'Unpadded month of request (partition field)',
        `day`             bigint  COMMENT 'Unpadded day of request (partition field)'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    '${location}'
;
