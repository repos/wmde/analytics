-- Parameters:
--     source_table             -- Fully qualified table name to compute the
--                                 aggregations for.
--     destination_directory    -- Fully qualified directory name that the file
--                                 should be exported to.
--
-- Usage:
--     spark3-sql -f gen_csv_wiktionary_cognate_missing_entries_daily.hql                                       \
--         -d source_table=wmde.wiktionary_cognate_missing_entries_daily                                        \
--         -d destination_directory=/wmf/tmp/wmde/analytics/airflow/wiktionary_cognate_missing_entries_daily

-- MARK: Insert

INSERT OVERWRITE DIRECTORY
    '${destination_directory}'

USING CSV OPTIONS
    ('sep'=',', 'header'='true', 'compression'='none')

-- MARK: Select

SELECT /*+ COALESCE(1) */
    *

FROM
    ${source_table}
;
