-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t2_wiktionary_cognate_most_popular_entries_daily.hql

-- Note: Please replace TEST_HIVE_USER_DB, TEST_YEAR, TEST_MONTH and TEST_DAY for testing.

-- MARK: Delete Data

DELETE FROM
    TEST_HIVE_USER_DB.wiktionary_cognate_most_popular_entries_daily

WHERE
    1=1
;

-- MARK: Entries

WITH wiktionary_entries AS (
    SELECT DISTINCT
        s.cgsi_dbname AS cgsi_dbname,
        t.cgti_normalized_key AS cgti_normalized_key

    FROM
        wmde.wiktionary_cognate_sites_daily AS s

    JOIN
        wmde.wiktionary_cognate_pages_daily AS p

    ON
        s.cgsi_key = p.cgpa_site
        AND p.year = TEST_YEAR
        AND p.month = TEST_MONTH
        AND p.day = TEST_DAY

    JOIN
        wmde.wiktionary_cognate_title AS t

    ON
        p.cgpa_title = t.cgti_raw_key
        AND t.year = TEST_YEAR
        AND t.month = TEST_MONTH
        AND t.day = TEST_DAY

    WHERE
        s.year = TEST_YEAR
        AND s.month = TEST_MONTH
        AND s.day = TEST_DAY
)

-- MARK: Insert Data

INSERT INTO
    TEST_HIVE_USER_DB.wiktionary_cognate_most_popular_entries_daily

SELECT
    t.cgti_raw AS entry,
    count(e.cgti_normalized_key) AS total_wiktionaries_with_entry

FROM
    wiktionary_entries AS e

LEFT JOIN
    wmde.wiktionary_cognate_title AS t

ON
    e.cgti_normalized_key = t.cgti_normalized_key
    AND t.year = TEST_YEAR
    AND t.month = TEST_MONTH
    AND t.day = TEST_DAY

ORDER BY
    total_wiktionaries_with_entry DESC,
    entry ASC

LIMIT
    10000
;
