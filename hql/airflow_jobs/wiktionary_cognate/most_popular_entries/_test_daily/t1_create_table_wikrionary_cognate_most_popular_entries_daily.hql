-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t1_create_table_wiktionary_cognate_most_popular_entries_daily.hql

-- Note: This script is for testing and should be ran in your local Hive DB schema.

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wiktionary_cognate_most_popular_entries_daily`(
        `entry`                          string  COMMENT 'A word that appears in at least 10 Wiktionaries',
        `total_wiktionaries_with_entry`  bigint  COMMENT 'The sum of all Wiktionaries that contain the word in the entry column',
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    'hdfs://analytics-hadoop/tmp/wmde/analytics/test_data/wiktionary_cognate_most_popular_entries_daily/'
;
