-- Parameters:
--     src_hive_wiktionary_cognate_sites_daily     -- Fully qualified table get the project
--                                                    entry relationships from.
--     src_hive_wiktionary_cognate_pages_daily     -- Fully qualified table get the project
--                                                    entry relationships from.
--     src_hive_wiktionary_cognate_titles_daily    -- Fully qualified table get the project
--                                                    entry relationships from.
--     destination_table                           -- Fully qualified table name to fill in
--                                                    project entry relationships.
--     year                                        -- Year of partition to get the project
--                                                    entry relationships from.
--     month                                       -- Month of partition to get the project
--                                                    entry relationships from.
--     day                                         -- Day of partition to get the project
--                                                    entry relationships from.
--
-- Usage:
--     hive -f wiktionary_cognate_most_popular_entries_daily.hql                                  \
--         -d src_hive_wiktionary_cognate_sites_daily=wmde.wiktionary_cognate_sites_daily         \
--         -d src_hive_wiktionary_cognate_pages_daily=wmde.wiktionary_cognate_pages_daily         \
--         -d src_hive_wiktionary_cognate_titles_daily=wmde.wiktionary_cognate_titles_daily       \
--         -d destination_table=wmde.wiktionary_cognate_most_popular_entries_daily                \
--         -d year=2024                                                                           \
--         -d month=6                                                                             \
--         -d day=1

-- MARK: Delete Data

DELETE FROM
    ${hive_most_popular_entries}

WHERE
    1=1
;

-- MARK: Entrires

WITH wiktionary_entries AS (
    SELECT DISTINCT
        s.cgsi_dbname AS cgsi_dbname,
        t.cgti_normalized_key AS cgti_normalized_key

    FROM
        ${src_hive_wiktionary_cognate_sites_daily} AS s

    JOIN
        ${src_hive_wiktionary_cognate_pages_daily} AS p

    ON
        s.cgsi_key = p.cgpa_site
        AND p.year = ${year}
        AND p.month = ${month}
        AND p.day = ${day}

    JOIN
        ${src_hive_wiktionary_cognate_titles_daily} AS t

    ON
        p.cgpa_title = t.cgti_raw_key
        AND t.year = ${year}
        AND t.month = ${month}
        AND t.day = ${day}

    WHERE
        s.year = ${year}
        AND s.month = ${month}
        AND s.day = ${day}
)

-- MARK: Insert Data

INSERT INTO
    ${hive_most_popular_entries}

SELECT
    t.cgti_raw AS entry,
    count(e.cgti_normalized_key) AS total_wiktionaries_with_entry

FROM
    wiktionary_entries AS e

LEFT JOIN
    ${src_hive_wiktionary_cognate_titles_daily} AS t

ON
    e.cgti_normalized_key = t.cgti_normalized_key
    AND t.year = ${year}
    AND t.month = ${month}
    AND t.day = ${day}

ORDER BY
    total_wiktionaries_with_entry DESC,
    entry ASC

LIMIT
    10000
;
