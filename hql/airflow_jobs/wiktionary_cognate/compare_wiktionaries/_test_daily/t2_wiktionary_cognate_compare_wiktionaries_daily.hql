-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f t2_wiktionary_cognate_compare_wiktionaries_daily.hql

-- Note: Please replace TEST_HIVE_USER_DB, TEST_YEAR, TEST_MONTH and TEST_DAY for testing.

-- MARK: Delete Data

DELETE FROM
    TEST_HIVE_USER_DB.wiktionary_cognate_compare_wiktionaries_daily

WHERE
    1=1
;

-- MARK: Entries

WITH wiktionary_entries AS (
    SELECT DISTINCT
        s.cgsi_dbname AS wiktionary,
        t.cgti_normalized_key AS cgti_normalized_key

    FROM
        wmde.wiktionary_cognate_sites_daily AS s

    JOIN
        wmde.wiktionary_cognate_pages_daily AS p

    ON
        s.cgsi_key = p.cgpa_site
        AND p.year = TEST_YEAR
        AND p.month = TEST_MONTH
        AND p.day = TEST_DAY

    JOIN
        wmde.wiktionary_cognate_titles_daily AS t

    ON
        p.cgpa_title = t.cgti_raw_key
        AND t.year = TEST_YEAR
        AND t.month = TEST_MONTH
        AND t.day = TEST_DAY

    WHERE
        s.year = TEST_YEAR
        AND s.month = TEST_MONTH
        AND s.day = TEST_DAY
)

-- MARK: Insert Data

INSERT INTO
    TEST_HIVE_USER_DB.wiktionary_cognate_compare_wiktionaries_daily

SELECT
    e2.wiktionary AS source_wiktionary,
    e1.wiktionary AS target_wiktionary,
    t.cgti_raw AS missing_source_entry

FROM
    wiktionary_entries AS e1

LEFT JOIN
    wiktionary_entries AS e2

ON
    e1.cgti_normalized_key = e2.cgti_normalized_key
    AND e1.wiktionary != e2.wiktionary

LEFT JOIN
    wmde.wiktionary_cognate_titles_daily AS t

ON
    e1.cgti_normalized_key = t.cgti_normalized_key
    AND t.year = TEST_YEAR
    AND t.month = TEST_MONTH
    AND t.day = TEST_DAY

WHERE
    e2.cgti_normalized_key IS NULL

ORDER BY
    source_wiktionary ASC,
    target_wiktionary ASC,
    missing_source_entry ASC
;
