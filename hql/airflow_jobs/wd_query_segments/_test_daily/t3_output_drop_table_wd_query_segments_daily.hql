-- Parameters:
--     None
--
-- Usage:
--     hive -f t3_output_drop_table_wd_query_segments_daily.hql

-- Note: Please replace TEST_HIVE_USER_DB for testing.

-- MARK: Select

SELECT
    day AS day,

    CASE
        WHEN
            total_wdqs_queries >= 25
        THEN
            cast(total_wdqs_queries AS STRING)
        ELSE
            '<25'
    END AS total_wdqs_queries,

    CASE
        WHEN
            total_single_entity >= 25
        THEN
            cast(total_single_entity AS STRING)
        ELSE
            '<25'
    END AS total_single_entity,

    CASE
        WHEN
            total_all_entity_statements >= 25
        THEN
            cast(total_all_entity_statements AS STRING)
        ELSE
            '<25'
    END AS total_all_entity_statements,

    CASE
        WHEN
            total_single_term_statement >= 25
        THEN
            cast(total_single_term_statement AS STRING)
        ELSE
            '<25'
    END AS total_single_term_statement,

    CASE
        WHEN
            total_single_inverse_statement >= 25
        THEN
            cast(total_single_inverse_statement AS STRING)
        ELSE
            '<25'
    END AS total_single_inverse_statement,

    CASE
        WHEN
            total_single_instance_or_subclass_statement >= 25
        THEN
            cast(total_single_instance_or_subclass_statement AS STRING)
        ELSE
            '<25'
    END AS total_single_instance_or_subclass_statement,

    CASE
        WHEN
            total_single_known_relation_statement >= 25
        THEN
            cast(total_single_known_relation_statement AS STRING)
        ELSE
            '<25'
    END AS total_single_known_relation_statement,

    CASE
        WHEN
            total_single_unknown_relation_statement >= 25
        THEN
            cast(total_single_unknown_relation_statement AS STRING)
        ELSE
            '<25'
    END AS total_single_unknown_relation_statement,

    CASE
        WHEN
            total_complex_queries >= 25
        THEN
            cast(total_complex_queries AS STRING)
        ELSE
            '<25'
    END AS total_complex_queries

FROM
    TEST_HIVE_USER_DB.wd_query_segments_daily

ORDER BY
    day DESC

LIMIT
    5
;

-- MARK: Drop Table

-- Note: Save the output for your MR!
-- Note: If possible report the output in the Phabricator task (values <25 as '<25').
-- Note: Check HDFS to make sure that test data and metadata have been deleted.

DROP TABLE
    TEST_HIVE_USER_DB.wd_query_segments_daily
;
