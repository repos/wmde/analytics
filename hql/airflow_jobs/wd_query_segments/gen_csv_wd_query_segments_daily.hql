-- Parameters:
--     source_table             -- Fully qualified table name to compute the
--                                 aggregations for.
--     destination_directory    -- Fully qualified directory name that the file
--                                 should be exported to.
--
-- Usage:
--     spark3-sql -f gen_csv_wd_query_segments_daily.hql                                       \
--         -d source_table=wmde.wd_query_segments_daily                                        \
--         -d destination_directory=/wmf/tmp/wmde/analytics/airflow/wd_query_segments_daily

-- MARK: Insert

INSERT OVERWRITE DIRECTORY
    '${destination_directory}'

USING CSV OPTIONS
    ('sep'=',', 'header'='true', 'compression'='none')

-- MARK: Select

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    day AS day,

    CASE
        WHEN
            total_wdqs_queries >= 25
        THEN
            cast(total_wdqs_queries AS STRING)
        ELSE
            '<25'
    END AS total_wdqs_queries,

    CASE
        WHEN
            total_single_entity >= 25
        THEN
            cast(total_single_entity AS STRING)
        ELSE
            '<25'
    END AS total_single_entity,

    CASE
        WHEN
            total_all_entity_statements >= 25
        THEN
            cast(total_all_entity_statements AS STRING)
        ELSE
            '<25'
    END AS total_all_entity_statements,

    CASE
        WHEN
            total_single_term_statement >= 25
        THEN
            cast(total_single_term_statement AS STRING)
        ELSE
            '<25'
    END AS total_single_term_statement,

    CASE
        WHEN
            total_single_inverse_statement >= 25
        THEN
            cast(total_single_inverse_statement AS STRING)
        ELSE
            '<25'
    END AS total_single_inverse_statement,

    CASE
        WHEN
            total_single_instance_or_subclass_statement >= 25
        THEN
            cast(total_single_instance_or_subclass_statement AS STRING)
        ELSE
            '<25'
    END AS total_single_instance_or_subclass_statement,

    CASE
        WHEN
            total_single_known_relation_statement >= 25
        THEN
            cast(total_single_known_relation_statement AS STRING)
        ELSE
            '<25'
    END AS total_single_known_relation_statement,

    CASE
        WHEN
            total_single_unknown_relation_statement >= 25
        THEN
            cast(total_single_unknown_relation_statement AS STRING)
        ELSE
            '<25'
    END AS total_single_unknown_relation_statement,

    CASE
        WHEN
            total_complex_queries >= 25
        THEN
            cast(total_complex_queries AS STRING)
        ELSE
            '<25'
    END AS total_complex_queries

FROM
    ${source_table}

ORDER BY
    day DESC
;
