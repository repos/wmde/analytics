-- Parameters:
--     source_table         -- Fully qualified table name to compute the
--                             aggregations for.
--     destination_table    -- Fully qualified table name to fill in
--                             aggregated values.
--     year                 -- Year of partition to compute aggregations
--                             for.
--     month                -- Month of partition to compute aggregations
--                             for.
--     day                  -- Day of partition to compute aggregations
--                             for.
--
-- Usage:
--     spark3-sql -f wd_query_segments_daily.hql                        \
--         -d source_table=discovery.processed_external_sparql_query    \
--         -d destination_table=wmde.wd_query_segments_daily            \
--         -d year=2024                                                 \
--         -d month=7                                                   \
--         -d day=1

-- Attn: discovery.processed_external_sparql_query doesn't parse all queries (<10% query loss).

-- MARK: Set Variables

SET hivevar:day_date = cast(
    concat(
        lpad(${year}, 4, '0'),
        '-',
        lpad(${month}, 2, '0'),
        '-',
        lpad(${day}, 2, '0')
    ) AS DATE
)
;

-- MARK: Delete Data

-- Delete existing data for the period to prevent duplication of data in case of recomputation.
DELETE FROM
    ${destination_table}

WHERE
    day = ${day_date}
;

-- MARK: Query Triples

WITH queries_with_triple_components AS (
    SELECT
        id AS id,
        query AS query,
        transform(q_info.triples, x -> x.subjectNode.nodeValue) AS triple_subjs,
        transform(q_info.triples, x -> x.predicateNode.nodeValue) AS triple_preds,
        transform(q_info.triples, x -> x.objectNode.nodeValue) AS triple_objs,
        q_info.nodes AS nodes

    FROM
        ${source_table}

    WHERE
        year = ${year}
        AND month = ${month}
        AND day = ${day}
        AND wiki = 'wikidata'
),

-- MARK: Query Flags

queries_with_triple_flags AS (
    SELECT
        id AS id,

        size(triple_subjs) = 1 AS is_single_statement,

        size(triple_subjs) = 2 AS is_two_statements,

        size(array_distinct(triple_subjs)) = 1 AS has_unique_subj,

        exists(
            triple_subjs,
            x ->
                x RLIKE '^wd:[QPL][0-9]+$'
                OR x RLIKE '^<http:\/\/www.wikidata.org\/entity\/[QPL][0-9]+>$'
                -- Lexeme senses and forms.
                OR x RLIKE '^wd:L[0-9]+-[FS][0-9]+$'
                OR x RLIKE '^<http:\/\/www.wikidata.org\/entity\/L[0-9]+-[FS][0-9]+>$'
        ) AS subjs_contain_wd_ents,

        exists(
            triple_subjs,
            x ->
                x = 'hint:Prior'
        ) AS subjs_contain_prior_hint,

        (
            -- A single entity could be set as a subject via 'values' or 'bind'.
            size(triple_subjs) = 1
            AND nodes[concat('NODE_VAR[', triple_subjs[0], ']')] = 2
            AND size(nodes) = 4
            AND (
                query LIKE '%values%'
                OR query LIKE '%VALUES%'
                OR query LIKE '%bind%'
                OR query LIKE '%BIND%'
            )
        ) AS subj_maybe_single_statement_values_bind_ent,

        -- Note: Subjects and objects are 'exists' while predicates are mostly 'forall'.
        forall(
            triple_preds,
            x ->
                x RLIKE '^wdt:P[0-9]+$'
                OR x RLIKE '^<http:\/\/www.wikidata.org\/prop\/direct\/P[0-9]+>$'
                OR x RLIKE '^p:P[0-9]+$'
        ) AS preds_are_wd_props,

        forall(
            triple_preds,
            x ->
                x RLIKE 'wdt:P[0-9]+'
                OR x RLIKE '<http:\/\/www.wikidata.org\/prop\/direct\/P[0-9]+>'
                OR x RLIKE 'p:P[0-9]+'
        ) AS preds_are_wd_props_or_wildcards,

        forall(
            triple_preds,
            x ->
                -- Instance of and subclass of, with wildcards being allowed.
                -- Note: Wildcards are outside the URI surrounded with parentheses.
                x RLIKE 'wdt:P31'
                OR x RLIKE 'p:P31'
                OR x RLIKE '<http:\/\/www.wikidata.org\/prop\/direct\/P31>'
                OR x RLIKE 'wdt:P279'
                OR x RLIKE 'p:P279'
                OR x RLIKE '<http:\/\/www.wikidata.org\/prop\/direct\/P279>'
        ) AS is_for_instance_or_subclass,

        exists(
            triple_preds,
            x ->
                -- Labels, aliases, descriptions and lemmas.
                -- Note: URIs are converted to their statement prefixes in parsing.
                x = 'rdfs:label'
                OR x = 'wikibase:label'
                OR x = 'skos:altLabel'
                OR x = 'schema:description'
                OR x = 'wikibase:lemma'
        ) AS is_for_terms,

        exists(
            triple_preds,
            x ->
                x = 'hint:gearing'
        ) AS preds_contain_gearing_hint,

        exists(
            triple_objs,
            x ->
                x RLIKE '^wd:[QPL][0-9]+$'
                OR x RLIKE '^<http:\/\/www.wikidata.org\/entity\/[QPL][0-9]+>$'
                -- Lexeme senses and forms.
                OR x RLIKE '^wd:L[0-9]+-[FS][0-9]+$'
                OR x RLIKE '^<http:\/\/www.wikidata.org\/entity\/L[0-9]+-[FS][0-9]+>$'
        ) AS objs_contain_wd_ents,

        exists(
            triple_objs,
            x ->
                x RLIKE '^wd:[QPL][0-9]+$'
                -- Lexeme senses and forms.
                OR x RLIKE '^wd:L[0-9]+-[FS][0-9]+$'
                OR x RLIKE '^<http:\/\/www.wikidata.org\/entity\/[QPL][0-9]+>$'
                OR x = 'wd:Qnull'
        ) AS objs_contain_wd_ents_or_null,

        exists(
            triple_objs,
            x ->
                x = 'forwards'
        ) AS objs_contain_forwards

    FROM
        queries_with_triple_components
),

-- MARK: Total Queries

total_queries_in_period AS (
    SELECT
        ${day_date} AS day,
        count(id) AS total_wdqs_queries

    FROM
        queries_with_triple_components
),

-- MARK: Single Entities

single_entity_query_ids AS (
    SELECT
        id

    FROM
        queries_with_triple_flags

    WHERE
        has_unique_subj
        AND subjs_contain_wd_ents
        AND preds_are_wd_props
        AND NOT is_for_instance_or_subclass
        AND NOT objs_contain_wd_ents
),

-- MARK: All Statements

all_entity_statements_query_ids AS (
    SELECT
        id

    FROM
        queries_with_triple_flags

    WHERE
        NOT subjs_contain_wd_ents
        AND subj_maybe_single_statement_values_bind_ent
        AND NOT preds_are_wd_props_or_wildcards
        AND NOT objs_contain_wd_ents
),

-- MARK: Single Terms

single_term_statement_query_ids AS (
    SELECT
        id

    FROM
        queries_with_triple_flags

    WHERE
        is_single_statement
        AND subjs_contain_wd_ents
        AND is_for_terms
),

-- MARK: Single Inverse

single_inverse_statement_query_ids AS (
    SELECT
        id

    FROM
        queries_with_triple_flags

    WHERE
        is_single_statement
        AND NOT subjs_contain_wd_ents
        AND preds_are_wd_props
),

-- MARK: Single Instance Subclass

single_instance_or_subclass_statement_query_ids AS (
    SELECT
        id

    FROM
        queries_with_triple_flags

    WHERE
        is_single_statement
        AND subjs_contain_wd_ents
        AND is_for_instance_or_subclass
        AND NOT objs_contain_wd_ents
),

-- MARK: Single Known

single_known_relation_statement_query_ids AS (
    SELECT
        id

    FROM
        queries_with_triple_flags

    WHERE
        (
            (
                is_single_statement
                AND preds_are_wd_props_or_wildcards
            ) OR (
                -- Note: Queries include 'hint:Prior hint:gearing "forward"' to speed them up.
                is_two_statements
                AND subjs_contain_prior_hint
                AND preds_contain_gearing_hint
                AND objs_contain_forwards
            )
        )
        AND subjs_contain_wd_ents
        AND objs_contain_wd_ents_or_null
),

-- MARK: Single Unknown

single_unknown_relation_statement_query_ids AS (
    SELECT
        id

    FROM
        queries_with_triple_flags

    WHERE
        is_single_statement
        AND subjs_contain_wd_ents
        AND NOT preds_are_wd_props_or_wildcards
        AND objs_contain_wd_ents
),

-- MARK: Period Totals

total_segmented_queries_in_period AS (
    SELECT
        ${day_date} AS day,

        count(e.id) AS total_single_entity,

        count(a.id) AS total_all_entity_statements,

        count(t.id) AS total_single_term_statement,

        count(i.id) AS total_single_inverse_statement,

        count(c.id) AS total_single_instance_or_subclass_statement,

        count(k.id) AS total_single_known_relation_statement,

        count(u.id) AS total_single_unknown_relation_statement,

        count(
            CASE
                WHEN
                    e.id IS NULL
                    AND a.id IS NULL
                    AND t.id IS NULL
                    AND i.id IS NULL
                    AND c.id IS NULL
                    AND k.id IS NULL
                    AND u.id IS NULL
                THEN
                    q.id
            END
        ) AS total_complex_queries

    FROM
        queries_with_triple_flags AS q

    LEFT JOIN
        single_entity_query_ids AS e

    ON
        q.id = e.id

    LEFT JOIN
        all_entity_statements_query_ids AS a

    ON
        q.id = a.id

    LEFT JOIN
        single_term_statement_query_ids AS t

    ON
        q.id = t.id

    LEFT JOIN
        single_inverse_statement_query_ids AS i

    ON
        q.id = i.id

    LEFT JOIN
        single_instance_or_subclass_statement_query_ids AS c

    ON
        q.id = c.id

    LEFT JOIN
        single_known_relation_statement_query_ids AS k

    ON
        q.id = k.id

    LEFT JOIN
        single_unknown_relation_statement_query_ids AS u

    ON
        q.id = u.id
)

-- MARK: Insert Data

INSERT INTO
    ${destination_table}

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    q.day AS day,
    q.total_wdqs_queries AS total_wdqs_queries,
    s.total_single_entity AS total_single_entity,
    s.total_all_entity_statements AS total_all_entity_statements,
    s.total_single_term_statement AS total_single_term_statement,
    s.total_single_inverse_statement AS total_single_inverse_statement,
    s.total_single_instance_or_subclass_statement AS total_single_instance_or_subclass_statement,
    s.total_single_known_relation_statement AS total_single_known_relation_statement,
    s.total_single_unknown_relation_statement AS total_single_unknown_relation_statement,
    s.total_complex_queries AS total_complex_queries

FROM
    total_queries_in_period AS q

JOIN
    total_segmented_queries_in_period AS s

ON
    q.day = s.day
;
