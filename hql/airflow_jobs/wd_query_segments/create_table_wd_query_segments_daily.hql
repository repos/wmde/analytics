-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f create_table_wd_query_segments_daily.hql    \
--         --database wmde                                       \
--         -d location=/wmf/data/wmde/wd_query_segments_daily

-- Attn: discovery.processed_external_sparql_query doesn't parse all queries (<10% query loss).

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wd_query_segments_daily`(
        `day`                                            date    COMMENT 'The day for which the metrics are computed over',
        `total_wdqs_queries`                             bigint  COMMENT 'Total Wikidata Query Service queries during the period',
        `total_single_entity`                            bigint  COMMENT 'Total queries that access only one entity',
        `total_all_entity_statements`                    bigint  COMMENT 'Total queries that access all statements of one entity',
        `total_single_term_statement`                    bigint  COMMENT 'Total single statement queries accessing labels, aliases or descriptions',
        `total_single_inverse_statement`                 bigint  COMMENT 'Total single statement queries that derive an entity from a predicate and an object',
        `total_single_instance_or_subclass_statement`    bigint  COMMENT 'Total single statement queries that query instances or subclasses of an entity subject',
        `total_single_known_relation_statement`          bigint  COMMENT 'Total single statement queries that check for an known relationship between a subject and an object',
        `total_single_unknown_relation_statement`        bigint  COMMENT 'Total single statement queries that check for an unknown relationship between a subject and an object',
        `total_complex_queries`                          bigint  COMMENT 'Total queries not included in any custom aggregation column'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    '${location}'
;
