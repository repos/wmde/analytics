-- Parameters:
--     None
--
-- Usage:
--     spark3-sql -f create_table_wd_rest_api_user_agents_monthly.hql    \
--         --database wmde                                               \
--         -d location=/wmf/data/wmde/wd_rest_api_user_agents_monthly

CREATE EXTERNAL TABLE IF NOT EXISTS
    `wd_rest_api_user_agents_monthly`(
        `month`           date    COMMENT 'The month for which the user agents are derived',
        `user_agent`      string  COMMENT 'A user agents that accessed the Wikidata REST API in the given month',
        `total_requests`  bigint  COMMENT 'The total Wikidata REST API requests made by the given user agent'
    )

USING
    ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION
    '${location}'
;
