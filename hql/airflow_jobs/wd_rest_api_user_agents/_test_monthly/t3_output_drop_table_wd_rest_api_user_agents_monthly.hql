-- Parameters:
--     None
--
-- Usage:
--     hive -f t3_output_drop_table_wd_rest_api_user_agents_monthly.hql

-- Note: Please replace TEST_HIVE_USER_DB for testing.

-- MARK: Select

SELECT
    *

FROM
    TEST_HIVE_USER_DB.wd_rest_api_user_agents_monthly

LIMIT
    5
;

-- MARK: Drop Table

-- Note: Save the output for your MR!
-- Note: If possible report the output in the Phabricator task (values <25 as '<25').
-- Note: Check HDFS to make sure that test data and metadata have been deleted.

DROP TABLE
    TEST_HIVE_USER_DB.wd_rest_api_user_agents_monthly
;
