-- Parameters:
--     source_table         -- Fully qualified table name to compute the
--                             aggregations for.
--     destination_table    -- Fully qualified table name to fill in
--                             aggregated values.
--     year                 -- Year of partition to compute aggregations
--                             for.
--     month                -- Month of partition to compute aggregations
--                             for.
--
-- Usage:
--     spark3-sql -f wd_rest_api_user_agents_monthly.hql                \
--         -d source_table=wmf.webrequest                               \
--         -d destination_table=wmde.wd_rest_api_user_agents_monthly    \
--         -d year=2024                                                 \
--         -d month=9

-- MARK: Set Variables

SET hivevar:month_date = cast(
    concat(
        lpad(${year}, 4, '0'),
        '-',
        lpad(${month}, 2, '0'),
        '-',
        '01'
    ) AS DATE
)
;

-- MARK: Delete Data

-- Delete existing data for the period to prevent duplication of data in case of recomputation.
DELETE FROM
    ${destination_table}

WHERE
    1=1
;

-- MARK: Insert Data

INSERT INTO
    ${destination_table}

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    ${month_date} AS month,
    user_agent AS user_agent,
    count(user_agent) AS total_requests

FROM
    ${source_table}

WHERE
    year = ${year}
    AND month = ${month}
    AND webrequest_source = 'text'
    AND normalized_host.project_family = 'wikidata'
    AND uri_path LIKE '/w/rest.php/wikibase/%'

GROUP BY
    month,
    user_agent

ORDER BY
    total_requests DESC
;
