# [Technical Wishes](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/technical_wishes)

Analytics tasks for the Technical Wishes team organized by the year the task was finished.
