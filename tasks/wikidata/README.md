# [Wikidata](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/wikidata)

Analytics tasks for the Wikidata team organized by the year the task was finished.
