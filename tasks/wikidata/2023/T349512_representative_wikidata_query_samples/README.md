# Representative Wikidata Query Samples

**Task**: [T349512](https://phabricator.wikimedia.org/T349512)

Jupyter notebooks and HTML versions with outputs for deriving and analyzing samples of WDQS queries in relation to the Blazegraph split.

## Contents

- [notebooks](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/wikidata/2023/T349512_representative_wikidata_query_samples/notebooks)
  - Notebooks without outputs for ease of replication
- [notebooks_html](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/wikidata/2023/T349512_representative_wikidata_query_samples/notebooks_html)
  - HTML versions of the notebooks that include outputs
  - PySpark warnings have been manually removed for readability
- blazegraph_sample_utils.py
  - Functions used to derive and test samples of WDQS queries
