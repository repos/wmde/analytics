# [Product Platform](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/product_platform)

Analytics tasks for the Product Platform team organized by the year the task was finished.
