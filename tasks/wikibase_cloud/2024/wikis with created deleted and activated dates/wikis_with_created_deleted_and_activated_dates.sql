set @query = (
	SELECT
		GROUP_CONCAT(
			CONCAT_WS("", "
				SELECT
					wiki,
					domain,
					created_at,
					deleted_at,
					activated_at,
					last_active_at,
					changes_count

				FROM
					(SELECT
						'",wiki_dbs.wiki_id,"' AS wiki,
						'",wikis.domain,"' AS domain,
						'",wikis.created_at,"' AS created_at,
						'",wikis.deleted_at,"' AS deleted_at
					) AS wiki_dbs,

					(SELECT
						MIN(rc_timestamp) AS activated_at,
						MAX(rc_timestamp) AS last_active_at,
						COUNT(*) AS changes_count
					FROM
						",wiki_dbs.name,".",wiki_dbs.prefix,"_recentchanges

					WHERE
						NOT (rc_timestamp < "DATE_FORMAT(wikis.created_at, '%Y%m%d%H%i%s')+20" AND rc_title='Main_Page')
					) AS c
")
SEPARATOR ' UNION ALL '
) AS query

    FROM
        apidb.wikis

    LEFT JOIN
        apidb.wiki_dbs

    ON
        wikis.id = wiki_dbs.wiki_id
);

prepare stmt from @query;
execute stmt;
deallocate prepare stmt;
