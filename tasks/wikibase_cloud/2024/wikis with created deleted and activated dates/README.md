This query returns all wikis with the dates of creation, deletion, activation, last activity and total number og changes
For activation, it looks at recent changes and ignores records with title Main_Page created earlier
than 20 seconds after creation of the wiki (workaround).
Unfortunately, I don't know how to convert binary timestamp into a proper one in the same query.
