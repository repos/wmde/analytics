set @query = (
	SELECT
		GROUP_CONCAT(
			CONCAT_WS("", "
				SELECT
					wiki,
					domain,
					created_at,
					deleted_at,
					pages,
					rc_ip,
					count

				FROM
					(SELECT
						'",wiki_dbs.wiki_id,"' AS wiki,
						'",wikis.domain,"' AS domain,
						'",wikis.created_at,"' AS created_at,
						'",wikis.deleted_at,"' AS deleted_at,
						'",wiki_site_stats.pages,"' AS pages
                    			) AS wiki_dbs,

					(SELECT
						rc_ip,
						COUNT(*) as count

					FROM
						",wiki_dbs.name,".",wiki_dbs.prefix,"_recentchanges

					GROUP BY
						rc_ip

					) AS ips
			")

	SEPARATOR ' UNION ALL '

) AS query

FROM
	apidb.wikis

LEFT JOIN
	apidb.wiki_dbs ON wikis.id = wiki_dbs.wiki_id

LEFT JOIN
	apidb.wiki_site_stats ON wikis.id=wiki_site_stats.wiki_id

);

prepare stmt from @query;
execute stmt;
deallocate prepare stmt;
