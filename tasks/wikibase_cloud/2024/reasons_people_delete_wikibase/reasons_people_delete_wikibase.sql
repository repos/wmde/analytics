set @query = (
    SELECT
        GROUP_CONCAT(
            CONCAT("
                SELECT
                    wiki,
                    domain,
                    created_at,
                    deleted_at,
                    users,
                    total_edits,
                    total_pages,
                    total_items,
                    total_properties,
                    prior_wiki_count,
                    wiki_deletion_reason

                FROM
                    (
                        SELECT
                            '",wiki_dbs.wiki_id,"' AS wiki,
                            '",wikis.domain,"' AS domain,
                            '",wikis.created_at,"' AS created_at,
                            '",wikis.deleted_at,"' AS deleted_at,
                            '",wikis.wiki_deletion_reason,"' AS wiki_deletion_reason
                    ) AS wiki_dbs,

                    (
                        SELECT
                            max(ss_total_edits) AS total_edits,
                            max(ss_total_pages) AS total_pages,
                            max(ss_users) AS users

                        FROM
                            ",wiki_dbs.name,".",wiki_dbs.prefix,"_site_stats
                    ) AS site_stats,

                    (
                        SELECT
                            sum(if(page_content_model = 'wikibase-item', 1, 0)) AS total_items,
                            sum(if(page_content_model = 'wikibase-property', 1, 0)) AS total_properties

                        FROM
                            ",wiki_dbs.name,".",wiki_dbs.prefix,"_page
                    ) AS entities,

                    (
                        SELECT
                            COUNT(*) AS prior_wiki_count

                        FROM
                            apidb.wiki_managers

                        WHERE
                            user_id = ",wiki_managers.user_id,"
                            AND created_at < '",wikis.deleted_at,"'
                    ) AS managers
            ")

        SEPARATOR ' UNION ALL '

        ) AS query

    FROM
        apidb.wiki_dbs

    LEFT JOIN
        apidb.wikis

    ON
        wiki_dbs.wiki_id = wikis.id

    LEFT JOIN
        apidb.wiki_managers

    ON
        wikis.id = wiki_managers.wiki_id

    WHERE
        wikis.wiki_deletion_reason IS NOT NULL
);

prepare stmt from @query;
execute stmt;
deallocate prepare stmt;
