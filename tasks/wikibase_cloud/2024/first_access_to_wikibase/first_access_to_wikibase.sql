set @query = (
    SELECT
        GROUP_CONCAT(
            CONCAT_WS("", "
                SELECT
                    wiki,
                    domain,
                    created_at,
                    deleted_at,
                    user_password

                FROM
                    (
                        SELECT
                            '",wiki_dbs.wiki_id,"' AS wiki,
                            '",wiki_domains.domain,"' AS domain,
                            '",wiki_dbs.created_at,"' AS created_at,
                            '",wikis.deleted_at,"' AS deleted_at
                    ) AS wiki_dbs,

                    (
                        SELECT
                            user_password

                        FROM
                            ",wiki_dbs.name,".",wiki_dbs.prefix,"_user

                        WHERE
                            user_password IS NOT NULL

                        LIMIT
                            1
                    ) AS user
            ")

        SEPARATOR ' UNION ALL '

        ) AS query

    FROM
        apidb.wikis

    LEFT JOIN
        apidb.wiki_dbs

    ON
        wikis.id = wiki_dbs.wiki_id

    LEFT JOIN
        apidb.wiki_domains

    ON
        wiki_dbs.wiki_id = wiki_domains.wiki_id
);

prepare stmt from @query;
execute stmt;
deallocate prepare stmt;
