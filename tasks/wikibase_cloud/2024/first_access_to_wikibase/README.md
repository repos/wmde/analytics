We can find out whether a Wikibase instance was at least once accessed by a logged in user by looking at the user_password.

The admin user created with the instance needs to setup the password before joining for the first time.
Users registered via Wiki itself create the password when they create their account.

The instances in the query below that don’t have user_password are those that have never been accessed by logged in users.
