# [SWE](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/swe)

Analytics tasks for the SWE department organized by the year the task was finished.
