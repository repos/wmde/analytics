# Instructions

- Copy `config_example.yaml` to `config.yaml`
- Adjust config for Hive database schema, data and output directories
- Download required files as mentioned in comments in `config.yaml` to the data directory set in config
- Run notebooks as follows:

  0. Set a value for `wd_snapshot` in `config.yaml`
  1. Set a value for `snapshot` in `config.yaml`
  2. Run `1_generate_shared_data.ipynb`
  3. In any order, run the notebooks to generate metrics tables:
     - `2_sitelink_metrics.ipynb`
     - `3_symbols_in_page_source_metrics.ipynb`
     - `4_wbc_entity_usage_metrics.ipynb`
  4. Set a new value for `snapshot` in `config.yaml`
  5. Rerun all notebooks as done in Steps 2 and 3
  6. (Optional) Redo steps 4 and 5 as needed
  7. Set the value of `snapshots_for_trends` to those snapshots that you've set in Steps 1, 4, etc.
  8. Run `5_output_base_visualizations_and_tables.ipynb`

  # Data privacy

  The metrics tables output by these notebooks should be considered a low-risk "Collations and combinations of already-public data that it may be inconvenient/difficult for external parties to access", according to [these data publication guidelines](https://foundation.wikimedia.org/wiki/Legal:Data_publication_guidelines).

  This is the case despite the use of data from `wmf_raw.mediawiki_private_linktarget`, because potentially private data from that table is filtered via a join with `wmf_raw.mediawiki_page`. See inline comments in `notebooks/3_symbols_in_page_source_metrics.ipynb` for details.
