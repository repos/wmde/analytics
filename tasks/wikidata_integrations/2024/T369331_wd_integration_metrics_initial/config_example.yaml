# Snapshots to use for tables in wmf_raw, wmf and wmf.wikidata_entity.
# Note: We use quotes to prevent Python from interpreting as datetime.
snapshot: "2024-08" # 2024-07
# Note: Check available snapshots with SELECT DISTINCT snapshot FROM wmf.wikidata_entity;.
wd_snapshot: "2024-08-05"

# Note: Only include stapshots here that the full process has been ran for.
# Note: Change `snapshot` above and rerun the notebooks 1-4. Then notebook 5 can be ran.
snapshots_for_trends:
  # - "2024-04"
  # - "2024-05"
  # - "2024-06"
  - "2024-07"
  - "2024-08"

table_base_names_for_trends:
  - types_of_symbol_by_cp_fsc
  - wbc_usages_agg

# Maximum number of rows for Pandas DataFrame for distribution plots.
dist_max_rows: 1000000

# Sampling random seed for repeatability. Set to null for no seed.
sampling_random_seed: null

# Where to find downloaded files with external data (filenames specified below).
# Note: Needs to be the full path.
data_dir: ~/data/

# Basic information and general metrics for wikis.
# See: https://meta.wikimedia.org/wiki/Movement_Insights/Wiki_comparison
wiki_comparison_filename: wiki-comparison-2024.tsv

# Database codes of wikis that can integrate with Wikidata.
# See: https://gerrit.wikimedia.org/r/plugins/gitiles/operations/mediawiki-config/+/refs/heads/master/dblists/wikidataclient.dblist
wikibase_clients_filename: wikidataclient.dblist

# Data to save csv files and charts.
# Note: Needs to be the full path.
output_dir: ~/output/

# Resolution to use when saving charts.
output_dpi: 150

# Hive schema (database) where we'll store the tables.
schema: andyrussg

# MARK: viz_sample_wiki_db_codes

# Database codes of wikis to highlight below histograms and for other visualizations.
# (This has no impact on actual metric calculations.)
viz_sample_wiki_db_codes:
  - enwiki
  - dewiki
  - cawiki
  - eowiki
  - arwiki
  - hewiki
  - urwiki
  - jawiki
  - trwiki
  - kawiki
  - enwiktionary
  - ruwiktionary
  - kuwiktionary
  - enwikivoyage
  - ruwikivoyage
  - fawikivoyage
  - enwikisource
  - zhwikisource
  - hewikisource
  - enwikiquote
  - itwikiquote
  - zhwikiquote
  - enwikibooks
  - enwikiversity
  - commonswiki

# MARK: viz_exclude_project_types

# Project types to exclude from some visualizations (no impact on actual metric calculations).
viz_exclude_project_types:
  - foundation
  - incubator
  - mediawiki
  - meta
  - sources
  - wiktionary
  - commons

# MARK: Temporary Tables

# Table and column names must coordinate with the code in the notebooks.

temporary_tables:
  # Shared data.
  - wd_usage_metrics_base

  # wbc_entity_usage metrics.
  - wbc_usage_counts
  - distinct_props_wbc

  # Symbols in page source metrics.
  - transcl_full
  - transcl
  - transcl_counts
  - wb_in_templates
  - wb_in_templates_norm
  - wb_in_modules
  - wb_in_page_wt
  - wb_in_source_transcl_gen
  - wb_by_cpage
  - wb_by_cpage_agg
  - distinct_props
  - distinct_qids

# MARK: Metrics Tables

# Metrics will be generated for the following tables with the included definitions and other meta data below.

metrics_tables:
  # MARK: missing_sitelinks

  missing_sitelinks:
    - index: "1"
      name: Percent of content pages missing sitelinks
      use_case_category: Basic WD setup
      units_of_analysis_and_aggregation_type: Percentage of content pages, per wiki
      data_source: page_props table
      column: prptn_unexpected_unconnected
      log_transform: False
      viz_type: project_type_scatterplot
      viz_as_percentage: True
      viz_label: |-
        Percent of the wiki's content pages that are
        missing sitelinks
      definition: >-
        On each wiki, count the content pages that do not have a Wikidata sitelink
        and that also do not contain the __EXPECTED_UNCONNECTED_PAGE__ magic word (used to indicate
        that page should not have a sitelink). Then calculate the percentage
        of the wiki's total content pages that this count represents.

  # MARK: wbc_usages_agg

  wbc_usages_agg:
    - index: "2.1"
      name: Wikidata requests from content, per content page
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: wbc_entity_usages table
      column: avg_usages_from_content
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of Wikidata requests caused by page
        content, per content page, per wiki, based on the
        wbc_entity_usage table
      definition: >-
        For each content page, count the Wikidata requests registered in the wbc_entity_usage
        table, other than sitelink, title, description or label requests on the Wikidata entity
        referred to by the page. This count represents wbc_entity_usage records that are triggered by
        Wikidata usages in the page's content (from somewhere in the page's full source code).
        Find the average for this count per content page across all content pages for each wiki.
      limitations: >-
        Some entries in the wbc_entity_usage table represent more actual Wikidata statement
        usage than others. Also, a small number of non-statement usages triggered by a page's content might
        not be properly counted.

    - index: "2.2"
      name: Wikidata entities in requests from content, per content page
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: wbc_entity_usages table
      column: avg_entities_in_usages_from_content
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of Wikidata entities associated with
        requests caused by page content, per content page,
        per wiki, based on the wbc_entity_usage table
      definition: >-
        For each content page, count the Wikidata entities associated with requests
        registered in the wbc_entity_usage table, other than sitelink, title, description
        or label requests on the Wikidata entity referred to by the page. This count represents
        entities associated with wbc_entity_usage records that are triggered by Wikidata usages in the
        page's content (from somewhere in the page's full source code).
        Find the average for this count per content page across all content pages for each wiki.
      limitations: >-
        A small number of non-statement usages triggered by a page's content might
        not be properly counted.

    - index: "2.3"
      name: Percentage of content pages with Wikidata usage from content
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Percentage of content pages, per wiki
      data_source: wbc_entity_usages table
      column: prptn_at_least_1_usage_from_content
      log_transform: False
      viz_type: project_type_scatterplot
      viz_as_percentage: True
      viz_label: |-
        Percentage of the wiki's content pages with
        one or more usages from content, based
        on the wbc_entity_usage table
      definition: >-
        On each wiki, count the content pages where there is one or more Wikidata
        usage from content. Usages from content are defined as Wikidata requests
        registered in the wbc_entity_usage table, other than sitelink, title,
        description or label requests on the Wikidata entity referred to by the page.
        This represents wbc_entity_usage records that are triggered by
        Wikidata usages in the page's content (from somewhere in the page's full source code).
        Finally, calculate the percentage of the wiki's total content pages that this
        count represents.
      limitations: >-
        A small number of non-statement usages triggered by a page's content might
        not be properly counted.

    - index: "2.4"
      name: Statement requests per content page
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: wbc_entity_usages table
      column: avg_graph_traversal_usages
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of Wikidata statements requests, per content
        page, per wiki, based on the wbc_entity_usage table
      definition: >-
        For each content page, count the Wikidata statement requests registered
        in the wbc_entity_usage table ("statement" or "all" usages, represented as "C",
        "C" with a property ID, or "X" in that table). Then find the average for this count
        per content page across
        all content pages for each wiki.
      limitations: >-
        While this may roughly indicate the scale of statement usage on content
        pages in a given wiki, some entries in the wbc_entity_usage table
        represent more actual Wikidata statement usage than others.

    - index: "2.5"
      name: Percentage of content pages that have accurate statement usage counts
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Percentage of content pages, per wiki
      data_source: wbc_entity_usages table
      column: prptn_cps_with_accurate_graph_traversal_counts
      log_transform: False
      viz_type: project_type_scatterplot
      viz_as_percentage: True
      viz_label: |-
        Percentage of the wiki's content pages for
        which statement usage counts are accurate, based
        on request types in the wbc_entity_usage table
      definition: >-
        On each wiki, count the content pages for which we know that our count of statement
        usage for that page is accurate. This is the case for all content pages without
        "all" requests and without "statement" requests that have no associated
        property ("X" or "C" requests in the wbc_entity_usage table). Then calculate the percentage
        of the wiki's total content pages that this count represents.

    - index: "2.6"
      name: Statement usages per content page that has accurate usage counts
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: wbc_entity_usages table
      column: avg_graph_traversal_usages_on_cps_with_accurate_gt_counts
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of Wikidata statements usages, per content
        page, per wiki, for content pages for which we have
        accurate statement usage counts, based on the wbc_entity_usage table
      definition: >-
        For each content page for which we have accurate statement usage counts,
        count the Wikidata statement requests registered in the wbc_entity_usage table.
        Then find the average for this count per content page across
        all content pages for which we have accurate counts, for each wiki.
      limitations: >-
        While this metric is accurate for a certain percentage
        of the pages of each wiki, it is not known whether the true average
        number of statements used across all content pages is higher or lower.

    - index: "2.7"
      name: Percentage of content pages with statement requests
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Percentage of content pages, per wiki
      data_source: wbc_entity_usages table
      column: prptn_at_least_1_graph_traversal_usage
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: True
      viz_label: |-
        Percentage of content pages with statement requests,
        per wiki, based on the wbc_entity_usage table
      definition: >-
        On each wiki, count the content pages where there is one or more Wikidata
        statement requests registered in the wbc_entity_usage table ("statement" or "all"
        usages, represented as "C", "C" with a property ID, or "X" in that table). Then
        calculate the percentage of the wiki's total content pages that this
        count represents.

    - index: "2.8"
      name: Distinct properties per content page in wbc_entity_usage table
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: wbc_entity_usages table
      column: avg_distinct_props
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of distinct Wikidata properties per
        content page, in statement requests in the
        wbc_entity_usage table
      definition: >-
        For each content page, count the properties in statement requests in
        the wbc_entity_usage table, removing any duplicate properties.
        Then find the average for this count per content page across
        all content pages for each wiki.
      limitations: >-
        While this may roughly indicate the scale and diversity of statement usage on
        content pages in a given wiki, some rows registering statement requests in
        that table do not store which property or properties were used for
        the page. This is the case for "all" requests as well as "statement" requests
        that cover multiple properties ("X" or "C" requests in the wbc_entity_usage table).

  wbc_usages_agg_globals: []
  wbc_usages_agg_by_proj_type: []
  wbc_graph_traversal_types: []

  # MARK: top_properties_wbc_by_project_type

  top_properties_wbc_by_project_type:
    - index: "4.4"
      name: Top properties for content pages, in wbc_entity_usage table, per project type
      use_case_category: Nature of statement usage on content pages
      units_of_analysis_and_aggregation_type: Top properties
      data_source: wbc_entity_usages table
      column: in_wbc_eu_count
      viz_type: project_type_top_n_props_barplot
      viz_as_percentage: False
      viz_label: |-
        Number of entries with each
        property in the wbc_entity_usage
        table
      definition: >-
        Count the number of times each property appears in a row in the
        wbc_entity_usage table (attached to a "statement" request,
        represented as "C" with a property) for content pages, per
        project type. Order properties by this count, grouping by project type.
      limitations: >-
        Some rows registering statement requests in wbc_entity_usage table
        do not store which property or properties were used. This is the
        case for "all" requests as well as "statement" requests that cover
        multiple properties ("X" or "C" requests in the wbc_entity_usage table).

  # MARK: top_properties_wbc_all_wikis

  top_properties_wbc_all_wikis:
    - index: "4.2"
      name: Top properties for content pages, in wbc_entity_usage table, all wikis
      use_case_category: Nature of statement usage on content pages
      units_of_analysis_and_aggregation_type: Top properties
      data_source: wbc_entity_usages table
      column: in_wbc_eu_count
      viz_type: global_top_n_props_barplot
      viz_as_percentage: False
      viz_label: |-
        Number of entries with each
        property in the wbc_entity_usage
        table
      definition: >-
        Count the number of times each property appears in a row in the
        wbc_entity_usage table (attached to a "statement" request,
        represented as "C" with a property) for content pages, per
        project type. Order properties by this count.
      limitations: >-
        Some rows registering statement requests in wbc_entity_usage table
        do not store which property or properties were used. This is the
        case for "all" requests as well as "statement" requests that cover
        multiple properties ("X" or "C" requests in the wbc_entity_usage table).

  # MARK: types_of_symbol_by_cp_fsc

  types_of_symbol_by_cp_fsc:
    - index: "3.1"
      name: Wikibase symbols in content pages source code, per content page
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: Content page source code
      column: avg_total_wb_symbols_count
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of Wikibase symbols in
        the full source code of content pages, per wiki
      definition: >-
        Search the Wikitext and Lua code of all templates, modules and content
        pages for all Wikibase symbols (property IDs, QIDs, Wikibase parser
        functions, Lua Wikibase functions), and count total number of symbols.
        If a source code unit (a template or a module) is transcluded (used) by multiple
        content pages, then the Wikibase symbols in
        that source code unit are counted the number of times that the unit
        is transcluded. Find the average for this count per content page across
        all content pages for each wiki.
      limitations: >-
        Source code units can be part of the full source code
        of many pages (pages where they are transcluded), but not all the code they
        contain is necessarily executed for every page, and we don't know which Wikibase symbols
        in the source code are actually used or executed for a given page. The fact that a
        Wikibase symbol appears in a source code unit does not necessarily mean that
        symbol was on the page, but rather, only that it may have been executed.

    - index: "3.2"
      name: Percentage of content pages with Wikibase symbols in full source code
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Percentage of content pages, per wiki
      data_source: Content page source code
      column: prptn_cps_with_any_wbs_in_fsc
      log_transform: False
      viz_type: project_type_scatterplot
      viz_as_percentage: True
      viz_label: |-
        Percentage of content pages with one or more
        Wikibase symbols in their full source code
      definition: >-
        Search the Wikitext and Lua code of all templates, modules and content
        pages for all Wikibase symbols (property IDs, QIDs, Wikibase parser
        functions, Lua Wikibase functions) and count the content pages with
        at least one Wikibase symbol in their full source
        code. Then calculate the percentage of the wiki's total content pages that this
        count represents.
      limitations: >-
        Source code units can be part of the full source code
        of many pages (pages where they are transcluded), but not all the code they
        contain is necessarily executed for every page, and we don't know which properties
        in the source code actually result in statement usages for a given page. The fact that a
        Wikibase symbol appears in a source code unit does not necessarily mean that
        Wikidata was used from content in the page, but rather, only that it may have been used.

    - index: "3.3"
      name: Property parser functions in content page source code, per content page
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: Content page source code
      column: avg_prop_pfs_count
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of property parser functions
        in the full source code of content pages, per wiki
      definition: >-
        Search the Wikitext of all templates and content pages and count
        the Wikibase property parser functions in every content page's full source
        code. If a source code unit (a template) is transcluded
        (used) by multiple content pages, then the parser functions in
        that source code unit are counted the number of times that the unit
        is transcluded. Find the average for this count per content page across
        all content pages for each wiki.
      limitations: >-
        Source code units can be part of the full source code
        of many pages (pages where they are transcluded), but not all the code they
        contain is necessarily executed for every page, and we don't know which parser functions
        in the source code are actually executed for a given page. The fact that a
        parser function appears in a source code unit does not necessarily mean that
        parser function was executed on the page, but rather, only that it may have been executed.

    - index: "3.4"
      name: Statement parser functions in content pages source code, per content page
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: Content page source code
      column: avg_stmts_pfs_count
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of statement parser functions
        in the full source code of content pages, per wiki
      definition: >-
        Search the Wikitext of all templates and content pages and count
        the Wikibase statement parser functions in every content page's full source
        code. If a source code unit (a template or a module) is transcluded
        (used) by multiple content pages, then the parser functions in
        that source code unit are counted the number of times that the unit
        is transcluded. Find the average for this count per content page across
        all content pages for each wiki.
      limitations: >-
        Source code units can be part of the full source code
        of many pages (pages where they are transcluded), but not all the code they
        contain is necessarily executed for every page, and we don't know which parser functions
        in the source code are actually executed for a given page. The fact that a
        parser function appears in a source code unit does not necessarily mean that
        parser function was executed on the page, but rather, only that it may have been executed.

    - index: "3.5"
      name: Lua Wikibase references in content page source code, per content page
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: Content page source code
      column: avg_wb_lua_accesses_count
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of Lua Wikibase references
        in the full source code of content pages, per wiki
      definition: >-
        Search the Wikitext of all Lua modules and count
        the Wikibase references ("mw.wikibase") in every content page's full source
        code. If a source code unit (module) is transcluded
        (used) by multiple content pages, then the Lua Wikibase references in
        that source code unit are counted the number of times that the unit
        is transcluded. Find the average for this count per content page across
        all content pages for each wiki.
      limitations: >-
        Source code units can be part of the full source code
        of many pages (pages where they are transcluded), but not all the code they
        contain is necessarily executed for every page, and we don't know which Lua Wikibase
        references in the source code are actually used for a given page. The fact that a
        Wikibase reference appears in a source code unit does not necessarily mean that
        code was executed on the page, but rather, only that it may have been executed.

    - index: "3.6"
      name: Property references in content page source code, per content page
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: Content page source code
      column: avg_total_prop_refs_count
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of property references
        in the full source code of content pages, per wiki
      definition: >-
        Search the Wikitext and Lua code of all templates, modules and content
        pages and count property references (identifiers or labels) in every
        content page's full source code. If a source code unit (template or module)
        is transcluded (used) by multiple content pages, then the property references in
        that source code unit are counted the number of times that the unit
        is transcluded. Find the average for this count per content page across
        all content pages for each wiki.
      limitations: >-
        Source code units can be part of the full source code
        of many pages (pages where they are transcluded), but not all the code they
        contain is necessarily executed for every page, and we don't know which property
        references in the source code are actually used for a given page. The fact that a
        property reference appears in a source code unit does not necessarily mean that
        code was invoked on the page, but rather, only that it may have been invoked.

    - index: "3.8"
      name: Entity references in content page source code, per content page
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: Content page source code
      column: avg_total_qid_refs_count
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of entity references
        in the full source code of content pages, per wiki
      definition: >-
        Search the Wikitext and Lua code of all templates, modules and content
        pages and count entity references (QIDs) in every
        content page's full source code. If a source code unit (module) is transcluded
        (used) by multiple content pages, then the entity references in
        that source code unit are counted the number of times that the unit
        is transcluded. Find the average for this count per content page across
        all content pages for each wiki.
      limitations: >-
        Source code units can be part of the full source code
        of many pages (pages where they are transcluded), but not all the code they
        contain is necessarily executed for every page, and we don't know which entity
        references in the source code are actually used for a given page. The fact that an
        entity reference appears in a source code unit does not necessarily mean that
        code was executed on the page, but rather, only that it may have been executed.

  types_of_symbol_by_cp_fsc_globals: []

  types_of_symbol_by_cp_fsc_by_proj_type: []

  # MARK: types_of_symbol_by_sc_unit

  types_of_symbol_by_sc_unit:
    - index: "5.1"
      name: Percentage of source code units with Wikibase symbols
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: Content page source code
      column: prptn_sc_units_with_wb_symbols
      log_transform: True
      viz_type: scu_type_scatterplot
      viz_as_percentage: True
      viz_label: |-
        Percentage of source code units with
        one or more Wikibase symbols
      definition: >-
        Search the Wikitext and Lua code of content pages and of all
        templates and modules transcluded in content pages,
        for all Wikibase symbols (property IDs, QIDs, Wikibase parser
        functions, Lua Wikibase functions) and count the source code units
        of each type with at least one Wikibase symbol. Then calculate the
        each source code unit type that this count represents.

  # MARK: top_t_with_wb

  top_t_with_wb:
    - index: "6.1"
      name: Top templates with Wikibase symbols, by content page transclusions
      use_case_category: Nature of statement usage on content pages
      units_of_analysis_and_aggregation_type: Top source code units
      data_source: Content page source code
      column: cp_transclusions_count
      viz_type: global_top_n_scu_barplot
      viz_as_percentage: False
      viz_label: |-
        Number of content pages where each
        template is transcluded
      definition: >-
        Search the Wikitext all templates transcluded in content pages,
        and select only the templates with one or more Wikibase symbols.
        Order templates by number of content page transclusions.
      limitations: >-
        Templates are not shared across wikis. Even if one template was copied
        from one wiki to another, or if templates on two wikis have the same
        name, purpose or associated same WD item, there is no guarantee that
        they will have the same functionality.

  # MARK: top_m_with_wb

  top_m_with_wb:
    - index: "6.2"
      name: Top modules with Wikibase symbols, by content page transclusions
      use_case_category: Nature of statement usage on content pages
      units_of_analysis_and_aggregation_type: Top source code units
      data_source: Content page source code
      column: cp_transclusions_count
      viz_type: global_top_n_scu_barplot
      viz_as_percentage: False
      viz_label: |-
        Number of content pages where each
        module is transcluded
      definition: >-
        Search the Wikitext all modules transcluded in content pages,
        and select only the modules with one or more Wikibase symbols.
        Order modules by number of content page transclusions.
      limitations: >-
        Modules are not shared across wikis. Even if one module was copied
        from one wiki to another, or if modules on two wikis have the same
        name, purpose or associated same WD item, there is no guarantee that
        they will have the same functionality.

  # MARK: top_properties_by_project_type

  top_properties_by_project_type:
    - index: "4.3"
      name: Top properties in content page source code, per project type
      use_case_category: Nature of statement usage on content pages
      units_of_analysis_and_aggregation_type: Top properties
      data_source: Content page source code
      column: in_full_sc_count
      viz_type: project_type_top_n_props_barplot
      viz_as_percentage: False
      viz_label: |-
        Number of content pages where each
        property appears at least once
        in the page's full source code
      definition: >-
        Search the Wikitext and Lua code of all templates, modules and content
        pages for property IDs, and count the content pages where each
        property appears one or more times in a content page's full source
        code. If a source code unit (a template or a module) is transcluded
        (used) by multiple content pages, then the properties in
        that source code unit are counted the number of times that the unit
        is transcluded; in other words, we consider that properties appear
        in the full source code of every content page where the source code unit is transcluded.
        Order properties by this count, grouping by project type.
      limitations: >-
        Source code units can be part of the full source code
        of many pages (pages where they are transcluded), but not all the code they
        contain is necessarily executed for every page, and we don't know which properties
        in the source code actually result in statement usages for a given page. The fact that a property
        ID appears in a source code unit does not necessarily mean that
        a statement with that property was used in the page, but rather, only
        that it may have been used.

  # MARK: top_properties_all_wikis

  top_properties_all_wikis:
    - index: "4.1"
      name: Top properties in full content page source code, all wikis
      use_case_category: Nature of statement usage on content pages
      units_of_analysis_and_aggregation_type: Top properties
      data_source: Content page source code
      column: in_full_sc_count
      viz_type: global_top_n_props_barplot
      viz_as_percentage: False
      viz_label: |-
        Number of content pages where each
        property appears at least once
        in the page's full source code
      definition: >-
        Search the Wikitext and Lua code of all templates, modules and content
        pages for property IDs, and count the content pages where each
        property appears one or more times in a content page's full source
        code. If a source code unit (a template or a module) is transcluded
        (used) by multiple content pages, then the properties in
        that source code unit are counted the number of times that the unit
        is transcluded; in other words, we consider that properties appear
        in the full source code of every content page where the source code unit is transcluded.
        Order properties by this count.
      limitations: >-
        Source code units can be part of the full source code
        of many pages (pages where they are transcluded), but not all the code they
        contain is necessarily executed for every page, and we don't know which properties
        in the source code actually result in statement usages for a given page. The fact that a property
        ID appears in a source code unit does not necessarily mean that
        a statement with that property was used in the page, but rather, only
        that it may have been used.

  # MARK: avg_distinct_props_and_qids_per_cp

  avg_distinct_props_and_qids_per_cp:
    - index: "3.7"
      name: Distinct properties in content page source code, per content pages
      use_case_category: Scale of statement usage on content pages
      units_of_analysis_and_aggregation_type: Average across content pages, per wiki
      data_source: Content page source code
      column: avg_distinct_prop_ids
      log_transform: True
      viz_type: project_type_scatterplot
      viz_as_percentage: False
      viz_label: |-
        Average number of distinct properties
        in the full source code of content pages, per wiki
      definition: >-
        Search the Wikitext and Lua code of all templates, modules and content
        pages for property references (identifiers or labels) in every
        content page's full source code. Count the number of distinct
        properties referred to in the full source code. If a source code unit
        (template or module) is transcluded
        (used) by multiple content pages, then the properties referred to in
        that source code unit are considered to be included in the full source
        code of that content page. Find the average for this count per content page across
        all content pages for each wiki.
      limitations: >-
        Source code units can be part of the full source code
        of many pages (pages where they are transcluded), but not all the code they
        contain is necessarily executed for every page, and we don't know which property
        references in the source code are actually used for a given page. The fact that a
        property appears in a source code unit does not necessarily mean that
        code was used on the page, but rather, only that it may have been used.

# MARK: cp_distributions_viz

cp_distributions_viz:
  - index_ref: "3.6"
    name: Distribution of property references in content page source code, per content page
    use_case_category: Scale of statement usage on content pages
    units_of_analysis_and_aggregation_type: Distribution of values across a given set of content pages
    data_source: Content page source code
    column: total_prop_refs_count
    tmp_table: wb_by_cpage_agg
    log_transform: True
    viz_label: |-
      Total number of property references
      in the full source code of content pages

  - index_ref: "2.4"
    name: Distribution of statement requests by content page
    use_case_category: Scale of statement usage on content pages
    units_of_analysis_and_aggregation_type: Distribution of values across a given set of content pages
    data_source: wbc_entity_usages table
    column: graph_traversal_usages
    tmp_table: wbc_usage_counts
    log_transform: True
    viz_label: |-
      Total number of Wikidata statements requests
      from content pages, based on the
      wbc_entity_usage table

# MARK: trends_viz

trends_viz:
  - index_ref: "3.6"
    name: Property references in content page source code, per content page
    use_case_category: Scale of statement usage on content pages
    units_of_analysis_and_aggregation_type: Average across content pages
    data_source: Content page source code
    column: avg_total_prop_refs_count
    table_base_name: types_of_symbol_by_cp_fsc
    viz_as_percentage: False
    viz_label: |-
      Average number of property references
      in the full source code of
      content pages

  - index_ref: "2.7"
    name: Percentage of content pages with statement requests
    use_case_category: Scale of statement usage on content pages
    units_of_analysis_and_aggregation_type: Percentage of content pages
    data_source: wbc_entity_usages table
    column: prptn_at_least_1_graph_traversal_usage
    table_base_name: wbc_usages_agg
    viz_as_percentage: True
    viz_label: |-
      Percentage of content pages with statement
      requests, based on the wbc_entity_usage
      table
