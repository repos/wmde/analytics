import yaml
import os
import pandas as pd


class Tables(dict):
    def __init__(self, tbls_list, prefix, suffix):
        for tbl in tbls_list:
            setattr(self, tbl, (prefix + tbl + suffix).replace("-", "_"))

    def __getattr__(self, key):
        return self[key]

    def __setattr__(self, key, value):
        self[key] = value


def load_config():
    path = os.path.dirname(__file__)

    # Hardcoded relative path to config.yaml.
    config_filename = os.path.join(path, "../../config.yaml")

    with open(config_filename, "r") as stream:
        config = yaml.safe_load(stream)

    tbls = Tables(
        tbls_list=config["metrics_tables"].keys(),
        prefix=config["schema"] + ".",
        suffix="__" + config["snapshot"],
    )

    tmp_tbls = Tables(
        tbls_list=config["temporary_tables"],
        prefix=config["schema"] + ".",
        suffix="__tmp_" + config["snapshot"],
    )

    metrics_array = []
    for table, metrics_for_table in config["metrics_tables"].items():
        for m in metrics_for_table:
            metrics_array.append(m | {"table": table})

    metrics = pd.DataFrame(metrics_array).sort_values("index")

    return (config, tbls, tmp_tbls, metrics)
