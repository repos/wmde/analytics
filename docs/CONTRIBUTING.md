# [Contributing](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/CONTRIBUTING.md)

> Helpful resources and expected coding conventions for Software Analytics at Wikimedia Deutschland (WMDE SWE Analytics)

<a id="contents"></a>

### **Contents**

- [Other Resources](#other-resources-)
- [Environment](#environment-)
- [Stat Machine](#stat-machine-)
- [Python](#python-)
- [SQL](#sql-)
- [SPARQL](#sparql-)
- [Merge Requests](#merge-requests-)
- [pre-commit](#pre-commit-)

<a id="other-resources-"></a>

## Other Resources [`⇧`](#contents)

The following are other helpful docs and files:

- [Analytics Platform Access](https://docs.google.com/document/d/1YYV-FDhu3OeLIT4ST2WakoVuyxwOZLwDqWVket4YD7k/edit?usp=sharing): Login procedures necessary for WMDE SWE Analytics work
- [Analytics Resources](https://docs.google.com/document/d/1ApzguzIgZIIC2anCDvoOIaaX90rb-sOb3n3onKyNLf0/edit?usp=sharing): Articles and codebases to explore and reference
- [onboarding_data_access.ipynb](https://gitlab.wikimedia.org/repos/wmde/analytics/-/blob/main/docs/onboarding_data_access.ipynb): Steps to start querying the [Data Lake](https://wikitech.wikimedia.org/wiki/Data_Platform/Data_Lake) using [wmfdata-python](https://gitlab.wikimedia.org/repos/data-engineering/wmfdata-python)

<a id="environment-"></a>

## Environment [`⇧`](#contents)

You can set up your [conda development environment](https://conda.io/projects/conda/en/latest/user-guide/getting-started.html) via the following commands in the project root:

```bash
conda env create -f conda-environment.yaml
conda activate wmde-analytics
```

### VS Code

If you're using [VS Code](https://code.visualstudio.com/), be sure to accept the prompt asking if you'd like it to load the new development environment as the default for the workspace when you run the commands above.

> Important: Please install the suggested VS Code extensions in the dropdown below to improve your development experience. These are also included in [.vscode/extensions.json](https://gitlab.wikimedia.org/repos/wmde/analytics/-/blob/main/.vscode/extensions.json?ref_type=heads), so you should be prompted to install them when you open the project.
>
> <details><summary>Suggested VS Code extensions</summary>
>
> <p>
>
> - [aaron-bond.better-comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments)
> - [blokhinnv.wikidataqidlabels](https://marketplace.visualstudio.com/items?itemName=blokhinnv.wikidataqidlabels)
> - [charliermarsh.ruff](https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff)
> - [ms-python.black-formatter](https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter)
> - [ms-python.python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
> - [ms-toolsai.jupyter](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter)
> - [mshdinsight.azure-hdinsight](https://marketplace.visualstudio.com/items?itemName=mshdinsight.azure-hdinsight)
> - [streetsidesoftware.code-spell-checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
>
> </p>
> </details>

> Note: Those using the [Better Comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments) extension should base their tag customization on the following:
>
> <details><summary>Better Comments tags</summary>
>
> ```json
> "better-comments.tags": [
>   {
>       "tag": "attn:",
>       "color": "#FF2D00",
>       "strikethrough": false,
>       "underline": false,
>       "backgroundColor": "transparent",
>       "bold": false,
>       "italic": false
>   },
>   {
>       "tag": "todo:",
>       "color": "#FF8C00",
>       "strikethrough": false,
>       "underline": false,
>       "backgroundColor": "transparent",
>       "bold": false,
>       "italic": false
>   },
>   {
>       "tag": "note:",
>       "color": "#98C379",
>       "strikethrough": false,
>       "underline": false,
>       "backgroundColor": "transparent",
>       "bold": false,
>       "italic": false
>   },
>   {
>       "tag": "see:",
>       "color": "#61A0Af",
>       "strikethrough": false,
>       "underline": false,
>       "backgroundColor": "transparent",
>       "bold": false,
>       "italic": false
>   },
>   {
>       "tag": "mark:",
>       "color": "#F62DAE",
>       "strikethrough": false,
>       "underline": false,
>       "backgroundColor": "transparent",
>       "bold": false,
>       "italic": false
>   }
> ]
> ```
>
> </details>

<a id="stat-machine-"></a>

## Stat Machine [`⇧`](#contents)

The suggested [stat machine](https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Clients) for WMDE SWE Analytics is [stat1010](https://wikitech.wikimedia.org/wiki/Stat1010). Using the same stat machine makes transferring files a bit more seamless.

You can use the following commands to access the machine:

```bash
# Command line access:
ssh stat1010.eqiad.wmnet

# Routed access to localhost 8880:
ssh -N stat1010.eqiad.wmnet -L 8880:127.0.0.1:8880

# If necessary, authenticate with Kerberos in the command line or a Jupyter terminal:
kinit  # enter your Kerberos password
```

Once in the machine you'll be able to access [Hadoop](https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Hadoop) and [MariaDB](https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/MariaDB) servers. See the Analytics Platform Access doc on Google Drive for an overview of how to log into the various tools we use.

<a id="python-"></a>

## Python [`⇧`](#contents)

[Python](https://www.python.org/) is the main coding language for WMDE SWE Analytics. We use it for all exploratory tasks and defining our [Airflow DAGs](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde). For [Jupyter](https://jupyter.org/) based work, please use [task_template.ipynb](https://gitlab.wikimedia.org/repos/wmde/analytics/-/blob/main/tasks/task_template.ipynb) for common imports and to assure that needed information is included. You can also leverage functions in [analytics_utils.py](https://gitlab.wikimedia.org/repos/wmde/analytics/-/blob/main/tasks/analytics_utils.py) that solve common problems in day to day work.

### Formatting

It's expected that you will use the [Ruff formatter](https://docs.astral.sh/ruff/formatter/), which is based on and compatible with [black](https://github.com/psf/black). For [Jupyter](https://jupyter.org/) based work, please use [jupyter-black](https://github.com/n8henrie/jupyter-black) in every notebook to format your code each time you run a cell.

> Note: Comments that are on their own line should be full sentences with capitalization and punctuation, while inline comments should not be capitalized or include punctuation ([source](https://peps.python.org/pep-0008/#comments)).

### Documentation

We generally follow [numpydoc conventions](https://numpydoc.readthedocs.io/en/latest/format.html) for documenting functions and Python code in general. Function docstrings should have the following format:

```python
def example_function(argument: argument_type) -> return_type:
    """
    An example docstring for a function so others understand your work.

    Parameters
    ----------
    argument : argument_type
        Description of your argument.

    Returns
    -------
    return_value : return_type
        Description of your return value.

    Raises
    ------
    ErrorType
        Description of the error and the condition that raises it.
    """

    ...

    return return_value
```

### Packages

The following is a list of useful packages for your work:

- [wmfdata-python](https://gitlab.wikimedia.org/repos/data-engineering/wmfdata-python): Tools for working with Wikimedia data on the internal analytics clients
- [NumPy](https://github.com/numpy/numpy): Multidimensional arrays and mathematical functions to operate on them
- [Pandas](https://github.com/pandas-dev/pandas): Data structures and operations for manipulating numerical tables
- [Matplotlib](https://github.com/matplotlib/matplotlib): Plotting and data visualization
- [Seaborn](https://github.com/mwaskom/seaborn): Based on Matplotlib, data visualization that has nicer looking plots
- [tqdm](https://github.com/tqdm/tqdm): Progress bars so you know how long your processes will take
- [pathlib](https://docs.python.org/3/library/pathlib.html): Easy class-based path library

<a id="sql-"></a>

## SQL [`⇧`](#contents)

It's generally preferable to write Spark based operations in Spark SQL rather than PySpark. This allows the queries to be more easily operationalized using Airflow, with SQL dialects further being easier for stakeholders and other SWE teammates to understand. WMDE SWE Analytics has strict conventions for all SQL, HiveQL and Spark SQL queries. Following these conventions makes code review dramatically easier as reviewers will be looking at consistent formatting.

An example query is:

```sql
SELECT
    e.wiki AS wiki,
    e.id AS edit_id,
    t.edit_text AS edit_text,
    regexp_extract(t.edit_text, 'REGEX_FOR_CHANGES', 1) AS text_changes

FROM
    wmde.hypothetical_wiki_edits_table AS e

JOIN
    wmde.hypothetical_edit_texts_table AS t

ON
    e.id = t.edit_id

WHERE
    e.snapshot = 'SNAPSHOT_OF_INTEREST'
    AND t.snapshot = 'SNAPSHOT_OF_INTEREST'
    -- Edit is on content and user pages.
    AND e.namespace IN (0, 2)

ORDER BY
    wiki DESC
;
```

What's included above is:

- Uppercase all keywords
- Break to a new line after all clause keywords
- Indent with four spaces so SQL code consistent with Python code
- Commas are at the end of lines
- `AND` and `OR` are at the beginning of lines
- Lowercase all functions
- Do not put spaces between parentheses and their contents
- If one column in a `SELECT` statement is renamed with `AS`, all should be
- Alias table names with `AS` and identifiers that are as short as possible if you're joining tables
- Always include `DESC` or `ASC` in `ORDER BY` clauses
- Use verbose column names over shorter ones that could be ambiguous
- End queries with a semicolon so they can be easily copied into Hive or Spark SQL

In addition:

- Comments should follow the Python conventions mentioned above
- Use `MARK:` comments to map out complex queries so they're easier to navigate via minimaps
- Put line breaks after each `SELECT` column if any operation is long enough to break onto multiple lines

SQL formatting can get quite complicated at times. Do your best, and we'll be happy to help during reviews!

> Note: Please familiarize yourself with [Spark SQL Built in Functions](https://archive.apache.org/dist/spark/docs/3.1.3/api/sql/).

> Note: Regular expressions are used a lot in our queries. We'd suggest [regex101](https://regex101.com/) for expression testing.

<a id="sparql-"></a>

## SPARQL [`⇧`](#contents)

SPARQL queries should follow the above SQL guidelines where applicable. If you're new to SPARQL, we'd suggest checking out the [Wikidata SPARQL tutorial](https://www.wikidata.org/wiki/Wikidata:SPARQL_tutorial) and [example SPARQL queries](https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/queries/examples) as first steps.

An example query is:

```sql
SELECT
    ?querySubject
    ?querySubjectLabel

WHERE {
    ?querySubject wdt:queryPredicate wd:queryObject.

    SERVICE wikibase:label {
        bd:serviceParam wikibase:language "[AUTO_LANGUAGE], en".
    }
}
```

<a id="merge-requests-"></a>

## Merge Requests [`⇧`](#contents)

Please include the Phabricator task id (`T#`) in your merge request title and follow the [merge request template for the repository](https://gitlab.wikimedia.org/repos/wmde/analytics/-/blob/main/.gitlab/MERGE_REQUEST_TEMPLATE.md?ref_type=heads). Doing so creates hyperlinks on GitLab that allow for easier investigations of tasks.

<a id="pre-commit-"></a>

## pre-commit [`⇧`](#contents)

[pre-commit](https://pre-commit.com/) is a tool to enforce many of the above coding standards on every commit. [pre-commit](https://pre-commit.com/) hooks are used to check code for pre-defined conventions and prevent a `git commit` if they have not been followed. Our pre-commit configuration can be found in [.pre-commit-config.yaml](https://gitlab.wikimedia.org/repos/wmde/analytics/-/blob/main/.pre-commit-config.yaml?ref_type=heads).

Please install [pre-commit](https://pre-commit.com/) in your local environment via [pip](https://pip.pypa.io/en/stable/) with the following command:

```bash
pip install pre-commit
```

From there you can install the [pre-commit](https://pre-commit.com/) hooks and trigger them automatically with the following commands:

```bash
pre-commit install
pre-commit run --all-files
```

Suggestions for new [pre-commit](https://pre-commit.com/) hooks are very welcome!
