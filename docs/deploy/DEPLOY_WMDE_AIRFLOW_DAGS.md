# [Deploy WMDE Airflow DAGs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/deploy/DEPLOY_WMDE_AIRFLOW_DAGS.md)

> Deploying WMDE Airflow DAGs to production

Sources:

- [wikitech.wikimedia.org/wiki/Data_Platform/Systems/Airflow/Instances#wmde](https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Airflow/Instances#wmde)

<a id="contents"></a>

### Contents

- [Set Up Tables](#set-up-tables-)
- [Set Up Directories](#set-up-directories-)
- [Deployment](#deployment-)
- [Running DAGs](#running-dags-)
- [Troubleshooting](#troubleshooting-)

<a id="set-up-tables-"></a>

## Set Up Tables [`⇧`](#contents)

> Note: Column names should include the "what" of the data that they contain. For example, metric columns that count should start with `total_`.

We need to create tables using the `analytics-wmde` user that is running our Airflow jobs to assure that it has access during DAG runs. This is done via the following commands:

```bash
# Log into your stat server of choice:
ssh statSTAT_NUM.eqiad.wmnet
kinit  # if necessary, then enter your Kerberos password

sudo -u analytics-wmde kerberos-run-command analytics-wmde spark3-sql -f FILE_TO_RUN_AND_ARGUMENTS_AS_DEFINED_IN_DOC_STRING
```

To sudo into Spark using `analytics-wmde` you can run the following command:

```bash
sudo -u analytics-wmde kerberos-run-command analytics-wmde spark3-sql --master yarn --executor-memory 8G --executor-cores 4 --driver-memory 2G --conf spark.dynamicAllocation.maxExecutors=64
```

From here you can run the needed Spark SQL queries:

```sql
USE YOUR_HIVE_USER_DB;
SHOW TABLES;
-- Run your production create table statement.
```

> Note: If you need help, feel free to contact WMF admins with the finalized create table query that should be ran. They're happy to support!

<a id="set-up-directories-"></a>

## Set Up Directories [`⇧`](#contents)

Specifically for directories, it's important to make sure that the `analytics-wmde` user has appropriate permissions to save, read and move files. If you're making a directory with your user account, then that account will be the owner and `analytics-wmde` won't have adequate permissions. WMF admins can change these permissions for you, but we can also make tables with the correct permissions by using sudo to impersonate the `analytics-wmde` user. Your HDFS user should be set up with permissions to impersonate `analytics-wmde`, or if not ask WMF admins to give you these permissions. From there you can make directories that `analytics-wmde` has access to via a command like the following:

```bash
sudo -u analytics-wmde kerberos-run-command analytics-wmde hdfs dfs --COMMAND
```

<a id="deployment-"></a>

## Deployment [`⇧`](#contents)

Once you've finished the process in [TEST_AIRFLOW_DAGS.md](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/test/TEST_AIRFLOW_DAGS.md), it's now time to merge the changes and interact with the new or updated DAGs. The new changes will automatically be deployed once the merge request on GitLab has been brought in. From there you can log in to the [WMDE Airflow instance](https://airflow-wmde.wikimedia.org) and authenticate to then see the Airflow UI with the new or updated DAGs.

<a id="running-dags-"></a>

## Running DAGs [`⇧`](#contents)

New DAGs will be loaded into the Airflow UI automatically. If you're deploying edits to an already running DAG, then note that you'll likely need to delete the variables for the DAG so that they can be recreated and the DAG can be reinterpreted. This is done in the `Admin` > `Variables` section of the Airflow UI. Make sure to check the `last_parsed_time` of each changed DAG to make sure that the variables have updated successfully. From there you can unpause the DAG in the `DAGs` tab and check to make sure that jobs are behaving as expected.

If the jobs are succeeding, congratulations! You can now exit out of your SSH tunnels :)

<a id="troubleshooting-"></a>

## Troubleshooting [`⇧`](#contents)

- For debugging a failed DAG in production:

  ```bash
  ssh statSTAT_NUM.eqiad.wmnet
  sudo -u analytics-wmde yarn logs -appOwner analytics-wmde -applicationId APPLICATION_ID | less
  ```

  > Note: If you are asked for a sudo password, then you need to ask WMF Data Engineering to be made a sudo user to be able to impersonate `analytics-wmde`.

- Sometimes a DAG might go to retry but actually succeed because of various issues. In this case you may have repeat values in your table. The best way to remove them is with the following query:

  ```sql
  INSERT OVERWRITE TABLE TABLE_NAME SELECT DISTINCT * FROM TABLE_NAME ;
  ```
