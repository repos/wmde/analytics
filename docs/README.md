# [Docs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs)

> Docs for regular processes

### Contents

- [Deploy](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/deploy)
  - Steps for deploying infrastructure
- [Test](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/test)
  - Steps for testing infrastructure
- [CONTRIBUTING.md](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/CONTRIBUTING.md)
  - Helpful resources and expected coding conventions
- [METRICS_FRAMEWORK.md](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/METRICS_FRAMEWORK.md)
  - A framework for defining metrics of interest and how to derive them
- [onboarding_data_access.ipynb](https://gitlab.wikimedia.org/repos/wmde/analytics/-/blob/main/docs/onboarding_data_access.ipynb)
  - Onboarding notebook for accessing data from the data lake
