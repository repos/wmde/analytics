# [Metrics Framework](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/METRICS_FRAMEWORK.md)

> A framework for defining metrics of interest for Software Analytics at Wikimedia Deutschland (WMDE SWE Analytics)

<a id="contents"></a>

### **Contents**

- [Overview](#overview-)
- [Metric Components](#metric-components-)
  - [Interval](#interval-)
  - [Activity Level](#activity-level-)
  - [Page Type](#page-type-)
  - [Actor Type](#actor-type-)
  - [Action Type](#action-type-)
  - [Acronym](#acronym-)
  - [Full Identifier](#full-identifier-)
- [Action Metrics](#action-metrics-)
- [Actor Metrics](#actor-metrics-)
  - [MediaWiki Metric Conversions](#mediawiki-metric-conversions-)
- [Content Metrics](#content-metrics-)
  - [Quality Items](#quality-items-)
  - [Quality Articles](#quality-articles-)
- [Instance Metrics](#instance-metrics-)
  - [Content Level](#content-level-)
- [Code Examples](#code-examples-)
  - [Content Edits](#content-edits-)

<a id="overview-"></a>

## Overview [`⇧`](#contents)

The following sections have breakdowns of prospective metrics that WMDE SWE Analytics could focus on. The goal is to be expansive and use structured terminology such that the team and the rest of SWE can understand what is being spoken about as explicitly as possible.

> **Note: Not all of the following metrics will be used, but defining them allows the team and greater department to more easily distinguish between the different variants.**

See also WMF and MediaWiki metric definition pages:

<details><summary>Metrics Sources</summary>
<p>

- [MediaWiki Metric Definitions](https://www.mediawiki.org/wiki/Analytics/Metric_definitions)
- [WMF Research Content Measurement Definitions](https://meta.wikimedia.org/wiki/Research:Knowledge_Gaps_Index/Measurement/Content)
- [Research and Decision Science/Data glossary](https://www.mediawiki.org/wiki/Wikimedia_Product/Data_dictionary)
- [Research:Standard_metrics](https://meta.wikimedia.org/wiki/Research:Standard_metrics)
- [Research and Decision Science/Data glossary/Clickthrough Rate](https://meta.wikimedia.org/wiki/Research_and_Decision_Science/Data_glossary/Clickthrough_Rate#Glossary)
- [Research and Decision Science/Data glossary/Retention Rate](https://meta.wikimedia.org/wiki/Research_and_Decision_Science/Data_glossary/Retention_Rate)
- [Research and Decision Science/Data glossary/Campaign Sign-up Rate](https://meta.wikimedia.org/wiki/Research_and_Decision_Science/Data_glossary/Campaign_Sign-up_Rate)

</p>
</details>

<a id="metric-components-"></a>

## Metric Components [`⇧`](#contents)

The following are the building blocks that could be used to define metrics of interest.

<a id="interval-"></a>

### `Interval` [`⇧`](#contents)

> The time span in which the actions take place

- **daily**, **weekly**, **monthly**, **quarterly**, **yearly**

<a id="activity-level-"></a>

### `Activity Level` [`⇧`](#contents)

> Grouping of actors (or instances) given the number of actions in the period

- **extremely active**: >=1,000 actions
- **very active**: >=100 actions
- **active**: >=5 actions
- **casual**: >=1 and <5 actions
- **dormant**: No actions in the period

> Important: Please note that **extremely active** and **very active** are included in **active** as **casual** is the only `activity level` that has an upper bound. These groups should be calculated in code as separate values such that total actors in the period can be derived via `dormant + casual + active + very + extremely`, but when discussing active groups we always mean the current group and all of those in the more active groups above.

> Note: In the metric definition tables below we'll list **active** before **extremely active** and **very active** as analytics industry **DAU** - **WAU** - **MAU** active user standards were a concept we focussed on when creating this system.

<a id="page-type-"></a>

### `Page Type` [`⇧`](#contents)

> Grouping given the namespace on which the action happens

- **any**: Any namespaces on the project (not included in acronyms)
- **content**: Main namespace and other associated content pages
- **talk**: All pages within talk namespaces

> Note: The **content** `page type` can be broken down further based on the type of content like Wikidata items, properties, etc.

<a id="actor-type-"></a>

### `Actor Type` [`⇧`](#contents)

> Grouping given the signed in status of the actor

- **user**: Signed in (not known as a bot at any time)
- **bot**: Signed in (known as a bot at any time)
- **temporary**: Signed out [temporary accounts](https://www.mediawiki.org/wiki/Help:Temporary_accounts)
- **device**: Signed in or signed out (equivalent of **any**)

<a id="action-type-"></a>

### `Action Type` [`⇧`](#contents)

> Grouping given the type of action being done

- **any**: Any actions in recent changes (read as "action" in full identifiers)
- **edit**: Non-redirect page edit actions
- **view**: Page view actions
- **request**: Calls to APIs and other services
- **upload**: Uploads to Wikimedia Commons

> Note: An edit in `xwiki` and `ywiki` within the same period qualifies the editor as an **xwiki-ywiki co-editor** for the period. Breakdowns for `activity level` can be expanded to **co-editors** as in the [**Actor Metrics**](#actor-metrics-) below. An **active co-editor** would thus have made five or more edits in both wikis during a period.

<a id="acronym-"></a>

### `Acronym` [`⇧`](#contents)

> How the metric can be abbreviated

To simplify acronyms that will be in use more often, the default `actor type` below is **user**. Acronyms for **bots**, **temporary accounts** and **devices** will include **B**, **T** and **D** respectively in combination with their `action type`, whereas **users** with an action type are referred to with only the action.

- For actions: **DE** for **daily edit** and not **DUE**
- For actors: **DAE** for **daily active editor** and not **DAUE**

<a id="full-identifier-"></a>

### `Full Identifier` [`⇧`](#contents)

> How the metric can be referred to

It could be the case that some `acronyms` are used in regular communication. The full stated names of identifiers are listed below to make sure that the exact meanings are understood and that long forms are defined for those metrics that `acronyms` are not suitable for.

<a id="action-metrics-"></a>

## Action Metrics [`⇧`](#contents)

The following table outlines the possible actions that could be included in analysis. Note that `activity level` is not included below, which isn't to say that actions couldn't be looked into for actors within an `activity level` group (i.e. **daily active editor edits - DAEE**).

| interval  | page type | actor type | action type | acronym | full identifier                  |
| :-------- | :-------- | :--------- | :---------- | :------ | :------------------------------- |
| daily     | any       | user       | any         | DA      | daily action                     |
| daily     | any       | user       | edit        | DE      | daily edit                       |
| daily     | any       | user       | \*          | D\*     | daily \*                         |
| daily     | any       | bot        | any         | DBA     | daily bot action                 |
| daily     | any       | bot        | edit        | DBE     | daily bot edit                   |
| daily     | any       | bot        | \*          | DB\*    | daily bot \*                     |
| daily     | any       | temporary  | any         | DTA     | daily temporary (account) action |
| daily     | any       | temporary  | edit        | DTE     | daily temporary (account) edit   |
| daily     | any       | temporary  | \*          | DT\*    | daily temporary (account) \*     |
| daily     | any       | device     | any         | DDA     | daily device action              |
| daily     | any       | device     | edit        | DDE     | daily device edit                |
| daily     | any       | device     | \*          | DD\*    | daily device \*                  |
| daily     | content   | \*         | \*          | DC\*    | daily content \*                 |
| daily     | talk      | \*         | \*          | DT\*    | daily talk \*                    |
| weekly    | \*        | \*         | \*          | W\*     | weekly \*                        |
| monthly   | \*        | \*         | \*          | M\*     | monthly \*                       |
| quarterly | \*        | \*         | \*          | Q\*     | quarterly \*                     |
| yearly    | \*        | \*         | \*          | Y\*     | yearly \*                        |

<a id="actor-metrics-"></a>

## Actor Metrics [`⇧`](#contents)

The following table outlines the possible actors that could be included in analysis. In contrast with the [**Action Metrics**](#action-metrics-) above, these definitions do include `activity level` as we should be subsetting by one of these when looking into groups of actors on the projects.

| interval  | activity level   | page type | actor type | action type | acronym | full identifier                  |
| :-------- | :--------------- | :-------- | :--------- | :---------- | :------ | :------------------------------- |
| daily     | active           | any       | user       | any         | DAU     | daily active user                |
| daily     | active           | any       | user       | edit        | DAE     | daily active editor              |
| daily     | active           | any       | user       | \*          | DA\*    | daily active \*                  |
| daily     | active           | any       | bot        | any         | DAB     | daily active bot                 |
| daily     | active           | any       | bot        | edit        | DABE    | daily active bot editor          |
| daily     | active           | any       | bot        | \*          | DAB\*   | daily active bot \*              |
| daily     | active           | any       | temporary  | any         | DAT     | daily active temporary (account) |
| daily     | active           | any       | temporary  | edit        | DATE    | daily active temporary editor    |
| daily     | active           | any       | temporary  | \*          | DAT\*   | daily active temporary \*        |
| daily     | active           | any       | device     | any         | DAD     | daily active device              |
| daily     | active           | any       | device     | edit        | DADE    | daily active device editor       |
| daily     | active           | any       | device     | \*          | DAD\*   | daily active device \*           |
| daily     | active           | content   | \*         | \*          | DAC\*   | daily active content \*          |
| daily     | active           | talk      | \*         | \*          | DAT\*   | daily active talk \*             |
| daily     | extremely active | \*        | \*         | \*          | DE\*    | daily extremely active \*        |
| daily     | very active      | \*        | \*         | \*          | DV\*    | daily very active \*             |
| daily     | casual           | \*        | \*         | \*          | DC\*    | daily casual \*                  |
| daily     | dormant          | \*        | \*         | \*          | DD\*    | daily dormant \*                 |
| weekly    | \*               | \*        | \*         | \*          | W\*     | weekly \*                        |
| monthly   | \*               | \*        | \*         | \*          | M\*     | monthly \*                       |
| quarterly | \*               | \*        | \*         | \*          | Q\*     | quarterly \*                     |
| yearly    | \*               | \*        | \*         | \*          | Y\*     | yearly \*                        |

<a id="mediawiki-metric-conversions-"></a>

### MediaWiki Metric Conversions [`⇧`](#contents)

Referring to the [MediaWiki Metric Definitions](https://www.mediawiki.org/wiki/Analytics/Metric_definitions), active editors and active users are defined within the framework above as:

- **MediaWiki active editor: MACE - monthly active content editor**
- **MediaWiki active user: MCU - monthly casual user**

<a id="content-metrics-"></a>

## Content Metrics [`⇧`](#contents)

Content is defined as any main namespace page across Wikimedia Projects including Wikibase items, Wikipedia articles, etc.

<a id="quality-items-"></a>

### Quality Items [`⇧`](#contents)

Note that there is currently no agreed to definition of a quality Wikibase item. Quality items could be defined such that they are at least Class C on [Wikidata:Item quality](https://www.wikidata.org/wiki/Wikidata:Item_quality) - items containing most critical statements, with some references, translations, aliases and sitelinks.

<a id="quality-articles-"></a>

### Quality Articles [`⇧`](#contents)

We follow WMF's [definition of a quality article](https://meta.wikimedia.org/wiki/Research:Knowledge_Gaps_Index/Measurement/Content#Standard_Quality_Criteria) for Wikipedia research, that an article is of standard+ quality if it meets at least 5 of the 6 following criteria:

- It is at least 8kB long in size
- It has at least 1 category
- It has at least 7 sections
- It is illustrated with 1 or more images
- It has at least four references
- It has 2 or more intra wiki links

<a id="instance-metrics-"></a>

## Instance Metrics [`⇧`](#contents)

Wikimedia Deutschland also has the concept of [Wikibase](https://wikiba.se/) instances for hosted [Wikibase Cloud](https://www.wikibase.cloud/) and self-hosted [Wikibase Suite](https://github.com/wmde/wikibase-release-pipeline/tree/main). Instances are similar to Wikidata, but are normally focused on a single topic. They should not be confused with [Wikimedia Projects](https://meta.wikimedia.org/wiki/Complete_list_of_Wikimedia_projects). Instances are subsetted based on their `activity level` and the amount of content that has been created by its community.

> Note: Metrics are collected for all [Wikibase Cloud](https://www.wikibase.cloud/) instances and [Wikibase Suite](https://github.com/wmde/wikibase-release-pipeline/tree/main) instances that enable the [monthly pingback mechanism](https://doc.wikimedia.org/Wikibase/master/php/docs_topics_pingback.html) via [wikibase-metadata](https://github.com/wmde/wikibase-metadata).

To simplify acronyms that will be in use more often, the default `actor type` is **user** as above, but we will not include **U** in acronyms. All acronyms further end in **I** to differentiate them from those in the [**Actor Metrics**](#actor-metrics-) above.

| interval | activity level   | page type | actor type | action type | acronym | full identifier                    |
| :------- | :--------------- | :-------- | :--------- | :---------- | :------ | :--------------------------------- |
| daily    | active           | any       | user       | any         | DAI     | daily active instance              |
| daily    | active           | any       | user       | edit        | DAEI    | daily active editor instance       |
| daily    | active           | \*        | \*         | \*          | DA\*I   | daily active \* instance           |
| daily    | extremely active | \*        | \*         | \*          | DE\*I   | daily extremely active \* instance |
| daily    | very active      | \*        | \*         | \*          | DV\*I   | daily very active \* instance      |
| daily    | casual           | \*        | \*         | \*          | DC\*I   | daily casual \* instance           |
| daily    | dormant          | \*        | \*         | \*          | DD\*I   | daily dormant \* instance          |
| \*       | \*               | \*        | \*         | \*          | \*I     | \* instance                        |

> Note: The above schema can also be used to classify wiki content like items, properties, etc.

<a id="content-level-"></a>

### `Content Level` [`⇧`](#contents)

> Grouping given the amount of content pages on an instance

- **created**: <=1 page
- **stub**: >=2 and <20 pages
- **established**: >=20 pages (boolean)
- **extra small**: >=20 and <100 pages
- **small**: >=100 and <1,000 pages
- **medium**: >=1,000 and <10,000 pages
- **large**: >=10,000 and <100,000 pages
- **extra large**: >=100,000 pages

> Note: The **established** boolean identifier above that includes all following levels is set with 20 as the cutoff given the behavior of instances once they reach their first 20 content pages.

> Note: Both `activity level` and `content level` can be combined for the behavior of instances at certain sizes.

<a id="code-examples-"></a>

## Code Examples [`⇧`](#contents)

<a id="content-edits-"></a>

### Content Edits [`⇧`](#contents)

We need to filter not just for main namespace (0 for Wikidata) and other content namespaces, but also that the page in question is not a redirect.

```sql
SELECT
    *

FROM
    event.mediawiki_page_change_v1

WHERE
    year = YEAR
    AND month = MONTH
    AND wiki_id = 'wikidatawiki'
    -- Other tables have different fields for namespace and is_redirect.
    AND page.namespace_id = 0
    AND page.is_redirect = false
```
