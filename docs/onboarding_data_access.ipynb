{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "37baae08-1b7d-4e1b-a52f-203c145f127a",
   "metadata": {},
   "source": [
    "# [Onboarding: Data Access](https://gitlab.wikimedia.org/repos/wmde/analytics/-/blob/main/docs/onboarding_data_access.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "569f358f-3fad-40ab-8791-3bae8bff8c7a",
   "metadata": {},
   "source": [
    "Welcome to WMDE SWE Analytics data access onboarding!\n",
    "\n",
    "Assuming that you have your [private data access](https://wikitech.wikimedia.org/wiki/Data_Platform/Data_access) as well as your [Kerberos credentials](https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Kerberos), this notebook is set up to help you start querying the [Data Lake](https://wikitech.wikimedia.org/wiki/Data_Platform/Data_Lake) using [wmfdata-python](https://gitlab.wikimedia.org/repos/data-engineering/wmfdata-python).\n",
    "\n",
    "Please also familiarize yourself with the contents of the [WMDE SWE Analytics contributing guide](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/CONTRIBUTING.md) along with this notebook :)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84dd7be2",
   "metadata": {},
   "source": [
    "## Accessing JupyterLab"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f6c09b44",
   "metadata": {},
   "source": [
    "> Note: The doc [Analytics Platform Access](https://docs.google.com/document/d/1YYV-FDhu3OeLIT4ST2WakoVuyxwOZLwDqWVket4YD7k/edit?usp=sharing) has all login procedures necessary for WMDE SWE Analytics work.\n",
    "\n",
    "> Note: The suggested [stat machine](https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Clients) for WMDE SWE Analytics is [stat1010](https://wikitech.wikimedia.org/wiki/Stat1010). Using the same stat machine makes transferring files a bit more seamless.\n",
    "\n",
    "You can access WMF's private JupyterLab via the following:\n",
    "- Login to stat1010 via an SSH port route and visit http://localhost:8880\n",
    "\n",
    "    ```bash\n",
    "    ssh -N stat1010.eqiad.wmnet -L 8880:127.0.0.1:8880\n",
    "    ```\n",
    "\n",
    "- Login to JupyterHub\n",
    "    - Username: Wikitech shell name\n",
    "    - Check [data.yaml](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/puppet/+/refs/heads/production/modules/admin/data/data.yaml) for your shell name once you have private data access\n",
    "    - Password: Wikitech\n",
    "- Once logged in, authenticate your Kerberos credential in the JupyterHub terminal with `kinit`\n",
    "    - Password: The one that was setup via email after receiving Kerberos credentials\n",
    "\n",
    "Jupyter notebooks within WMF's Spark setup have a lot of query logging warnings by default. You can do the following commands to get rid of them:\n",
    "\n",
    "```bash\n",
    "ipython profile create # creates two files, one being ipython_kernel_config.py\n",
    "echo \"c.IPKernelApp.capture_fd_output = False\" >> \"PATH_TO_ipython_kernel_config.py\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91883152-e818-4962-a211-5b704d855005",
   "metadata": {},
   "source": [
    "## Imports and Session"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a957e99-4b47-4f44-a07c-030b1b32138a",
   "metadata": {},
   "source": [
    "The next line loads in the extension [jupyter-black](https://github.com/n8henrie/jupyter-black) that will format all cell blocks after each time they're ran."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa576e5b-a518-47b9-81cb-ca60ce87570a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# pip install jupyter-black"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7a91ad63-2fa2-42b9-b232-c844eba0fd2e",
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext jupyter_black"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b635bb03",
   "metadata": {},
   "source": [
    "Notes on the `analytics_utils` functions below:\n",
    "- `display_peek`: Display the contents of a dataframe and require a keyboard input that then hides the output.\n",
    "- `print_spark_session_info`: Prints the session information including a link where you can check the status of your Spark jobs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "adaaa356-bf06-4784-ad52-17f4a6e7048e",
   "metadata": {},
   "outputs": [],
   "source": [
    "from analytics_utils import display_peek, print_spark_session_info\n",
    "\n",
    "import pandas as pd\n",
    "import wmfdata as wmf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db38fef2-0a12-491b-9e95-76f0831a7bd4",
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.set_option(\"display.max_rows\", 16)\n",
    "pd.set_option(\"display.max_columns\", None)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "215d438c-3f1d-462d-b330-f259ea12fb5b",
   "metadata": {},
   "source": [
    "Please note that you need to have opened a terminal in Jupyter Lab and authenticated your Kerberos credentials on the stat server before the running the next cell as this is required to create your Spark session."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c20f6b8b-09af-49f3-86fb-a1f296cbe6f9",
   "metadata": {},
   "outputs": [],
   "source": [
    "spark = wmf.spark.create_session(\n",
    "    type=\"local\",  # good for up to a few GB of data\n",
    "    # type=\"yarn-large\",  # or add resources to the session so it's able to handle larger queries\n",
    "    app_name=\"wmde_data_access_onboarding\",  # give a descriptive name to your session\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d44c8e2e-0f6e-4dfa-9b67-1dadf8946392",
   "metadata": {},
   "source": [
    "Check out the link below to see where you can track the progress of your queries. Processes will be tracked once queries via the spark session have been started."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c5129439-de37-44a3-8fa7-921c88dad189",
   "metadata": {},
   "outputs": [],
   "source": [
    "print_spark_session_info(spark=spark)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6479927a-a1ac-4cc4-9b71-9020cae4178a",
   "metadata": {},
   "source": [
    "## Exploration"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "583c7b4f",
   "metadata": {},
   "source": [
    "### Basic Access"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "340f680a-3eab-4c21-a066-5366c4f2054a",
   "metadata": {},
   "outputs": [],
   "source": [
    "wd_rest_api_requests_exploration_query = \"\"\"\n",
    "SELECT\n",
    "    *\n",
    "\n",
    "FROM\n",
    "    wmf.webrequest\n",
    "\n",
    "WHERE\n",
    "    normalized_host.project_family = 'wikidata'\n",
    "    AND uri_path LIKE '/w/rest.php/wikibase/%'\n",
    "\n",
    "LIMIT\n",
    "    5\n",
    ";\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "95ca65e2",
   "metadata": {},
   "source": [
    "Please note that converting your Spark DataFrame to Pandas is very process intensive. Open the URL from `print_spark_session_info` above to check your query status!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "55902508",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_wd_rest_api_requests_exploration = spark.sql(\n",
    "    wd_rest_api_requests_exploration_query\n",
    ").cache()\n",
    "df_pd_wd_rest_api_requests_exploration = df_wd_rest_api_requests_exploration.toPandas()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b967e058",
   "metadata": {},
   "source": [
    "We use `display_peek` below to show us the results of the query and then force us to close the results before being able to run further operations in the notebook. We thus hide personal information in the notebook as we work."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7cd956ac-74d7-4763-bdf0-a416c434ca99",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_peek(df_pd_wd_rest_api_requests_exploration)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24f06ab7",
   "metadata": {},
   "source": [
    "### Create Temp Views"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "366a60d7",
   "metadata": {},
   "source": [
    "The following creates a temporary view that can then be queried from in Spark SQL queries during the session."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ae161186",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_wd_rest_api_requests_exploration.createOrReplaceTempView(\n",
    "    \"df_wd_rest_api_requests_exploration\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db8827c6",
   "metadata": {},
   "outputs": [],
   "source": [
    "copy_wd_rest_api_requests_exploration_query = \"\"\"\n",
    "SELECT\n",
    "    *\n",
    "\n",
    "FROM\n",
    "    df_wd_rest_api_requests_exploration\n",
    ";\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bdbe5a66",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_copy_wd_rest_api_requests_exploration = spark.sql(\n",
    "    copy_wd_rest_api_requests_exploration_query\n",
    ").toPandas()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27b97802",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_peek(df_copy_wd_rest_api_requests_exploration)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3677b385-883e-4a22-ae79-1cfba3f4f82f",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
