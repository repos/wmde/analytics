# [Test Spark Queries](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/test/TEST_SPARK_QUERIES.md)

> Testing Spark SQL queries within a personal database on the WMF analytics cluster and infrastructure

Sources:

- [wikitech/Analytics/Systems/Cluster/Spark](https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark)

<a id="contents"></a>

### Contents

- [Access Spark SQL Server](#access-spark-sql-server-)
- [Run Test Queries](#run-test-queries-)
- [Clearing Test Data](#clearing-test-data-)

<a id="access-spark-sql-server-"></a>

## Access Spark SQL Server [`⇧`](#contents)

First SSH into your stats server of choice and authenticate with Kerberos if it’s not cached:

```bash
ssh statSTAT_NUM.eqiad.wmnet
kinit  # if necessary, then enter your Kerberos password
```

Then do a command from the [Spark Shell Wikitech Documentation](https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark#Start_a_spark_shell_in_yarn) - in this case the Spark SQL command:

```bash
spark3-sql --master yarn --executor-memory 8G --executor-cores 4 --driver-memory 2G --conf spark.dynamicAllocation.maxExecutors=64
```

Note that in some cases you may need to sudo access another user that has permissions you need to run the queries. This can be done via the following, using `analytics-privatedata` as an example:

```bash
sudo -u analytics-privatedata kerberos-run-command analytics-privatedata spark3-sql --master yarn --executor-memory 8G --executor-cores 4 --driver-memory 2G --conf spark.dynamicAllocation.maxExecutors=64
```

<a id="run-test-queries-"></a>

## Run Test Queries [`⇧`](#contents)

You can now execute SQL statements against the database:

```sql
USE YOUR_HIVE_USER_DB;
SHOW TABLES;
```

Please note that all of your test queries should be ran on a table that's saved in a unique location. The following should be at the end of each of your create table statements:

```sql
-- Change UNIQUE_ID to the name of your DAG or another identifier for the table.
LOCATION
    'hdfs://analytics-hadoop/tmp/wmde/analytics/test_data/UNIQUE_ID/'
```

Don't use the same location for other testing processes to prevent write accesses of your user and `analytics-privatedata` or `analytics-wmde` getting crossed.

<a id="clearing-test-data-"></a>

## Clearing Test Data [`⇧`](#contents)

After you're done testing the queries, please delete any tables that you don't need for further testing (Airflow DAGs, etc). Assuming that your test tables were created with a `DESTINATION` in the `/tmp/wmde` directory on HDFS, then they can be cleared with `hdfs dfs -rm -r ../../tmp/wmde/*` or a corresponding query for the desired path.

> Note: Deleting files on HDFS moves them to the user's `.Trash` directory, with trash deletion interval for WMF's HDFS instance being 30 days.
