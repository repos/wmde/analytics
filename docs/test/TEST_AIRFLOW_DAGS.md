# [Test Airflow DAGs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/test/TEST_AIRFLOW_DAGS.md)

> Testing Airflow DAGs and jobs via an Airflow instance deployed on the WMF analytics cluster and infrastructure

Sources:

- [wikitech/Data_Engineering/Systems/Airflow/Developer_guide#Development_instance](https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Airflow/Developer_guide#Development_instance)

<a id="contents"></a>

### Contents

- [Set Up Airflow Directory](#set-up-airflow-directory-)
- [Run Airflow Deployment](#run-airflow-deployment-)
- [Set Up Tables](#set-up-tables-)
- [Test Airflow DAG](#test-airflow-dag-)
- [Clean Up](#clean-up-)
- [Troubleshooting](#troubleshooting-)

<a id="set-up-airflow-directory-"></a>

## Set Up Airflow Directory [`⇧`](#contents)

First SSH into your stats server of choice and authenticate with Kerberos if it’s not cached:

```bash
ssh statSTAT_NUM.eqiad.wmnet
kinit  # if necessary, then enter your Kerberos password
```

From here note that you'll need to be deploying a copy of [data-engineering/airflow-dags](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags) that has the DAGs you'd like to test. This could be your fork if you chose to make one, or for those working directly via branch commits to the upstream repo you would clone the repo itself and switch to your branch with the [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/).

```bash
# Via a fork:
git clone https://gitlab.wikimedia.org/YOUR_USERNAME/airflow-dags.git

# Via the upstream repo:
git clone https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags.git

# Then enter your DAG repo and checkout the branch with the DAG you want to test:
cd airflow-dags
git pull  # in case you've cloned before
git checkout YOUR_BRANCH_NAME
```

<a id="run-airflow-deployment"></a>

## Run Airflow Deployment [`⇧`](#contents)

Run the following to create a virtual environment and an SQLite DB as well as launch the `wmde` Airflow processes:

```bash
# Within airflow-dags ...

# Orient yourself to the CLI options:
# ./run_dev_instance.sh -h

# Normal dev instance startup:
./run_dev_instance.sh wmde
```

<!--
sudo -u analytics-privatedata rm -rf "/tmp/wmde/andrewtavis_wmde_airflow_test"
sudo -u analytics-privatedata kerberos-run-command analytics-privatedata ./run_dev_instance.sh -p 8080 -m "/tmp/wmde/andrewtavis_wmde_airflow_test" wmde
-->

A sudo command via the `analytics-privatedata` user is the normal way that Airflow DAGs are tested though:

```bash
sudo -u analytics-privatedata rm -rf "/tmp/wmde/YOUR_HIVE_USER_airflow_test"  # remove directory in the stat file system if it exists
sudo -u analytics-privatedata kerberos-run-command analytics-privatedata ./run_dev_instance.sh -p 8080 -m "/tmp/wmde/YOUR_HIVE_USER_airflow_test" wmde
```

<a id="set-up-tables-"></a>

## Set Up Tables [`⇧`](#contents)

> Note: Column names should include the "what" of the data that they contain. For example, metric columns that count should start with `total_`.

You'll need to make test versions of the tables needed for the DAG that are tied to your Hive user, with [Test Spark Queries](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/test/TEST_SPARK_QUERIES.md) detailing how to run Spark SQL queries. Please make sure that your create table queries all have the following at the end so the outputs are saved in an appropriate location, and further that the defined location is unique:

```sql
-- Change DAG_ID to the name of your DAG.
LOCATION
    'hdfs://analytics-hadoop/tmp/wmde/analytics/test_data/DAG_ID/'
```

> Note: The production instance of tmp is `/wmf/tmp`, but we use the parallel `/tmp/wmde` for production and testing. Please put all testing table in `tmp/wmde/analytics/test_data/DAG_ID/` as specified above.

If you're using `analytics-privatedata` to run Airflow above, then note that you need to make sure that your tables are made via by accessing Spark via the `analytics-privatedata` user.

```bash
# Log into your stat server of choice:
ssh statSTAT_NUM.eqiad.wmnet
kinit  # if necessary, then enter your Kerberos password

sudo -u analytics-privatedata kerberos-run-command analytics-privatedata spark3-sql --master yarn --executor-memory 8G --executor-cores 4 --driver-memory 2G --conf spark.dynamicAllocation.maxExecutors=64
```

From there you can initialize your database schema and create the table.

```sql
USE YOUR_HIVE_USER_DB;
-- Paste your create table statement from your testing files.
```

> Note: Not doing this can cause serious problems in the permissions of Spark within Airflow. You would need to provide write permissions to `analytics-privatedata` via `hdfs dfs -chmod -R PATH_TO_DIRECTORY`, but even this does not guarantee that the user will have write access.

<a id="test-airflow-dag-"></a>

## Test Airflow DAGs [`⇧`](#contents)

Once everything's ready, open an SSH tunnel locally from a new terminal window to access the Airflow UI as described in the script logs. You'll then need to edit the variables for the DAG via the following steps:

- Select `Admin` > `Variables` to go to an overview of the variables for each DAG on the instance
- Select `Show record` to view a DAG's variables or `Edit record` to change them
  - Note: If you've changed the `DagProperties` props in the code since last deployment ...
    - You'll potentially see a warning `Property {prop_name} is not overridable.` if you've changed the keys
    - This means you need to recreate the DAG variables, but also do this if you've changed their values
    - Select `Delete record` in the `Variables` UI and wait a moment for the DAG to reinterpret automatically
- Change the variables to access tables in your Hive user database and export to your HDFS user directory (as needed)
  - Ideally version control the test variables you created as a JSON in the directory for the DAG
    - This provides a record of the test and makes it easier for others to reproduce it
    - Please replace the following:
      - `wmf` or `wmde` target table database schemas with `TEST_HIVE_USER_DB`
      - The `start_date` for the DAG with `TEST-YYYY-MM-DDT00:00:00` (test with most recent time period)
  - Change the above defined test variables to your hive user database and the most recent time period respectively
  - Wait for the `Last parsed time` (`last_parsed_time`) in the DAG `Details` to update, which indicates that the DAG has been reinterpreted (Airflow time is UTC)
    - The `Next Run` of your DAG should now be set appropriately if you've changed it
- You can now unpause the DAG and check that the outputs are expected
  - So tests finish more quickly on a sequential executor, don't run multiple intervals of any DAG
  - Choose an interval that you'd like to test on and mark any others as failed
- Once the DAGs are finished, check outputs via queries from tables or using `hdfs dfs -cat FILE_PATH` on files

If everything rums properly, then you're ready to merge!

<a id="clean-up-"></a>

## Clean Up [`⇧`](#contents)

Note that it's important to first drop tables before deleting their data. Deleting table metadata before dropping a table could prevent you from dropping it later as the table reference would be pointing at an invalid location. If you're unable to drop a table, then contact an admin or also try with Hive as described below.

It's good practice to clear the data from your DAG testing. HDFS can be cleared with `hdfs dfs -rm -r DIR_PATH/*` (likely `hdfs dfs -rm -r ../../tmp/wmde/*` ). Note further that if you're using `analytics-privatedata`, then you'll potentially need to sudo into this user to delete files its created (likely `sudo -u analytics-privatedata hdfs dfs -rm -r /tmp/wmde/*`). Remember also to clear the Airflow directory for your test instance with `sudo -u analytics-privatedata rm -rf "/tmp/wmde/YOUR_HIVE_USER_airflow_test"` - the command directly before the one that creates the instance above.

> Note: Deleting files on HDFS moves them to the user's `.Trash` directory, with trash deletion interval for WMF's HDFS instance being 30 days.

As we're assigning our test tables to the `analytics-privatedata` user, there is a chance that you won't be able to drop them during cleanup with Spark due to permissions. If this is the case, remove their data file on HDFS and then drop the table via Hive. You can access Hive by logging into the a stat machine and typing `hive`. You can then `USE YOUR_HIVE_USER_DB;` and run needed queries.

> Note: After you're done testing run `ps aux | grep airflow` to make sure that everything is shut down properly.

It's also important to clean up your branches if they weren't automatically deleted on merge, which should be set on merge request creation. If you have stale branches, feel free to delete them in the [GitLab Airflow branches UI](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/branches) and then update your local branches with `git remote update origin --prune`.

<a id="troubleshooting-"></a>

## Troubleshooting [`⇧`](#contents)

Errors in Airflow DAGs will happen :) The following are some things to look out for when you see that a job is "up for retry" or things just aren't working.

1. In the logs for the job, which can be found via `Details` > `Log`, look for the following keywords to see helpful hints on what's wrong:

   - `ERROR`
   - `Caused by`

   > Note: Don't forget to check the timestamp of the logs! Airflow keeps them all by default, so you may think that you're seeing the same error, but it's actually the same one and you need to scroll down. This is why building Airflow in a fresh `tmp` directory is a good idea so you have new logs for each testing process.

2. If the logs are not clear, one way that you can get a more descriptive output is to take the Spark command that's failing and run it directly in your stat machine of choice. Sometimes this cam lead to more detailed errors.

   > Note: If access to the output table has been given to `analytics-privatedata`, then you'll need to prepend the command with `sudo -u analytics-privatedata` in order for the keytab to be picked up.

3. If a DAG job is "passing" but not producing expected results given prior job tests, running the Spark command from Airflow directly on the stat machine can uncover an error that wasn't being shown in the UI.

   > Note: If the data isn't being written into the table even though the DAG is passing, then make sure that the table has been made using the `analytics-privatedata` on Spark if this is how you're running the local Airflow instance.

4. You could run into issues with the Airflow scheduler like the following:

   ```
   The scheduler does not appear to be running. Last heartbeat was received # days ago.
   The DAGs list may not update, and new tasks will not be scheduled.
   ```

   > Note: This is not necessarily a problem as the dev Airflow instance is running a sequential executor, meaning only one task can be executed at a time. While a task is executing it also cannot emit heartbeats.

   One cause of this could be that you had an Airflow process at a given port that wasn't exited properly and is now orphaned. Trying to start a new process at the same port could reference the old one without valid connections. Check to see if you have something running at the given port even without starring up an instance with `./run_dev_instance.sh`.

   For this you'll want to check your running processes with `ps aux | grep airflow` on the stat server you test Airflow on. You can then `kill PID_NUMBER` (or `sudo -u analytics-privatedata kill PID_NUMBER`) to clear the related processes. Specifically you'll want to kill the `gunicorn: master [airflow-webserver]`, which will kill all Gunicorn workers, as well as `airflow scheduler`. Don't worry about processes that are running for other uses as those won't have an effect.

5. Normally you should not need to log in to the test Airflow instance. If you are being asked to log in, then this is a good sign that you have an orphaned Airflow instance running. Please follow the steps above to check and solve this issue.

6. If you're getting an error like `Failed to open input stream for file:`, then the permissions for the tables aren't set properly.

   - Make sure that you're creating the table with Spark via the `analytics-privatedata` if this is how you're running your local airflow instance.
   - If the above is not working, check that the `analytics-privatedata` user has access to all tables via `hdfs dfs -chmod -R 777 ../../tmp/wmde` or `sudo -u analytics-privatedata hdfs dfs -chmod -R 777 ../../tmp/wmde`
     - Check the metadata location of the table specifically on `Hive` via `DESCRIBE FORMATTED TEST_HIVE_USER_DB.TABLE_NAME` or get it from the error in the Airflow logs
     - Check the file to make sure that permissions are set to `-rwxrwxrwx`

7. If the connection to the Airflow frontend is being reset, take a moment and then reload the page. There's no need to kill and redo the command.

8. If all else fails, update your branch with the latest changes from main as this could lead to better error logs and other updates.
