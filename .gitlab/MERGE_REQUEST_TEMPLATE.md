<!---
Thank you for your merge request! 🚀
-->

### Contributor checklist

<!--
Please replace the empty checkboxes [ ] below with checked ones [x] accordingly.
Please also replace PATH_TO_JOB_TESTS with an appropriate path within wmde/analytics/hql.
-->

- [ ] I have checked all files included in this merge request for data that should not be publicly released
- [ ] Code formatting and conventions are in line with the [WMDE SWE Analytics contributing guide](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/CONTRIBUTING.md)

### Description

<!--
Please describe briefly what your merge request proposes to change. Especially if you have more than one commit, it is helpful to give a summary of what your contribution is trying to achieve.

Please also link to the appropriate Phabricator task by replacing TASK_NUMBER below (GitLab will link).

You can also replace TASK_NUMBER in 'Bug: TASK_NUMBER' below to trigger CodeReviewBot Phabricator messages for this MR.
See: https://wikitech.wikimedia.org/wiki/GitLab/Webhooks
Bug: TASK_NUMBER
-->

- TASK_NUMBER: Description
